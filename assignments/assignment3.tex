\documentclass[letterpaper]{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{times}

\newcommand{\real}{\mathbb{R}}
\newcommand{\tr}{^\top}
\newcommand{\opt}{^\star}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0mm}


% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\title{Assignment 3}
\author{Reinforcement Learning}
\date{\textbf{Due}: Wednesday 9/22 at 11:59PM}

\begin{document}
\maketitle

\begin{itemize}
\item \textbf{Submission}: Turn in the theoretical part as a \texttt{PDF} on \url{http://mycourses.unh.edu} or on paper in class. LaTeX, (R)markdown,  or scanned hand-written notes are all OK. Go with whatever you are familiar with.
\item Please also turn in your source code separately as a text file. Please document the source code with some basic instructions including dependencies that will help us run your code if needed.
\item \textbf{Questions}: See the class website for a link to Piazza and office hours
\end{itemize}


\paragraph{Notes}
\begin{itemize}
    \item You should read Chapters 1 and 2 in RLOC to solve the problems on this assignment. You may also want to consult the lecture notes.
    \item You should be able to implement this assignment in any higher-level programming language, including Python, R, MATLAB, Julia, Java, C++. Although Jupyter / Rnotebook and similar tools may be helpful later in the course, implementing a simple executable script will be easier for now. The OpenAI gym assignment will require some Python. If this is a problem, please let the instructor (Marek) know.
\end{itemize}

\paragraph{Edits}
\begin{enumerate}
    \item Fixed a typo in 2.1. The right-hand side should read $v_{t+1}$ instead of $v_{t+1}\opt$
\end{enumerate}

\section*{Problem 1: Bellman Optimality [30\%]}

Recall that the Bellman optimality equation is:
\begin{equation} \label{eq:dynamic_condition}
	\begin{aligned}
		v_T\opt(s) &\;=\; r_T(s) ~,\\
		v_t\opt(s) &\;=\; \max_{a\in\actions}\; \Exx{W_t \sim p_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a, W_t)) }, \qquad t = 0, \ldots, T-1~,
	\end{aligned}
\end{equation}

Please answer the following questions.
\begin{enumerate}
	\item The optimality conditions in~\eqref{eq:dynamic_condition} are stated in terms of the optimal value function. Restate these optimality conditions for the state-action value function (or q-function or Q-factor) which is defined as
	\[
	q_t\opt(s,a) = \Exx{W_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a,W_t)) }~.
	\]
	Your condition should involve only $q\opt_t$ and $v_T\opt$ (and NO $v_t\opt$ for $t < T$.)
	\item Write how you would construct $\pi\opt$ from $q\opt$.
	\item Give a pseudocode of an algorithm that computes $q\opt$ and $\pi\opt$ for a dynamic program (similar to the lecture notes).
\end{enumerate}

\section*{Problem 2: Randomized Policies [40\%]}

Randomized policies randomly choose actions from some probability for each state that they visit. A \emph{randomized policy} is defined as: $\pi: \states\times\actions \to [0,1]$. For example, a randomized policy in the optimal parking problem can decide to park with probability $70\%$ and go with probability $30\%$. Such policy would be defined as:
\[
\pi_t(s,a) = \begin{cases}
	0.7  &\text{ if } a = P \\
	0.3  &\text{ if } a = G
\end{cases}~.
\]

We will prove that randomized policies will never outperform the optimal deterministic $\pi\opt$ in a dynamic program. Follow these steps.

\begin{enumerate}
	\item Argue by backward induction (informally is OK) that $v_t(s,\pi)$ for a randomized policy $\pi$ satisfies that:
	\begin{equation*}
		\begin{aligned}
			v_T(s,\pi) &\;=\; r_T(s) ~,\\
			v_t(s,\pi) &\;=\; \sum_{a\in\actions}\; \pi(s,a) \cdot \Exx{W_t \sim p_t}{r_t(s,a, W_t) + v_{t+1}(f_t(s,a, W_t); \pi) }, \qquad t = 0, \ldots, T-1~,
		\end{aligned}
	\end{equation*}
	\item Argue that for any randomized policy $\pi$, there exists an action $a$ for each state $s$ such that
	\[
		q_t(s,a; \pi) \;\ge\; v_t(s;\pi)~.
	\]
	\emph{Hint}: Use the definition of the terms and rewrite the expectation as a sum.
	\item Use backward induction to argue that if $q_t(s,\bar\pi(s); \pi) \ge v_t(s;\pi)$ then $v_t(s,\bar{\pi}) \ge v_t(s,\pi)$ for any two policies $\bar\pi$ and $\pi$ (randomized or otherwise). \emph{Hint}: Backward induction and using the fact that probabilities are non-negative.
	\item Combine the results above to argue that for any randomized policy $\pi$ there exists a deterministic policy $\bar\pi$ such that:
	\[
	v_0(s_0; \bar{\pi}) \ge v_0(s_0; \pi)~.
	\]
\end{enumerate}


\section*{Problem 3: Lookahead [30\%]}

\begin{enumerate}
	\item Give an example of a problem in which lookahead is likely to work well. Be specific about the problem, which can be either realistic or synthetic.

	\item Give an example of a problem in which lookeahead is likely to fail (either does not compute a good policy or will be too computationally expensive).  Be specific about the problem, which can be either realistic or synthetic.

	\item Recall the definition of discounted objective
	\[
	\max_{\pi\in\Pi_D} \; \Ex{\sum_{k=0}^{T-1} \gamma^k \cdot r_k(S_k, A_k, W_k) + \gamma^T \cdot r_T(S_T) ~\middle\vert~
			\begin{matrix} S_k = f_{k-1}(S_{k-1}, A_{k-1}, W_{k-1}),\\ S_0 = s_0, A_k = \pi_k(S_{k}) \end{matrix}}
	\]
	for a discount factor $\gamma \in [0,1]$. What do you expect the impact of the discount factor be on how well lookahead works? In other words, do you think that multistep lookahead is likely to work better or worse when $\gamma$ is small rather than close to $1$?
\end{enumerate}

\end{document}