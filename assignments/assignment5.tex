\documentclass[letterpaper]{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{times}

\newcommand{\real}{\mathbb{R}}
\newcommand{\tr}{^\top}
\newcommand{\opt}{^\star}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0mm}


% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\title{Assignment 5}
\author{Reinforcement Learning}
\date{\textbf{Due}: Wednesday 10/06 at 11:59PM}

\begin{document}
\maketitle

\begin{itemize}
\item \textbf{Submission}: Turn in the theoretical part as a \texttt{PDF} on \url{http://mycourses.unh.edu} or on paper in class. LaTeX, (R)markdown,  or scanned hand-written notes are all OK. Go with whatever you are familiar with.
\item Please also turn in your source code separately as a text file. Please document the source code with some basic instructions including dependencies that will help us run your code if needed.
\item \textbf{Questions}: See the class website for a link to Piazza and office hours
\end{itemize}

\section*{Problem 1: Linear Regression [50\%]}

In this exercise, we will experiment with linear regression and various feature construction algorithms. Consider a dynamic program with a single-dimensional state space $\states = [0,1]$ and let $\hat{\states} = \{ 0, 0.1, \ldots, 0.9, 1\}$. Also, let the true value function be $\hat{v}(s) = \min \{ 2 \cdot s , -0.1 \cdot s + 0.2, 0.8 - s \}$. (We are omitting the $t$'s in this case because they are all the same.)

Please compute $w \in \Real^k$ that solves
\[
\min_{w\in\Real^K} \; \sum_{\hat{s}\in\hat{\states}} (\phi(\hat{s})\tr w - \hat{v}{(\hat{s})})^2
\]
for the feature functions $\phi$ defined below. For each choice of $\phi$, also generate a plot which has $s$ on the $x$-axis and $\phi(\hat{s})\tr w$ on the $y$-axis.
\begin{enumerate}[nosep]
	\item Linear features with both slope and intercept
	\item Polynomial features with degree 2
	\item Polynomial features with degree 3
	\item Radial basis functions with  3 centers
	\item \ [optional+3] Constant features (aggregation) with 5 different values
	\item \ [optional+3] Linear regression splines with at least 3 knots
\end{enumerate}

I recommend reading Chapter 9.5 in RL in addition to the lecture notes.

\section*{Problem 2: Fitted Value Iteration [50\%]}

In this problem, we will implement and experiment with the FVI algorithm on a simple RL problem. 

This is the same problem setup from last time. Imagine that you are put in charge of managing water levels of a reservoir on a dammed river. Rainfall, and therefore the inflow, varies from day to day. Your goal is to control the outflow from the reservoir in order to make sure that the water level in the reservoir is stable to allow for recreation. It is important, however, that the outflow does not vary too much because irrigation, electricity generation, and aquatic species rely on steady outflow. The outflow decisions are made daily and we assume no access to inflow forecasts.

This simplified reservoir management can be framed as the following dynamic program:
\begin{itemize}
	\item \emph{States} $\states = [0,1]$ where $0$ represents the minimum acceptable level of water in the reservoir, and $1$ represents the maximum acceptable level.
	\item \emph{Actions} $\actions = \{ 0, \ldots, 7 \}$ represent the desired outflow from the reservoir for the following day.
	\item \emph{Disturbances} $\mathcal{W} = \{0, \ldots, 10\}$ represent the random inflows. Assume that the probability distribution over these disturbances is uniform.
	\item \emph{Transition function} is (omitting the subscript $t$ because the transitions do not depend on time):
	\[
	f(s,a,w) \;=\; \min \{ 1, \max\{0, s + 0.11 \cdot w - 0.13 \cdot a \} \}
	\]
	\item \emph{Rewards} (also omitting the subscript $t$):
	\[
	r(s,a,w) \;=\; -(s - 0.5)^2 -  |a - 3|
	\]
	\item The terminal reward for $t = T$ is zero
	\item The initial state is $s_0 = 0$
	\item The horizon is one year: $T = 365$
\end{itemize}

Please complete the following parts.
\begin{enumerate}
	\item Implement FVI to approximate the optimal value function with degree 2 polynomial features as $\phi: \states \to \Real^3$. Use $|\hat{S}_t| = 15$ sampled uniformly from $[0,1]$ for all $t = 0, \ldots, T-1$. That is, compute $(\tilde{w}_t)_{t=0}^{T-1}$ and let $\tilde{v}_t(s) = \phi(s)\tr \tilde{w}_t$. Then, plot $\tilde{v}_0(s_0)$ as a function of $s_0 \in [0,1]$. Let $\tilde{\pi}^1$ be the policy greedy with respect to the value function $\tilde{v}(s)$ computed in this part.
	\item Use Monte-Carlo policy evaluation with $M=30$ (less if this is too slow) to approximate $v_0(0; \tilde{\pi}^1)$.
	\item Plot $v_0(s_0; \tilde{\pi}^1)$ computed from the Monte-Carlo simulation as a function of $s_0$ and compare with $\tilde{v}_0(s_0)$. Discuss the results and their importance.
	\item Experiment with a different choice of features. That is, run FVI with at least one different choice of $\phi$ and compare the computed value function with the one from the first part. Do you get a better policy (in terms of its return)? 
\end{enumerate}


\section*{Bonus Problem 3: Monte Carlo Estimation [extra +20\%]}

This question is \emph{optional}. It requires reading and understanding advanced material which we have not covered in class. You may need to do some extra reading to answer this question successfully. Only spend time on it if you are done with the others.

Consider the reservoir problem again. I would like to know the return of the heuristic policy $\tilde{\pi}$ defined for all $t$ as:
\[
\tilde\pi(s) = \operatorname{round}(7 \cdot s)
\]
How many iterations $M$ of the Monte-Carlo policy evaluation method do I need to run in order to estimate $\tilde{\rho} \approx v_0(0, \tilde{\pi})$ such that $|\tilde{\rho} - v_0(0, \tilde{\pi})| \le 1$ with $95\%$ confidence. Full credit only if you make no distributional assumptions (such as normality). Explain what you mean by the $95\%$ confidence.

\emph{Hint}: Concentration inequalities. This book may be useful:

Boucheron, S., Lugosi, G., \& Massart, P. (2013). Concentration Inequalities: A Nonasymptotic Theory of Independence. Oxford University Press. 




\end{document}