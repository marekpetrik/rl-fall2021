\documentclass[letterpaper]{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{times}

\newcommand{\real}{\mathbb{R}}
\newcommand{\tr}{^\top}
\newcommand{\opt}{^\star}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0mm}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers

\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\title{Assignment 7}
\author{Reinforcement Learning}
\date{\textbf{Due}: Friday 10/29 at 11:59PM}


\begin{document}
    \maketitle

    \begin{itemize}
        \item \textbf{Submission}: Turn in the theoretical part as a \texttt{PDF} on \url{http://mycourses.unh.edu} or on paper in class. LaTeX, (R)markdown,  or scanned hand-written notes are all OK. Go with whatever you are familiar with.
        \item Please also turn in your source code separately as a text file. Please document the source code with some basic instructions, including dependencies that will help us run your code if needed.
        \item \textbf{Questions}: See the class website for a link to Piazza and office hours
    \end{itemize}

    Additional study materials:

    You can find a rigorous and clear treatment of the infinite horizon discounted MDPs in Chapter 6 of
    \begin{quote}
        Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. Wiley-Interscience.
    \end{quote}
    I also suggest that you consult Chapter 3 of
    \begin{quote}
        Sutton, R. S., \& Barto, A. G. (2018). Reinforcement learning: An introduction (2nd ed.). The MIT Press.
    \end{quote}
    The second book's pdf is available online and can be accessed using a link from the class website.

    \section*{Problem 1: Interest rate and discounting [30\%]}

    In this problem we establish an important relationship between discount factors and interest rates.

    Suppose that each policy $\pi$ generates a steady and predictable (deterministic) stream of \emph{annual} incomes $r_0^\pi, r_1^\pi, r_2^\pi, \ldots, r_T^\pi$. You save these incomes in a bank account with an interest rate $\nu \ge 0$. The interest in your account compounds. That is, your account will have $g^\pi_0 = r_0^\pi$ funds in the first year, $g^\pi_1 = r_0^\pi \cdot (1+\nu) + r_1^\pi$ funds in the second year, $g^\pi_2 = (r_0^\pi \cdot (1+\nu) + r_1^\pi)\cdot(1+\nu) + r_2^\pi$, and so on.

    Please answer the following questions now.

    \begin{enumerate}
        \item Give a general formula for the total amounts of funds $g^\pi_t$ at any $t = 0, \ldots, T$. \emph{Hint}: The answer involves a sum and exponentiation. Write the first few terms and observe the pattern.
        \item Recall that the $\gamma$-discounted return $\rho^\pi$ of a policy for $T$ time steps is defined as:
        \[
        \rho_T^\pi \;=\; \sum_{t=0}^T \gamma^t \cdot r^\pi_t~.
        \]
        Now, give the value for the discount factor $\gamma \in [0,1]$ as a function of $\nu \ge 0$ such that
        \[
        g_T^{\pi_1} > g_T^{\pi_2} \;\Rightarrow\; \rho_T^{\pi_1} > \rho_T^{\pi_2}
        \]
        for any two policies $\pi_1$ and $\pi_2$. In other words, this means that a policy that maximizes the discounted return (value function) also maximizes the total amount of funds in the bank account.

        \emph{Hint}: You need to construct a discount factor such that $\rho_T^\pi = c \cdot g^\pi_T$ for some positive $c$.
    \end{enumerate}

    \section*{Problem 2: Value Function in Grid Worlds [40\%]}

    In this problem, we will evaluate some policy in a grid world. You need to use linear algebra (numpy) to solve this problem. If you have written more than 50 lines of code, you are not implementing the solution to this problem efficiently.

    The goal of this exercise is to get familiar with matrix operations used when computing value functions.

    Consider a discount objective with a discount factor $\gamma = 0.99$ in this problem.

    \begin{enumerate}
        \item Construct a single-dimensional linear grid-world environment, similar to one covered in the class. Your environment should have at least 5 cells, and the transition probabilities can be arbitrary but should be reasonable. Assume any arbitrary rewards. Construct the transition probability matrix $P^\pi$ and reward vector $r^\pi$.

        \item Write code that approximates the value function $v_{0,\infty}^\pi$ by sequentially computing $v_{0,0}^\pi, v_{0,1}^\pi, \ldots, v_{0,100}^\pi$. You should be using matrix operations (numpy or similar) to achieve this goal and the Bellman update equation
        \[
        v_{0,T+1}^\pi \;=\; L^\pi v_{0,T}^\pi~,
        \]
        which we covered in the class. This algorithm is a simple form of value iteration.

        \item Write code that computes the value $v_{0,\infty}^\pi$ by solving the system of linear equations that we covered in class.

        \item Write code that computes the error $\| v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty$ where the $L_\infty$ norm is defined for $x\in\Real^S$ as
        \[
        	\| x\|_\infty = \max_{i=1, \ldots, S} |x_i|
        \]

        \item Give a proof showing that
        \[
        \| v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty \;\le\; \frac{1}{1-\gamma} \| L v_{0,100}^\pi - v_{0,100}^\pi \|_\infty~.
        \]
        This is a very useful bound that can be used to establish the number of value iterations sufficient to get a close approximation of the infinite-horizon value function $v_{0,\infty}^\pi$.

    \end{enumerate}


    \section*{Problem 3: Neumann Series Representation [30\%]}

    In this section, we will establish an alternative way of expressing the infinite-horizon $\gamma$ discounted value function $v_{0,\infty}^\pi$. As always, we assume that $\gamma \in [0,1)$ and also consider some fixed, given policy $\pi$.

    \begin{enumerate}
        \item Show that the value function can be expressed as the following series
        \[
        v_{0,\infty}^\pi = \sum_{t = 0}^\infty \gamma^t (P^\pi)^t r^\pi~.
        \]
        The expression $(P^\pi)^t$ refers to the power of the matrix. For instance, $(P^\pi)^3 = P^\pi P^\pi P^\pi$. This is different from the element-wise power function.

        \item Look up and give the formula for the sum of the Neumann series (\url{https://en.wikipedia.org/wiki/Neumann_series}). Then use this formula to derive the formula for $v_{0,\infty}^\pi$.

        \item Show that the solution that you obtained in the part 2 of this problem satisfied the fixed-point solution
        \[ v_{0,\infty}^\pi = r^\pi + \gamma P^\pi v_{0,\infty}^\pi~. \]

    \end{enumerate}

\end{document}