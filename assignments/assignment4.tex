\documentclass[letterpaper]{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{times}

\newcommand{\real}{\mathbb{R}}
\newcommand{\tr}{^\top}
\newcommand{\opt}{^\star}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0mm}


% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\title{Assignment 4}
\author{Reinforcement Learning}
\date{\textbf{Due}: Wednesday 9/29 at 11:59PM}

\begin{document}
\maketitle

\begin{itemize}
\item \textbf{Submission}: Turn in the theoretical part as a \texttt{PDF} on \url{http://mycourses.unh.edu} or on paper in class. LaTeX, (R)markdown,  or scanned hand-written notes are all OK. Go with whatever you are familiar with.
\item Please also turn in your source code separately as a text file. Please document the source code with some basic instructions including dependencies that will help us run your code if needed.
\item \textbf{Questions}: See the class website for a link to Piazza and office hours
\end{itemize}

\paragraph{Notes}
\begin{itemize}
    \item You should read Chapter 3  RLOC to solve the problems on this assignment. You may also want to consult the lecture notes.
\end{itemize}

\section*{Problem 1: 780 students [30\%]}

The aim of this exercise is to get you do familiarize yourself with implementing basic linear algebra using the programming language that you are comfortable with. We will not need any advanced linear algebra concepts, we will mostly use it to solve systems of linear equations and simple optimization problems.

Undergraduate students also have the option of solving the graduate version of this problem instead of the undergraduate one.

Please see \url{https://gitlab.com/marekpetrik/rl-fall2021#linear-algebra} for some linear algebra references that I have found useful. If you have other recommendations, please post them on Piazza and I will add them to the website.

If you are using Java and cannot switch to Python, I recommend that you use: \url{http://ejml.org/wiki/index.php?title=Main_Page}. If you are using Python, you should be using Numpy. R and MATLAB have the necessary linear algebra tools built in.

\begin{enumerate}
	\item The first goal is to solve a system of linear equations
	\begin{align*}
		2 x_1 + 3 x_2 + 5 x_3 &= 1 \\
		3 x_1 + 5 x_2 + 2 x_3 &= 2 \\
		1 x_1 + 1 x_2 + 1 x_3 &= 3 
	\end{align*}
	To solve this linear system, you need to formulate a $3\times 3$ matrix $A$ and a vector $b$ defined as:
	\[
	A = \begin{pmatrix}
	2 & 3 & 5 \\ 
	3 & 5 & 2 \\ 
	1 & 1 & 1 
	\end{pmatrix} \qquad b = \begin{pmatrix}
	1 \\ 2 \\ 3
	\end{pmatrix}
	\]
	Then compute the solution as
	\[
	x = A^{-1} b~,
	\]
	where $A^{-1}$ is the matrix inverse.
	\item The second goal is to solve an \emph{over-determined} system of linear equations:
	\begin{align*}
		2 x_1 + 3 x_2 &\approx 1 \\
		3 x_1 + 5 x_2 &\approx 2 \\
		1 x_1 + 1 x_2 &\approx 3 
	\end{align*}
	in order to minimize the sum of squares of the errors. To solve this linear system, you need to formulate a $3\times 2$ matrix $A$ and a vector $b$ defined as:
	\[
	A = \begin{pmatrix}
		2 & 3  \\ 
		3 & 5  \\ 
		1 & 1  
	\end{pmatrix} \qquad b = \begin{pmatrix}
		1 \\ 2 \\ 3
	\end{pmatrix}
	\]
	Then compute the solution as
	\[
	x = (A\tr A)^{-1} A\tr b~,
	\]
	where $A^{-1}$ is the matrix inverse and $A\tr$ is the transpose of the matrix.
\end{enumerate}


\section*{Problem 1: 880 students [30\%]}

We say that an approximate value function $\tilde v = (\tilde v_t)_{t=0}^T$ is \emph{sequentially improving} if 
\[
	\max_{a\in\actions} \; \Exx{W_t}{r_t(s, a, W_t) + \tilde v_{t+1}(f_t(s, a, W_t))} \ge \tilde v_t(s)
\]
for each $s\in\states$ and $t = 0, \ldots, T$.

\begin{enumerate}
	\item Prove that when $\tilde{v}_t(s) = v_t(s, \tilde{\pi})$ for some policy $\tilde{\pi}\in\Pi_D$ and all $s\in\states$ and $t=0, \ldots, T$, then $\tilde{v}$ is self-improving.
	\item Prove that the rollout policy $\bar{\pi}$ constructed from a sequentially-improving heuristic value $\tilde{v}$ is an improving policy:
	\[
	v_t(s, \bar{\pi}) \;\ge\; \tilde{v}_t(s)
	\]
	for all $s\in\states$ and $t = 0,\ldots,T$. \emph{Hint}: This result follows by a small modification of the similar proof from the lecture notes.
\end{enumerate}


\section*{Problem 2: Rollout [70\%]}

In this problem, we will implement and experiment with the rollout algorithm on a simple RL problem. 

Imagine that you are put in charge of managing water levels of a reservoir on a dammed river. Rainfall, and therefore the inflow, varies from day to day. Your goal is to control the outflow from the reservoir in order to make sure that the water level in the reservoir is stable to allow for recreation. It is important, however, that the outflow does not vary too much because irrigation, electricity generation, and aquatic species rely on steady outflow. The outflow decisions are made daily and we assume no access to inflow forecasts.

This simplified reservoir management can be framed as the following dynamic program:
\begin{itemize}
	\item \emph{States} $\states = [0,1]$ where $0$ represents the minimum acceptable level of water in the reservoir, and $1$ represents the maximum acceptable level.
	\item \emph{Actions} $\actions = \{ 0, \ldots, 7 \}$ represent the desired outflow from the reservoir for the following day.
	\item \emph{Disturbances} $\mathcal{W} = \{0, \ldots, 10\}$ represent the random inflows. Assume that the probability distribution over these disturbances is uniform.
	\item \emph{Transition function} is (omitting the subscript $t$ because the transitions do not depend on time):
	\[
		f(s,a,w) \;=\; \min \{ 1, \max\{0, s + 0.11 \cdot w - 0.13 \cdot a \} \}
	\]
	\item \emph{Rewards} (also omitting the subscript $t$):
	\[
		r(s,a,w) \;=\; -(s - 0.5)^2 -  |a - 3|
	\]
	\item The terminal reward for $t = T$ is zero
	\item The initial state is $s_0 = 0$
	\item The horizon is one year: $T = 365$
\end{itemize}

Using the domain specification above, please write code that will answer the following questions.

\begin{enumerate}
	\item Implement the domain specified above and a heuristic policy $\tilde{\pi}$ defined for all $t$ as:
	\[
		\tilde\pi(s) = \operatorname{round}(7 \cdot s)
	\]
	This policy releases an increasing amount of water with increasing reservoir levels.

	\item Implement a Monte-Carlo value function estimation algorithm (Algorithm 2.1 in lecture notes 6) that estimates $v_0(s_0;\tilde{\pi})$ using $M=100$ simulations. Run the algorithm several times (without setting a random seed). What do you observe?

	\item Let $v_0^M(s_0;\tilde{\pi})$ be the Monte-Carlo estimate  with varying numbers of samples $M = 20, 40, \ldots, 200$. Repeat $5$ times. Then, compute and \emph{plot} the sample variance (\url{https://en.wikipedia.org/wiki/Variance}) of each estimator $v_0^M(s_0;\tilde{\pi})$ across the $5$ runs as a function of $M$.

	\item Implement a 1-step lookahead algorithm and compare its return to using the heuristic policy $\tilde{\pi}$. \emph{Hint}: You will need to run Algorithm 2.1 again to estimate the return of the lookahead policy.

	\item Implement a rollout algorithm (Algorithm 2.2) using the $\tilde{\pi}$ described above with $M=10$ and compare its return with $\tilde{\pi}$ and the lookahead algorithm. \emph{Hint}: You will need to run Algorithm 2.1 again to evaluate the return of $\bar{\pi}$.
\end{enumerate}


\end{document}