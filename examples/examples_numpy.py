import numpy as np

# vector
x = np.array([1,2,3,4,5,6,7,4,3,2])

# slice it
print(x[0:3])
print(x[:])


# operations
print(np.sum(x))
print(np.max(x))
print(np.argmax(x))

print(np.mean(x))
print(np.var(x))
print(np.std(x))

# matrix

A = np.array([[1,2,3], [4,5,7], [1,1,4]])
b = np.array([4,3,-1])

print(A)

# slice it
print(A[0,:])
print(A[:,0])

# operations
print(np.max(A))
print(np.max(A,1))

# linear algebra
print(A)
print(A.T)

# multiplication
print(np.dot(A,b))
print(np.matmul(A,b))
print(A @ b)
print(A @ A)

# DIFFERENT!!! Do not do this
print(A * A)

# matrix inverse
print(np.linalg.inv(A))
print(np.linalg.inv(A) @ b)
print(np.linalg.solve(A, b))

# matrix convenience

print(np.ones((4,4)))
print(np.zeros((4,4)))

print(np.eye(4))


