# Tested with:
# Python 3.9.7
# Numpy 1.21.2

## Generate the problem definition

import numpy as np
import matplotlib.pyplot as pp

S_all = np.linspace(0,1,500)

v_true = lambda s: min(s, 0.8 - s)
v_true_v = np.vectorize(v_true)

pp.plot(S_all, v_true_v(S_all))
pp.show()


S_hat = np.linspace(0,1,6)

v_hat = v_true_v(S_hat)
pp.plot(S_hat, v_hat, 'x')
pp.show()

### Experiment with different features

# polynomial features
#phi = lambda s: np.array([1,s,s**2, s**3, s**4])

# piecewise constant / aggregation
#phi = lambda s: np.array([float(s < 0.5), float(s >= 0.5)])

# linear spline
phi = lambda s: np.array([1, s, max(0.0, s - 0.3), max(0.0, s - 0.7)])


A = np.array([phi(s) for s in S_hat])

# idiomatic, but inefficient and unreliable way
#    w = np.linalg.inv(A.T @ A) @ A.T @ v_hat
# better
#    w = np.linalg.solve(A.T @ A,  A.T @ v_hat)
# even better
w = np.linalg.lstsq(A, v_hat, rcond = 0)[0]

v_tilde = lambda s: phi(s).T @ w


v_tilde_v = np.vectorize(v_tilde)

pp.plot(S_hat, v_hat, 'x')
pp.plot(S_all, v_tilde_v(S_all))
pp.show()
