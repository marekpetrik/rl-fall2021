# only working with policy evaluation

import numpy as np
import matplotlib.pyplot as pp

gamma = 0.9

P = np.array([[0.5, 0.5, 0.0, 0.0, 0.0],
              [0.5, 0.0, 0.5, 0.0, 0.0],
              [0.0, 0.5, 0.0, 0.5, 0.0],
              [0.0, 0.0, 0.5, 0.0, 0.5],
              [0.0, 0.0, 0.0, 0.5, 0.5]])

r = np.array([1,2,10,-4,-5])

## value iteration: vpi = L vpi

vpi = np.random.rand(5) * 100 - 100
for t in range(100):
    vpi = r + gamma * P @ vpi
    print(t+1, vpi.T)

## solving using a system of linear equations

vpi = np.linalg.solve(np.eye(5) - gamma*P, r)
print(vpi)
pp.plot(range(5), vpi,'o:'); pp.xlabel("state"); pp.ylabel("value")
pp.show()

## projections and value functions

# feature function (features are linear in states)
phi = lambda s: [1, s]

# feature matrix
Phi = np.array([phi(s) for s in range(5)])


# best approximation of the true value function
w_fit = np.linalg.inv(Phi.T @ Phi) @ Phi.T @ vpi
pp.plot(range(5), vpi,'o:', label='vpi')
pp.plot(range(5), Phi @ w_fit,'o:', label='vtilde')
pp.xlabel("state"); pp.ylabel("value"); pp.legend()
pp.show()

## Bellman update


# start with some w
w = np.random.rand(2)  # why 3?

# approximate value function
tildev = Phi @ w
pp.plot(range(5), tildev,'o:', label="v_approx")
pp.show()

# Bellman update of tildev
tildev_2 = r + gamma * P @ Phi @ w
pp.plot(range(5), tildev,'o:'); pp.xlabel("state"); pp.ylabel("value")
pp.plot(range(5), tildev_2,'o:'); pp.xlabel("state"); pp.ylabel("value")
pp.show()

## projected Bellman update (projected value iteration)

# projected Bellman update

w_2_fit = np.linalg.inv(Phi.T @ Phi) @ Phi.T @ (r + gamma * P @ Phi @ w)
pp.plot(range(5), tildev,'o:', label="tildev");
pp.plot(range(5), tildev_2,'o:', label="tildev_2");
pp.plot(range(5), Phi @ w_2_fit,'o:', label="p_tildev_2");
pp.xlabel("state"); pp.ylabel("value"); pp.legend()
pp.show()

## LSTD (iterative)

wpi = np.random.rand(2)
for t in range(200):
    wpi = np.linalg.inv(Phi.T @ Phi) @ Phi.T @ (r + gamma * P @ Phi @ wpi)
    print(t, (Phi @ wpi).T)

## LSTD (fixed point)

# both of the options below work
wpi = np.linalg.inv(Phi.T @ (np.eye(5) - gamma*P ) @ Phi) @ Phi.T @ r
wpi = np.linalg.solve(Phi.T @ (np.eye(5) - gamma*P ) @ Phi, Phi.T @ r)
print(Phi @ wpi)

## Does it always work? (iterative)

r_x = np.array([0,0])
P_x = np.array([[0,1],[0,1]])
Phi_x = np.array([[0.5,],[1,]])

wpi_x = np.array([1,])

for t in range(200):
    wpi_x = np.linalg.inv(Phi_x.T @ Phi_x) @ Phi_x.T @ \
                    (gamma * P_x @ Phi_x @ wpi_x)
    print(t, Phi_x @ wpi_x)

## Does it always work? (fixed point)

wpi_x = np.linalg.inv(Phi_x.T @ (np.eye(2) - gamma*P_x ) @ Phi_x) @  Phi_x.T @ r_x
#wpi_x = np.linalg.solve(Phi_x.T @ (np.eye(2) - gamma*P_x ) @ Phi_x, Phi_x.T @ r_x)
print(Phi_x @ wpi_x)


