\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{mathabx}        % for \bigtimes
\usepackage{booktabs}
\usepackage{mathtools}

% diagram stuff
\usepackage{pgf}
\usepackage{tikz}

\usetikzlibrary{arrows,automata}

\lectureNotesTitle{Lecture 5: Value Function Approximation}
\date{September 16th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}


% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\begin{document}
	\maketitle
	
	\section{Value Function Approximation: General Ideas}
	
	We are going to focus first on solving dynamic programs with \emph{large state spaces} and \emph{large horizons}, assuming that there are a small number of actions and disturbances.
	
	The key to value function approximation methods is that if we have the optimal value function $v\opt$, then it is trivial to construct the optimal policy $\pi\opt$. The following observation summarizes this fact.
	
	\begin{obs}
		When $v\opt = (v\opt_t)_{t=0}^T$ is known then the optimal policy $\pi\opt = (\pi_t\opt)_{t=0}^{T-1}$ is easy to construct (even online) when $\actions$ and $\dist$ are small by solving the following optimization problem:
		\begin{equation} \label{eq:piopt_construction}
			\pi_t\opt(s) \;\in\; \arg\max_{a\in\actions} \; \Exx{W_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a,W_t)) } 
		\end{equation}
		for any current state $s\in\states$.
	\end{obs}
	
	
	The main idea of value function approximation is to replace the optimal value function by an approximation. Note that the value function represents the expected rewards you get starting from a particular state and can thus capture the potential for future rewards for the particular action. We now define the notion of a greedy policy which is constructed from an arbitrary value function.
	
	\begin{defn}[Greedy Policy]
		We call a policy $\tilde\pi = (\tilde\pi_t)_{t=0}^{T-1}$ to be greedy (with respect) to a value function $\tilde{v} = (\tilde{v}_t)_{t=0}^T$ when it satisfies that
		\begin{equation} \label{eq:greedy_construction}
			\tilde\pi_t(s) \;\in\; \arg\max_{a\in\actions} \; \Exx{W_t}{r_t(s,a, W_t) + \tilde{v}_{t+1}(f_t(s,a,W_t)) } 
		\end{equation}
		for each state $s\in\states$.
	\end{defn}		
	
	The greedy policy can either be constructed ahead of the execution, but typically one would construct it during the execution unless there are time constraints on picking the correct action.
	
	The optimization problem in~\eqref{eq:greedy_construction} can be difficult for large $\actions$ (computing the optimum) and $\dist$ (computing the expectation). The optimization problem is also hard to solve in reinforcement learning settings in which the probabilities of the disturbances are not known. In such cases, it is preferable to construct the optimal policy from the state-action value function ($q$ function) $\tilde{q} = (\tilde{q}_t)_{t=0}^{T-1}$ as
	\[
	\tilde\pi_t(s) \;\in\; \arg\max_{a\in\actions} \; \tilde{q}(s,a)~,
	\]
	for each $s\in\states$.
	
	
	\begin{algorithm}
		Compute approximate value function $\tilde{v}$ for all $s$ and $t$ \;
		Construct the greedy policy $\tilde{\pi}$ \;
		\Return $\tilde{\pi}$ \;
		\caption{Value Function Approximation} \label{alg:value_function_approx}
	\end{algorithm}
	
	
	The general outline of the value function approximation approach is summarized in \cref{alg:value_function_approx}. We will discuss how to approximate the value function next.
	
	\paragraph{General Approaches to Approximating Value Function}
	
	There are numerous general approaches for computing the approximate value function $\tilde{v}$. There are many variations, extensions, and improvements to each general approach.
	\begin{enumerate}
		\item \emph{Lookahead}: Reduce the horizon $T$ to reduce possible states that are visited. (There are many extensions, including Monte-Carlo Tree Search, LAO*, and others.)
		\item Solve a simpler problem. The construction of the simpler problem heavily depends on the structure of the problem we want to solve. Some examples are:
		\begin{enumerate}
			\item Decompose the problem if it consists of several weakly interacting parts (such as each customer is treated independently)
			\item Assume that the stochastic disturbances are deterministic (model predictive control is an example of this)
			\item Assume that the future is known and sample multiple possible futures (information relaxation, hindsight optimization are some examples)
			\item \ldots
		\end{enumerate}
		\item Assume (or prove) a specific form of the value function. Fitted Value Iteration is a common example of this approach. We will cover these types of methods in detail in the coming couple of weeks.
	\end{enumerate}
	
	\section{Multi-step Lookahead}
	
	The main idea in this approach is to reduce the horizon $T$ to reduce the number of states visited.
	
	Lets define the lookahead policy formally. The method will require significant computation before making a move. We will also assume that we have some approximate value function $\bar{v} = (\bar{v}_t)_{t=0}^T$. In many cases, it is OK to assume that this function is zero: $\bar{v}_t(s) = 0$ for all $s\in\states$ and $t = 0,\ldots, T$. 
	
	We are going to assume some fixed time step $t = 0, \ldots, T$ and lookahead length $L$. We also assume that $t + L \le T$ to avoid additional complications. At the end of the horizon $T$, one would simply reduce the lookeahed distance $L$ in order to satisfy that $t + L \le T$. The approximate value function is computed to satisfy:
	\begin{equation} \label{eq:la_values}
		\begin{aligned}
			\tilde{v}_{t + L}(s_{t+L}) \;&=\;   \bar{v}_{t + L}(s_{t + L}), \quad &&\forall s\in\tilde\states_{t + L} \\
			\tilde{v}_{t + l}(s_{t+l})    \;&=\;   \max_{a\in\actions} \Ex{r_{t+l}(s_{t+l},a, W_{t+l}) + \tilde{v}_{t+l+1}(f_{t+l}(s_{t+l},a,W_{t+l}))} \quad &&\forall s\in\tilde\states_{t + l}~,
		\end{aligned}
	\end{equation}
	for each $l = 1, \ldots, L-1$. The sets $\tilde\states_{t+l} \subseteq \states$ satisfy for each $l = 1, \ldots, L$:
	\begin{equation} \label{eq:la_sets}
		\begin{aligned}
			\tilde\states_{t} &\;=\; \{s_t\} \\
			\tilde\states_{t+l} &\;=\; \left\{ f_{t+l-1}(s_{t+l-1},a_{s+l-1},w_{t+l-1}) \mid s_{t+l-1} \in \tilde\states_{t+l-1},\, a \in \actions, \, w \in \dist \right\}~.
		\end{aligned}
	\end{equation}
	
	The lookahead algorithm works in two phases and is summarized in \cref{alg:lookahead}.
	
	
	\begin{algorithm}
		\KwIn{Dynamic program specification, Lookahead distance $L$, Current state $s_t$, optionally $\bar{v}$}
		\KwOut{Action to take $a$ for the current state}
		
		\tcp{Forward pass: Construct sets $\tilde\states_{t+l}$}
		Construct $\tilde\states_t$ according to~\eqref{eq:la_sets} \;
		\For{$l = 1, \ldots, L$}{
			Construct $\tilde\states_{t+1}$ according to~\eqref{eq:la_sets} \;
		}
		Compute $\tilde{v}_{t+L}$ according to~\eqref{eq:la_values} \;
		\tcp{Backward pass: Construct values $\tilde{v}_{t+l}$}
		
		\For{$l = L-1, \ldots, 1$}{
			Compute $\tilde{v}_{t+l}$ according to~\eqref{eq:la_values} \;
		}
		\Return $a \in \arg\max_{a\in\actions} \; \Exx{W_t}{r_t(s,a, W_t) + \tilde{v}_{t+1}(f_t(s,a,W_t)) } $
		\caption{$L$-step Lookahead} \label{alg:lookahead}
	\end{algorithm}
	
	\begin{prop}
		The computational complexity of \cref{alg:lookahead} is $\mathcal{O}((|\actions|\cdot|\dist|)^L)$.
	\end{prop}
	
	%	\bibliographystyle{plain}
	%	\bibliography{biblio}
	
\end{document}