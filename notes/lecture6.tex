\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{mathabx}        % for \bigtimes
\usepackage{booktabs}
\usepackage{mathtools}

% diagram stuff
\usepackage{pgf}
\usepackage{tikz}

\usetikzlibrary{arrows,automata}

\lectureNotesTitle{Lecture 6: Rollouts}
\date{September 21th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}


% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\begin{document}
    \maketitle

	\section{Multistep Lookahead}

    The lookahead method that we discussed last time is a heuristic method and has been proven to work well in many settings, but it is not appropriate in many problems.

    In problems with large action spaces $\mathcal{A}$ and disturbance spaces $\mathcal{W}$, the computational complexity of even small lookaheads becomes prohibitive.

    On the other hand, even when the lookahead is feasible computationally, it may not give rise to a good policy. In particular, when the reward or penalty is delayed beyond the horizon then the lookahead policy will be unable to anticipate it.

    \section{Rollout}

    Rollout is a method that can alleviate some of the limitations of multistep lookahead. It relies on there being a heuristic policy that is used to estimate the optimal value function.

    Recall that the goal in value function approximation is to compute an approximate value function $\tilde v = (\tilde v_t)_{t=0}^T$ that approximates the optimal value function $v\opt$ as closely as possible. In the rollout method, we will use the simulated returns that come from a policy $\tilde\pi = (\tilde\pi_t)_{t=0}^T$ to construct the approximate value function.

    \subsection{Idealized Rollout}

    First, lets discuss an idealized version of the rollout algorithm and then we show how it can be modified to get a practical method. In the remainder of the section, we will use $\tilde\pi$ to denote the heuristic policy and $\bar{\pi}$ to denote the policy that is computed by the rollout algorithm. The idealized rollout policy $\bar\pi$ is constructed for each state $s\in\states$ as:
    \begin{equation} \label{eq:rollout_idealized}
        \begin{aligned}
            \bar\pi_t(s) &\;\in\; \arg\max_{a\in \actions} \Exx{W_t}{r_t(s,a,W_t) + \tilde{v}_t(f_t(s, a, W_t))} \\
            \tilde{v}_t(s') &\;=\; v_t(s'; \tilde\pi) , \quad \forall s'\in\states~.
        \end{aligned}
    \end{equation}
    It will be convenient later to use an alternative, but equivalent, definition of the rollout using the q-functions:
    \begin{equation} \label{eq:rollout_idealized2}
        \bar\pi_t(s) \;\in\; \arg\max_{a\in \actions} q_t(s,a;\tilde\pi)~.
    \end{equation}

    The following theorem shows that the idealized rollout algorithm is guaranteed to perform no worse than the heuristic policy $\tilde\pi$. This concept is also known as the policy improvement and will play an important role in the several future algorithms, including the well-known policy iteration.
    \begin{theorem} \label{thm:improvement}
        Let $\bar{\pi}$ be defined as in~\eqref{eq:rollout_idealized}. Then:
        \[
            v_t(s; \bar{\pi}) \;\ge\; v_t(s;\tilde{\pi})
        \]
        for each $s\in\states$ and $t = 0, \ldots, T$.
    \end{theorem}
    It is important to note that \cref{thm:improvement} implies that the return of the policy $\bar{\pi}$ is no worse than the return of $\tilde\pi$. However, the result is not completely trivial since the improvements in~\eqref{eq:rollout_idealized} happen a single state at a time, ignoring the possible improvements in other states.

    \begin{proof}
        We will (of course) prove the result by backward induction. The \emph{base case} follows directly from the definition:
        \[
        v_T(s, \bar\pi) = r_T(s) = v_T(s, \tilde\pi)~.
        \]
        For the \emph{inductive case}, assume that the theorem holds for $t+1$ and we need to show it for $t$. That is, we know that
        \begin{enumerate}
            \item[(a)] $ v_{t+1}(s; \bar{\pi}) \;\ge\; v_{t+1}(s;\tilde{\pi})$ for all $s\in \states$, and
            \item[(b)] $q_t(s, \bar\pi_t(s); \tilde\pi) \ge q_t(s, \tilde\pi_t(s); \tilde\pi)$
        \end{enumerate}
        The property (b) above is true from the construction of $\bar{\pi}$ in~\eqref{eq:rollout_idealized} and, in particular, the alternative definition in~\eqref{eq:rollout_idealized2}. Using the two properties above, we get by algebraic manipulation for each $s\in\states$ that
        \begin{align*}
            v_t(s; \bar\pi) &= q_t(s, \bar{\pi}(s); \bar\pi) = \Exx{W_t}{r_t(s,\bar{\pi}(s),W_t) + v_t(f_t(s, \bar{\pi}(s), W_t); \bar{\pi})} \\
            &\ge \Exx{W_t}{r_t(s,\bar{\pi}(s),W_t) + v_t(f_t(s, \bar{\pi}(s), W_t); \tilde{\pi})} && \text{from (a)} \\
            &= q_t(s, \bar{\pi}(s); \tilde\pi) \\
            &\ge q_t(s, \tilde{\pi}(s); \tilde\pi)  && \text{from (b)} \\
            &= v_t(s; \tilde\pi)~.
        \end{align*}
        Along with the property (a), we also used the fact that the expectation operator is monotone: $X \le Y \Rightarrow \Ex{X} \le \Ex{Y}$. This result establishes our desired property.
    \end{proof}

    \subsection{Practical Rollout}

    The rollout algorithm shown in equation~\eqref{eq:rollout_idealized} is impractical because computing the value $v_t(s',\tilde\pi)$ is very difficult in large problems. Instead of computing this value exactly, we will simulate the policy and compute $\tilde{v}_t(s')$ as the average of the simulated runs. \cref{alg:monte_carlo} depicts the Monte-Carlo algorithm that estimates, by simulation, the value function of a state for any policy. The actual rollout algorithm is summarized in \cref{alg:rollout}.

        \begin{algorithm}
    	\KwIn{A policy $\tilde{\pi} = (\tilde\pi_t)_{t=0}^T$ with $\tilde\pi_t: \states\to\actions$ to evaluate, current state $\bar{s}$, current time step $\bar{t}$, number of simulations $M > 0$}
    	\KwOut{An estimate of the value function $\tilde v_{\bar{t}}(\bar{s}) \approx v_{\bar{t}}(\bar{s}, \tilde{\pi})$}
    	\tcp{Use policy rollouts (simulations) to estimate $\tilde v$}
    	\For{$i = 1, \ldots, M$}{
    		$s^i_{\bar{t}} \gets \bar{s}$ \tcp*{Initial state}
    		\For{$t = \bar{t}, \ldots, T-1$}{
    			Sample $w_t^i \sim p_t$ \;
    			$s_{t+1}^i \gets f_t(s_t, \tilde{\pi}_t(s_{t}^i), w_t^i)$ \;
    			$r_{t}^i \gets r_t(s_t, \tilde\pi_t(s_t^i), w_t^i) $\;
    		}
    		$r_T^i \gets r_T(s_T^i)$ \;
    	}
    	$\tilde{v}_{\bar{t}}(\bar{s}) \gets \frac{1}{M} \sum_{i=1}^M \left( \left(
    	\sum_{t=\bar{t}}^{T-1} r_t^i\right) + r_T^i
    	\right)$\;
    	\Return{$\tilde{v}_{\bar{t}}(\bar{s})$}\;
    	\caption{Monte-Carlo (rollout) Value Function Estimate} \label{alg:monte_carlo}
    \end{algorithm}

    \begin{algorithm}
        \KwIn{A heuristic policy $\tilde{\pi} = (\tilde\pi_t)_{t=0}^{T-1}$ with $\tilde\pi_t: \states\to\actions$, current state $\bar{s}$, current time step $\bar{t}$, number of simulations $M > 0$}
        \KwOut{An action to take: $a\in\actions$, such that $a = \bar\pi(s)$}
        \tcp{Determine states $\states'$ for which we need to know $\tilde{v}_t(s';\tilde\pi), s'\in\states'$}
        $\states' \gets \{f(\bar{s},a,w) \mid a\in\actions, w \in \dist\}$ \;
        \tcp{Use policy rollouts (simulations) to estimate $\tilde v$}
        \For{$s'\in\states'$}{
			Use \cref{alg:monte_carlo} with $M$ simulations to compute
            $\tilde{v}_{\bar{t}+1}(s')$ \;
        }
        \tcp{Compute the rollout policy $\bar\pi$}
        $\bar\pi_{\bar{t}}(\bar{s}) \in \arg\max_{a\in \actions} \Exx{W_t}{r_{\bar{t}}(s,a,W_t) + \tilde{v}_{\bar{t}+1}(f_t(s, a, W_t))}$\;
        \Return{$\bar\pi_{\bar{t}}(\bar{s})$}\;
        \caption{Rollout Optimization: Single step} \label{alg:rollout}
    \end{algorithm}




	The following notion will be necessary in order to show that the rollout algorithm improves on the value of the heuristic policy $\tilde{\pi}$.
	\begin{defn} \label{def:improving}
		We say that an approximate value function $\tilde v = (\tilde v_t)_{t=0}^T$ is \emph{sequentially improving} if
		\[
		\max_{a\in\actions}\, \Exx{W_t}{r_t(s, a, W_t) + \tilde v_{t+1}(f_t(s, a, W_t))} \ge \tilde v_t(s)
		\]
		for each $s\in\states$ and $t = 0, \ldots, T$.
	\end{defn}

	\cref{def:improving} requires that the value function estimates only get better over time.

	When the approximate value function corresponds exactly to a value function of a policy, then it can be easily shown to be sequentially improving.
	\begin{prop}
		When $\tilde{v}_t(s) = v_t(s, \tilde{\pi})$ for some policy $\tilde{\pi}\in\Pi_D$ and all $s\in\states$ and $t=0, \ldots, T$, then $\tilde{v}$ is sequentially-improving.
	\end{prop}

	Note that if $\tilde{\pi}$ was history-dependent, then there $v(\cdot, \tilde{\pi})$ may not be sequentially improving. Fortified rollouts are one approach to rectify that problem.

	\begin{theorem}\label{thm:improvement2}
		The rollout policy $\bar{\pi}$ constructed from a self-improving heuristic value $\tilde{v}$ is an improving policy:
		\[
		v_t(s, \bar{\pi}) \;\ge\; \tilde{v}_t(s)
		\]
		for all $s\in\states$ and $t = 0,\ldots,T$.
	\end{theorem}
	\begin{proof}
		Follows as a simple extension of the proof of \cref{thm:improvement}.
	\end{proof}


%	\bibliographystyle{plain}
%	\bibliography{biblio}

\end{document}