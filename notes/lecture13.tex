\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{url}
\usepackage{bbm}
\usepackage{bm}


\usepackage{fullpage}

\renewcommand{\*}{\bm}

\lectureNotesTitle{Lecture 13: Solving Markov Decision Processes}
\date{October 26th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
	\maketitle

	\section{Introduction} \label{sec:intro}

	In addition to Chapter 4 in RLOC, I recommend that you also consult the following other resources. You can find a rigorous and clear treatment of the infinite horizon discounted MDPs in Chapter 6 of
	\begin{quote}
		Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. Wiley-Interscience.
	\end{quote}
	I suggest that you also take a look at Chapter 3 of
	\begin{quote}
		Sutton, R. S., \& Barto, A. G. (2018). Reinforcement learning: An introduction (2nd ed.). The MIT Press.
	\end{quote}
	The second book's pdf is available online and can be accessed using a link from the class website.

	This lecture continues the coverage of the foundations of infinite horizon models, targets the infinite horizon. We then also introduce the Markov Decision Process, which is the model used to optimize actions in sequential environments.

	\section{Vector Norms}

	A vector norm is a mapping from a vector space to a real value which represents the length of the vector. The norm of $\*x\in\Real^N$ is typically denoted as $\|\*x\|$ and satisfies the following properties:
	\begin{enumerate}
		\item Triangle inequality: $\| \*x + \*y \| \le \| \*x \| + \| \*y\|$ for each $\*x,\*y \in \Real^N$
		\item Absolute homogeneity: $\| c \cdot \*x \| = |c| \| \*x \|$ for each $\*x \in \Real^N$ and a scalar $c$
		\item Positive definiteness: If $\| \*x \| = 0$ then $\*x = \*0$ for each $\*x\in\Real^N$
	\end{enumerate}

	The main norms that we will be using are:
	\begin{enumerate}
		\item $L_2$ norm:
		\[
		\| \*x \|_2 = \sqrt{\*x\tr \*x}
		\]
		\item $L_\infty$ norm:
		\[
		\| \* x \|_\infty = \max_{i = 1, \ldots, N} |x_i|
		\]
	\end{enumerate}

	\section{Markov Decision Process}

	A Markov decision process consists of:
	\begin{enumerate}
		\item States: $\states$
		\item Actions: $\actions$
		\item Transition probabilities: $P: \states \times \actions \times \states \to [0,1]$ or $P: \states \times \actions \to \Delta^S$
		\item Reward function: $r:\states \times \actions \to \Real$
		\item (optional) Initial distribution $p_0 \in \Delta^S$
	\end{enumerate}

	The solution to a Markov decision process is a policy. There are many types of policies. The main types that we will consider are:
	\begin{enumerate}
		\item Markov Deterministic Policies: $\pi:\states\times\mathcal{T} \to \actions$, where $\mathcal{T} = \{0, \ldots, T-1\}$ for some horizon $T$. We will denote the set of Markov policies by $\Pi_M$.
		\item Stationary Deterministic Policies: $\pi: \states \to \actions$. We will denote the set of stationary policies by $\Pi_D$.
	\end{enumerate}

	As before, we assume a \emph{discount factor} $\gamma \in [0,1)$. The discount factor must be less than $1$ in order to avoid infinite value functions. That is, if the sequence of rewards received is $R_0^\pi, R_1^\pi, \dots$, then the objective is to compute a policy that solves
	\[
	\max_\pi \, \Ex{\sum_{t = 0}^{\infty} \gamma \cdot R_t^\pi}~.
	\]


	In this lecture, we drop the subscript for the value functions. All value functions are computed for the infinite horizon. That is, we use $v = v_{0,\infty}$.

	In the remainder of the course, we will use $v^\pi:\states \to\Real$ to denote the value function for a stationary deterministic policy $\pi$. The value function is formally defined as follows:
	\begin{defn}
		Consider an MDP with discount factor $\gamma\in[0,1]$. The infinite-horizon discounted value function $v^\pi : \states \to \Real$ for any stationary policy $\pi$ is defined as
		\[
		v^\pi(s) \;=\; \Ex{\sum_{t=0}^\infty \gamma^{t} \cdot r(S_t, \pi(S_t)) ~\middle|~ S_0 = s, S_{t+1} \sim P(S_t, \pi(S_t), \cdot)}
		\]
		for each $s\in\states$.
	\end{defn}


	We will use $L^\pi$ to denote the Bellman operator for the Markov reward process induced by the stationary policy $\pi$. Recall that the value function $\*v^\pi$ (vector) for the policy $\pi$ satisfies:
	\[
	\*v^\pi \;=\; L^\pi \*v^\pi~.
	\]

	The \emph{optimal} value function $v\opt: \states \to \Real$ is defined as
	\begin{equation}\label{eq:optimal_policy_def}
	v\opt(s) \;=\; \max_{\pi \in \Pi_D} v^\pi(s)~,
	\end{equation}
	for each $s\in\states$.

	\begin{theorem}[Optimal Policy]
	The exists an optimal deterministic stationary policy $\pi\opt$, such that
	\[
	v^{\pi\opt}(s) \;=\; v\opt(s)~,
	\]
	for each $s\in\states$.
	\end{theorem}

	\section{Markov Decision Process: Bellman Operator}

	As with a fixed policy, we will define a Bellman operator.
	\begin{defn}[Bellman Operator]
		The Bellman operator $L:\Real^S \to \Real^S$ for a Markov decision process is defined for any $\*x \in \Real^S$ as
		\[
		(L \*x)(s) \;=\; \max_{a\in\actions} \, \left\{r(s,a) + \gamma \cdot \sum_{s'\in \states} P(s,a,s') x(s') \right\}~.
		\]
	\end{defn}

	In comparison with the Bellman operator $L^\pi$ for a fixed policy $\pi$, the MDP Bellman operator $L$ is not linear or affine.


	An important concept that we need is the notion of the $L_\infty$ norm, which is defined for any $\*x\in\Real^S$ as:
	\[
	\|\*x\|_\infty = \max_{i = 1, \ldots, S} |x_i|~.
	\]

	The Bellman operator is a contraction under the $L_\infty$ norm.
	\begin{theorem}
		The Bellman operator $L:\Real^S \to \Real^S$ is a $\gamma$-contraction in the $L_\infty$ norm. That is,
		\[
		\| L \*x - L \*y \|_\infty \;\le\; \gamma\cdot \| \*x - \*y \|_\infty~,
		\]
		for any $\*x, \*y \in \Real^S$.
	\end{theorem}
	See Proposition 6.2.4 in \cite{Puterman2005} for the proof.

	Because the Bellman operator is a contraction, we can employ the well-known Banach fixed point theorem, which is stated next.
	\begin{theorem}[Banach Fixed Point Theorem, Proposition 6.2.3 in \cite{Puterman2005}] \label{thm:banach}
		Suppose that $\mathcal{U}$ is a Banach space and $T : \mathcal{U} \to \mathcal{U}$ is a contraction mapping for some norm. Then:
		\begin{enumerate}
			\item There exists a unique fixed point $\*v\opt \in \mathcal{U}$ such that $T \*v\opt = \*v\opt$, and
			\item For arbitrary $\*x\in\mathcal{U}$ we have that
			\[
			\lim_{n\to\infty} T^n \*x = \*v\opt~.
			\]
		\end{enumerate}
	\end{theorem}

	The optimal value function in an MDP must satisfy the following Bellman optimality equation.
	\begin{theorem}
		The optimal value function $\*v\opt \in \Real^S$ in a Markov decision process satisfies:
		\[
			\*v\opt \;=\; L \*v\opt~.
		\]
		In addition, the solution to the fixed point equation is unique.
	\end{theorem}

	As with the finite-horizon setting, we will be using greedy policies, which are defined as follows.
	\begin{defn}[Greedy Policy]
		We call a stationary policy $\tilde\pi: \states\to\actions$ to be greedy (with respect) to a value function $\tilde{v} \in \Real^S$ when it satisfies that
		\begin{equation} \label{eq:greedy_construction}
			\tilde\pi(s) \;\in\; \arg\max_{a\in\actions} \; r(s,a) + \gamma \cdot \sum_{s'\in \states} P(s,a,s') \tilde v(s')
		\end{equation}
		for each state $s\in\states$.
	\end{defn}


	\section{Computing Value Function}

	We now consider two methods for computing the value function for a Markov decision process using the properties established above.


	The first algorithm is an iterative method based on iterating the computation of the Bellman operator and it is summarized in \cref{alg:value_iteration}. The algorithm runs a number of iterations, and in each one, it updates the value function. It is given a number of iterations and outputs an approximation of $\*v_{0,\infty}$. The approximation gets better with an increasing number of iterations $N$.

	\begin{algorithm}
		\KwIn{Initial value function $\*x\in \Real^S$, and number of iterations $N$}
		\KwOut{An approximation of the value funtion $\*v\opt$}
		$\*v_0 \gets \*x$\;
		\For{$n \in 1, \ldots, N$}{
			$\*v_n \gets L \*v_{n-1}$\;
		}
		\Return{$\*v_N$}\;
		\caption{Value iteration algorithm.}
		\label{alg:value_iteration}
	\end{algorithm}

	\cref{alg:value_iteration} is also known as the Jacobian version of value iteration. See Chapter 6 of \cite{Puterman2005} for a Gauss-Seidel version of value iteration. Value iteration only converges to the optimal value function $\*v\opt$ in the limit. We can, however, terminate the computation when the solution gets close. The following result can be used to bound the error.
	\begin{theorem}
		Suppose that $\*v\opt \in \Real^S$ is an arbitrary value function. Then:
		\[
		\| \*v\opt - \*v\|_\infty \;\le\; \frac{1}{1-\gamma}\| L \*v - \*v \|_\infty~.
		\]
	\end{theorem}
	\begin{proof}
		We will prove the result using the contraction property of the Bellman operator and the triangle inequality.
		\begin{align*}
			\| \*v\opt - \*v\|_\infty &= \| \*v\opt - L \*v + L \*v  - \*v\|_\infty \\
			&\le \| \*v\opt - L \*v \|_\infty + \| L \*v  - \*v\|_\infty \\
			&\le \| L\*v\opt - L \*v \|_\infty + \| L \*v  - \*v\|_\infty \\
			&\le \gamma \cdot \| \*v\opt - \*v \|_\infty + \| L \*v  - \*v\|_\infty ~.
		\end{align*}
		Subtracting $\gamma \cdot \| \*v\opt - \*v \|_\infty$ from both sides and dividing by $1-\gamma$ yields the desired inequality.
	\end{proof}

	Another popular approach to solving MDPs is to use policy iteration, which is summarized in \cref{alg:policy_iteration}

	\begin{algorithm}
		\KwOut{Value function $\*v\opt$, $\pi\opt$}
		$n\gets 0$\;
		$\pi_0 \gets$ an arbitrary initial policy \;
		\Repeat{$\pi_n = \pi_{n-1}$}{
			\tcp{Policy evaluation: Compute the value function}
			$\*v_n \gets \*v^{\pi_n} = (\*I - \gamma\cdot \*P^\pi)^{-1} \*r^\pi$\;
			\tcp{Policy improvement: Compute the greedy policy}
			$\pi_{n+1} \gets \pi$ such that $L^\pi \*v_n = L \*v_n$\;
			$n \gets n + 1$\;
		}
		\Return{$\*v_n, \pi_n$}\;
		\caption{Policy iteration.}
		\label{alg:policy_iteration}
	\end{algorithm}


	\bibliographystyle{plain}
	\bibliography{biblio}
\end{document}

