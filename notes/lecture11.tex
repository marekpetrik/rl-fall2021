\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{url}
\usepackage{bbm}
\usepackage{bm}


\usepackage{fullpage}

\renewcommand{\*}{\bm}

\lectureNotesTitle{Lecture 11: Infinite Horizon Models: Policy Evaluation}
\date{October 19th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
	\maketitle
	
	\section{Introduction} \label{sec:intro}
	
	In addition to Chapter 4 in RLOC, I recommend that you also consult the following other resources. You can find a rigorous and clear treatment of the infinite horizon discounted MDPs in Chapter 6 of
	\begin{quote}
		Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. Wiley-Interscience.
	\end{quote}
	I suggest that you also take a look at Chapter 3 of
	\begin{quote}
		Sutton, R. S., \& Barto, A. G. (2018). Reinforcement learning: An introduction (2nd ed.). The MIT Press.
	\end{quote}
	The second book's pdf is available online and can be accessed using a link from the class website.
	
	This lecture covers the foundations of infinite horizon models. We first focus on models that assume a fixed policy and aim to compute its value function. The two models that we consider here are the Markov chain and the Markov reward process. We will cover Markov decision processes, which allow for optimizing actions, in the next lecture.
	
	In comparison to the dynamic programming models we considered in the earlier lectures, we will assume that all components of the models are independent of time. That is, transition functions and rewards are all independent of time. Clearly, time independence is convenient when scaling the dynamic programming models to infinite horizon problems.
	
	\section{Markov Chains}
	
	A Markov chain is a stochastic model that describes a sequence of random events. The sequence of random events is often referred to as a \emph{process}.
	
	A Markov chain consists of the following components:
	\begin{enumerate}
		\item State set: $\states$. We will assume that this set is \emph{finite} for now to simplify the exposition.
		\item Transition probability function (or a matrix): $P : \states \times \states \to \Real$ or $P:\states \to \Delta^\states$, where $\Delta^\states$ denotes the set of transition probabilities over states $\states$. Here, $P(s_1,s_2)$ represents the probability of transitioning from $s_1$ to $s_2$.
	\end{enumerate}
	
	
	A Markov chain can model a sequence of random events $S_0, S_1, \ldots$ if it satisfies the Markov property. The Markov property states that the future evolution of the process is independent of its past evolution conditional on the present. In other words, it also implies that knowing the current state $S_t$ is sufficient to predict the future, and the values of $S_0, \ldots, S_{t-1}$ is irrelevant.
	\begin{defn}[Markov property]
		A sequence of random variables $S_0, S_1, \ldots$ satisfies the Markov property if
		\[
		\Pr{S_{t+1} = s_{t+1} \mid S_t = s_t} = \Pr{S_{t+1} = s_{t+1} \mid S_0 = s_0, \ldots, S_t = s_t}
		\]
		for any $t\ge 0$.
	\end{defn}
	
	A sequence of random events that satisfies the Markov property with constant probabilities can be described using a Markov chain with the transition function $P$ constructed as:
	\[
	P(s,s') = \Pr{S_t+1 = s' \mid S_t = s}
	\]
	for all $t = 0, \ldots, T-1$ and $s,s'\in \states$.
	
	It is easy to see that the evolution of states in $S_0, S_1, \ldots$ a dynamic program of the type we considered earlier can be modeled as a Markov chain, as long as the transition function is time-independent. Consider a dynamic program with some policy $\pi:\states\to\actions$, transitions function $f$, and reward function $r$. The transition function for an equivalent Markov chain is constructed such that for each $s,s'\in\states$ we have
	\[
	P(s, s') = \sum_{w\in\dist}\Pr{W = w} \mathbbm{1}\{f(s, \pi(s), w) = s'\}~.
	\]
	The symbol $\mathbbm{1}$ represents the indicator function which is $1$ when its argument is true and $0$ otherwise.
	
	\section{Markov Reward Process: Discounted return}
	
	A Markov reward process augments a Markov chain with:
	\begin{enumerate}
		\item A reward function $r:\states\to\Real$.
	\end{enumerate}
	
	We will be interested in infinite-horizon discount return as a way of combining the rewards received in multiple time periods. As with a dynamic program, we define the value function as follows.
	\begin{defn}
		Consider a discount factor $\gamma\in[0,1]$ and a sequence of states $S_0, S_1 \ldots, $ generated by a Markov chain. The value function $v_{t,T} : \states \to \Real$ for the initial time step $t \le T$ and a horizon $T \ge 0$ is defined as:
		\[
		v_{t,T}(s) \;=\; \Ex{\sum_{l=t}^T \gamma^{l-t} r(S_l) \mid S_t = s}
		\]
		for each $s\in\states$.
	\end{defn}
	
	
	As with a dynamic program, the value function satisfies a dynamic programming equation, also known as the Bellman equation. It will be convenient to represent the Bellman equation and value function using linear algebra. Assume that $|\states| = S$ and some arbitrary but fixed order of states $s_1, s_2, \ldots, s_S \in \states$. We will use $\*v_{t,T}\in \Real^S$ (bold) to denote the vector:
	\[
	\*v_{t,T} \;=\; \begin{pmatrix}
		v_{t,T}(s_1) \\
		v_{t,T}(s_2) \\
		\vdots \\
		v_{t,T}(s_S)
	\end{pmatrix}~.
	\]
	We will also use $\*P \in\Real^{S\times S}$ to denote the matrix of transition probabilities as
	\[
	\*P \;=\; \begin{pmatrix}
		P(s_1, s_1) & P(s_1, s_2) & \ldots & P(s_1, s_S) \\
		P(s_2, s_1) & P(s_2, s_2) & \ldots & P(s_2, s_S) \\
		\ldots & \ldots \\
		P(s_S, s_1) & \ldots & \ldots & P(s_S, s_S) 
	\end{pmatrix}~.
	\]
	Finally, $\*r \in \Real^S$ denotes the vector of rewards.
	
	As with earlier dynamic programs, we can show that the value function satisfies the following dynamic programming 
	\begin{theorem}[Bellman equation] \label{thm:Bellman}
		The value function $v_{t,T}:\states \to \Real$ for $0 \le t < T$ of a Markov reward process satisfies for each $s\in\states$:
		\begin{align*}
			v_{t,T}(s) &\;=\; r(s) + \gamma \cdot \sum_{s'\in \states} P(s,s') \cdot v_{t+1, T}(s') \\
			v_{T,T}(s) &\;=\; r(s)~.
		\end{align*}
		Using the matrix notation defined above, we get that
		\begin{align*}
			\*v_{t,T} &\;=\; \*r + \gamma \cdot \*P \*v_{t+1,T} \\
			\*v_{T,T} &\;=\; \*r~.
		\end{align*}
	\end{theorem}
	\begin{proof}
		The proof follows using the similar set of steps as the derivation of the dynamic programming equation for a stochastic dynamic program.
		\begin{align*}
			v_{t,T} &= \Ex{\sum_{l=t}^T \gamma^{l-t}(S_l) \mid S_t = s} \\
			&= \Ex{r(S_t) + \sum_{l=t+1}^T \gamma^{l-t}(S_l) \mid S_t = s} \\
			&= \Ex{r(S_t) + \gamma \sum_{l=t+1}^T \gamma^{l-t-1}(S_l) \mid S_t = s} \\
			&= \Ex{r(S_t) + \gamma \Ex{\sum_{l=t+1}^T \gamma^{l-t-1}(S_l)\mid S_{t+1} } \mid S_t = s} \\
			&= \Ex{r(S_t) + \gamma v(S_{t+1}) \mid S_t = s}~.
		\end{align*}
		The theorem then follows from the definition of the expectation operator and by algebraic manipulation.
	\end{proof}
	
	Finally, the following lemma will facilitate the analysis of what happens as the horizon increases.
	\begin{lemma} \label{lem:move}
		Suppose that $v_{t,T}:\states \to \Real$ is a value function for $0 \le t \le T$. Then, we have for each $s\in\states$ that:
		\[
		v_{t,T}(s) = v_{t+1, T+1}(s)~.
		\]
	\end{lemma}
	The lemma can be shown readily by backward induction. 
	
	Finally, using \cref{lem:move} we can restate \cref{thm:Bellman} as follows.
	\begin{cor}
		The value function $v_{t,T}:\states \to \Real$ for $0 \le t \le T$ of a Markov reward process satisfies for each $s\in\states$:
		\begin{align*}
			v_{t,T+1}(s) &\;=\; r(s) + \gamma \cdot \sum_{s'\in \states} P(s,s') \cdot v_{t, T}(s') \\
			v_{T,T}(s) &\;=\; r(s)~.
		\end{align*}
		Using the matrix notation defined above, we get that
		\begin{align*}
			\*v_{t,T+1} &\;=\; \*r + \gamma \cdot \*P \*v_{t,T} \\
			\*v_{T,T} &\;=\; \*r~.
		\end{align*}
	\end{cor}
\end{document}
