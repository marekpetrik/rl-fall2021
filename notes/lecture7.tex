\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{mathabx}        % for \bigtimes
\usepackage{booktabs}
\usepackage{mathtools}

% diagram stuff
\usepackage{pgf}
\usepackage{tikz}

\usetikzlibrary{arrows,automata}

\lectureNotesTitle{Lecture 7: Fitted Value Iteration}
\date{September 28th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\begin{document}
	\maketitle

	\section{Fitted Value Iteration}

	Fitted Value Iteration~(FVI) is a very general reinforcement learning method. We will discuss the finite horizon version of the algorithm for now. Later in the course, we will generalize this method to infinite horizon problems.

	As with the earlier methods, we will use FVI to compute an approximate value function $\tilde{v}_t, t=0,\ldots,T$  and then compute the \emph{greedy} policy $\tilde{\pi}$:
	\begin{equation}
		\tilde\pi_t(s) \;\in\; \arg\max_{a\in\actions} \; \Exx{W_t}{r_t(s,a, W_t) + \tilde{v}_{t+1}(f_t(s,a,W_t)) }
	\end{equation}
	for any state $s\in\states$.

	FVI builds on the basic dynamic programming method that we discussed earlier but generalizes the idea to problems with infinite state spaces. When the state space is infinite, it is impossible to represent each state's value function, not even compute it. To address this challenge, we will use machine learning techniques to generalize value functions from a small number of states to the entire state space.

	To avoid unnecessary complexity, this document describes a linear FVI, which uses linear value function approximation, also known as linear regression. However, the general idea of FVI generalizes easily to almost any machine learning method, such as K-NN, SVR, regression trees, neural networks, and others.

	In linear FVI, we assume that there exists a feature function $\phi: \states \to \Real^K$ which computes a $K$-dimensional feature vector. Given these features, we will \emph{assume} that the optimal value function can be approximated well as their linear combination.
	\begin{asm} \label{asm:linear_approximation}
		The optimal value function $v\opt_t : \states \to \Real$ can be approximated (well) as a linear combination of features generated by $\phi$. That is, for each $t = 0, \ldots, T-1$, there exists a vector $w_t \in \Real^K$ such that for each $s\in\states$:
		\[
		v\opt_t(s) \;\approx\; \tilde{v}_t(s) \;=\; \phi(s)\tr w_t = \sum_{i=1}^K \phi(s)_i (w_t)_i~.
		\]
	\end{asm}

	\cref{asm:linear_approximation} implies that feature function $\phi$ needs to return attributes of the state that can be linearly combined to define the optimal value function. Consider the following example.
	\begin{example}
		Consider the reservoir management problem with $\mathcal{S} = [0,1]$. The simplest possible feature function $\phi$ may be:
		\[
		\phi(s) = s,
		\]
		which simply means that the feature is the reservoir water level. This assumption means that we are assuming that the optimal value function will be linear in the water level, and the intercept will be zero. As we will see later, we can define features to allow for a wide range of possible representable value functions. For example, defining the features as
		\[
		\phi(s) = \begin{pmatrix}
			1 \\ s
		\end{pmatrix}
		\]
		means that we are assuming that the value function is linear with an arbitrary intercept. We will discuss this construction later.
	\end{example}

	We are now ready to discuss the actual FVI algorithm, which is summarized in \cref{alg:fvi}. Its main idea is simple. It proceeds by computing the approximate value function by iterating backward in time. Instead of computing the value function for all states (which is not possible because there are too many of them), it calculates the true value function update only for a small subset of states, which we call $(\hat{S}_t)_{t=0}^{T}$.

	The main idea of \cref{alg:fvi} is approximate the optimal value function by an approximate value function $\tilde{v}$ using state features $\phi$ and weights $w$ for $t+1$ as:
	\begin{equation} \label{eq:linear_approximation}
		\tilde{v}_{t+1}(s) = \phi(s)\tr \tilde{w}_{t+1} \quad \forall s\in\states~.
	\end{equation}
	Note that we never need to actually compute the function value for all states $s\in\states$. Instead, we compute $\tilde{v}_{t+1}(s)$ only when it is actually needed. Having computed $\tilde{v}_{t+1}$, we then estimate $\hat{v}(\hat{s})$ for some relatively small number of states $\hat{s}\in\hat{\states}$ as follows:
	\begin{equation}
		\hat{v}_t(\hat{s}) \gets \max_{a\in\actions}\, \Exx{W_t}{r_t(\hat{s},a, W_t) + \tilde{v}_{t+1}(f_t(\hat{s},a,W_t)) }, \quad \forall \hat{s}\in\hat\states_t~.
	\end{equation}
	The approximate value function $\tilde{v}_t$ is then computed using linear regression to match the sampled values $\hat{v}_t(\hat{s})$ as closely as possible.

	\begin{algorithm}
		\KwIn{Feature function $\phi: \states \to \Real^K$, sub-sampled states $\hat{\states}_t,\,t = 0, \ldots, T$}
		\KwOut{Parameters $(\tilde{w}_t)_{t = 0}^{T}$ such that $\tilde{v}_t(s) = \phi(s)\tr \tilde{w}_t$ for all $s\in\states$}
		\tcp{Compute $\tilde{w}_T$}
		$\hat{v}_{T}(\hat{s}) \gets r_{T}(\hat{s}), \quad \forall s\in\hat\states_T$\;
		$\tilde{w}_T \gets \arg\min_{w\in\Real^K} \sum_{\hat{s}\in\hat{\states}_T} (\phi(\hat{s})\tr w - \hat{v}_T{(\hat{s})})^2$ \;
		\tcp{Backward pass to compute $\tilde{w}_t, t=0, \ldots, T-1$}
		\For{$t = T-1, \ldots, 0$}{
			$\hat{v}_t(\hat{s}) \gets \max_{a\in\actions}\, \Exx{W_t}{r_t(\hat{s},a, W_t) + \phi(f_t(\hat{s},a,W_t))\tr \tilde{w}_{t+1}}$ for all $\hat{s}\in\hat{\states}_t$\;
			$\tilde{w}_t \gets \arg\min_{w\in\Real^K} \sum_{\hat{s}\in\hat{\states}_t} (\phi(\hat{s})\tr w - \hat{v}_t{(\hat{s})})^2$ \;
		}
		\Return{$\tilde{w}_t$ for $t = 0, \ldots, T$}
		\caption{Linear Fitted Value Iteration} \label{alg:fvi}
	\end{algorithm}
	Hopefully, this is clear, but it is worth repeating. We compute the policy from the output of FVI by computing the \emph{greedy} policy $\tilde{\pi}$ as
	\begin{equation} \label{eq:greedy_specific}
		\tilde\pi_t(s) \;\in\; \arg\max_{a\in\actions} \; \Exx{W_t}{r_t(s,a, W_t) + \phi(f_t(s,a,W_t))\tr \tilde{w}_{t+1} }
	\end{equation}
	for any state $s\in\states$.

	Note that FVI requires sample states $\hat{S}_t,t=0,\ldots,T$. For now, we will use randomly generated states. A good choice of the states will become much more important when working with infinite horizon problems. It is not as important in finite-horizon (finite $T$) problems.

	\section{Policy Evaluation using Fitted Value Iteration}

	In addition to policy optimization, FVI can also be used to estimate the value function $v_t(s;\pi)$ of some policy $\pi$. Using FVI for value function evaluation is less computationally demanding than using Monte Carlo estimation but requires features. With good features, FVI can outperform Monte Carlo methods. However, when the features are bad, FVI can perform very poorly.


	\begin{algorithm}
		\KwIn{Feature function $\phi: \states \to \Real^K$, sub-sampled states $\hat{\states}_t\subset \states$, policy $\pi_t$ for $t = 0, \ldots, T$}
		\KwOut{Parameters $(\tilde{w}_t)_{t = 0}^{T}$ such that $\tilde{v}_t(s) = \phi(s)\tr \tilde{w}_t \approx v_t(s;\pi)$ for all $s\in\states$}
		\tcp{Compute $\tilde{w}_T$}
		$\hat{v}_{T}(\hat{s}) \gets r_{T}(\hat{s}), \quad \forall s\in\hat\states_T$\;
		$\tilde{w}_T \gets \arg\min_{w\in\Real^K} \sum_{\hat{s}\in\hat{\states}_T} (\phi(\hat{s})\tr w - \hat{v}_T{(\hat{s})})^2$ \;
		\tcp{Backward pass to compute $\tilde{w}_t, t=0, \ldots, T-1$}
		\For{$t = T-1, \ldots, 0$}{
			$\hat{v}_t(\hat{s}) \gets \Exx{W_t}{r_t(\hat{s},\pi_t(\hat{s}), W_t) + \phi(f_t(\hat{s},\pi_t(\hat{s}),W_t))\tr \tilde{w}_{t+1}}$ for all $\hat{s}\in\hat{\states}_t$\;
			$\tilde{w}_t \gets \arg\min_{w\in\Real^K} \sum_{\hat{s}\in\hat{\states}_t} (\phi(\hat{s})\tr w - \hat{v}_t{(\hat{s})})^2$ \;
		}
		\Return{$\tilde{w}_t$ for $t = 0, \ldots, T$}
		\caption{Linear Fitted Value Iteration: Policy Evaluation} \label{alg:fvi}
	\end{algorithm}



	%	\bibliographystyle{plain}
	%	\bibliography{biblio}

\end{document}