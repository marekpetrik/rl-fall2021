
\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{url}
\usepackage{bbm}
\usepackage{bm}
\usepackage{graphics}

\usepackage{fullpage}

\renewcommand{\*}{\bm}

\lectureNotesTitle{Lecture 17: LSTDQ and Policy Optimization}
\date{November 11th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
\maketitle

\section{Introduction}

In this lecture, we will adapt LSTD to the policy optimization problem. When optimizing policies, it is common to approximate the q-function rather than the value function. The main reason for this is that one can construct a greedy policy from the q-function without knowing the transition probabilities $P$ or rewards $r$. Recall that constructing the greedy policy from the value function requires one to solve the Bellman operator.

Another reason to approximate the q-function is when there are many actions, this approximation approach can generalize among these actions assuming the greedy optimization problem can be solve easily.

For more details, I recommend that you consult \cite{Lagoudakis2003}.

\section{Q-function Basics}

In this section, we summarize the basic properties of q-functions that we will need to build policy optimization algorithms.

Recall that $q^{\pi}:\states\times\actions \to\Real$ for a deterministic stationary policy $\pi$ is defined as:
\[
  q^{\pi}(s,a) \;=\; r(s,a) + \gamma \cdot \sum_{s'\in\states} v^{\pi}(s')~.
\]
The q-function $q\opt:\states\times\actions \to \Real$ for the optimal policy is similarly defined as
\[
  q\opt(s,a) \;=\; r(s,a) + \gamma \cdot \sum_{s'\in\states} v\opt(s')~.
\]

The following theorem summarizes the properties of the q functions that we will need to construct optimization algorithms.
\begin{theorem} \label{thm:q_optimality}
Assume a discount factor $\gamma\in[0,1)$. Then the q-function $q^{\pi}:\states\times\actions \to\Real$ for some stationary deterministic policy $\pi:\states\times\actions\to\Real$ is the unique solution to
\[
q^\pi(s,a) = r(s,a) + \gamma \cdot \sum_{s'\in\states} p(s,a,s') q^\pi(s', \pi(s'))~,
\]
for all $s\in\states, a\in\actions$. Similarly, the q-function $q\opt:\states\times\actions \to \Real$ for any optimal policy $\pi\opt:\states\times\actions\to\Real$ is the unique solution to:
\[
q\opt(s,a) = r(s,a) + \gamma \cdot \sum_{s' \in\states} p(s,a,s') \max_{a'\in\actions} q\opt(s', a')~,
\]
for all $s\in\states, a\in\actions$.
\end{theorem}

\section{LSTD-Q}

This section now describes how to approximate the q-function of a given policy. This method is a direct extension of the LSTD ideas to this more general setting.

The features in this setting must be defined for both state and actions, and not only states. Assume thus the feature function $\phi:\states\times\actions\to\Real$. A common approach to defining a feature function for both states and actions from a state feature function is to simply duplicate the features for each action. That is, if the original state feature function has $K$ features, the state-action feature function will have $K\cdot A$ features. The following example illustrates such construction. 
\begin{example}
Consider a state feature function $\hat{\*\phi}:\states\to\Real$ and an MDP with 2 actions $\{a_1,a_2\}$. The state-action feature function $\*\phi:\states\times\actions\to\Real$ could be defined for each $s\in\states, a\in\actions$ as:
  \[
    \*\phi(s,a) = \begin{cases}(\hat{\*\phi}(s); \*0) & \mathrm{if} \quad a = a_1 \\
      ( \*0; \hat{\*\phi}(s)) & \mathrm{if} \quad a = a_2
    \end{cases}~.
  \]
\end{example}


When approximating value functions using vector notation, we needed to assume some ordering of states to express vectors over the state space. To express vector over the joint space of states and actions, assume an ordering of states and action that iterates first of states and then over actions: $(s_1,a_1), (s_2, a_1), \ldots, (s_S,a_1), (s_1, a_2), \ldots, (s_S, a_2), \ldots, (s_S,a_A)$. Here, $S$ is the number of states and $A$ is the number of actions.

With the ordering defined above, let $\*q^{\pi} \in \Real^{S\cdot A}$ be a vector representation of the q-function for a policy $\pi$. That is, we have:
\[
  \*q^{\pi} = 
\begin{pmatrix}
q^{\pi}(s_1,a_1) \\ q^{\pi}(s_2, a_1) \\ \vdots \\ q^{\pi}(s_S, a_A)
\end{pmatrix} =
\begin{pmatrix}
\*q^{\pi}(\cdot,a_1) \\ \*q^{\pi}(\cdot, a_2) \\ \vdots \\ \*q^{\pi}(\cdot, a_A)
\end{pmatrix}
~.
\]

The notation $\*q^{\pi}(\cdot,a)$ represents the vector across the dimension indicated by the dot. To construct the approximation, we will need to defined the following matrices:
\begin{align*}
\*A &= \begin{pmatrix} \*P(\cdot, a_1, \cdot) \\ \*P(\cdot, a_2, \cdot) \\ \vdots \\ \*P(\cdot,a_A,\cdot) \end{pmatrix} \in \Real^{S\cdot A \times S}&
\*b &= \begin{pmatrix} \*r(\cdot,a_1) \\ \*r(\cdot,a_2) \\ \vdots \\ \*r(\cdot,a_A) \end{pmatrix} \in \Real^{S\cdot A}&
\*\Phi &= \begin{pmatrix} \*\phi\tr(s_1,a_1) \\ \*\phi\tr(s_2, a_1) \\ \vdots \\ \*\phi\tr(s_S, a_A) 
\end{pmatrix} \in \Real^{S\cdot A \times K}~.
\end{align*}

We will also need the following matrix $\Pi^{\pi}\in\Real^{S \times S\cdot A}$ that maps q-values to the corresponding value function such that
\[
  \*v^{\pi} = \*\Pi^{\pi} \*q^{\pi}~.
\]

The matrix $\*\Pi^{\pi}$ is defined using the 0-1 identity function $\mathbb{I}$ as:
\[
  \*\Pi^{\pi}(i,j) = \mathbb{I}\{i = j\}~.
\]
The two equations above simply translate to $v^\pi(s) = q^\pi (s, \pi(s)) $ for each state $s\in\states$.

The approximate q-function $\tilde{q}^\pi :\states\times \actions\to \Real$ can be represented using some weights $\*w^\pi  \in \Real^K$ as
\[
  \tilde{\*q}^\pi  = \*\Phi \*w^\pi ~.
\]

The \emph{iterative equation} for the LSTDQ is
\begin{equation}\label{eq:lstdq-iter}
\*w^{\pi}_{j+1} \;=\; (\*\Phi\tr \*D \*\Phi)^{-1} \*\Phi\tr \*D (\*b + \gamma \cdot \*P  \*\Pi^\pi \*\Phi  \*w_j^{\pi})~,
\end{equation}
where $\*D \in \Real^{S\cdot A \times S \cdot A}$ is a diagonal matrix that represents the frequency of the states and actions present in the data.

Similarly, the fixed-point equation for LSTDQ is

\begin{equation}\label{eq:lstdq-fixed}
\*w^{\pi} \;=\; \left(\*\Phi\tr \*D (\*I - \gamma \cdot \*P  \*\Pi^\pi) \*\Phi\right)^{-1}  \*\Phi\tr \* D \*b~,
\end{equation}

The convergence properties of LSTDQ are quite similar to the convergence properties of LSTD in terms of on-policy and off-policy convergence.

\section{Data-driven LSTDQ}

Similarly, to LSTD, one can also compute the LSTDQ solution derectly from a dataset.

\begin{align*}
\tilde{\*F} &= \frac{1}{L} \cdot \sum_{l=1}^L \*\phi(s_l,a_l)\*\phi(s_l,a_l)\tr  \quad \in \Real^{K\times K} \\
\tilde{\*H}^\pi  &= \frac{1}{L} \cdot \sum_{l=1}^L \*\phi(s_l, a_l)\*\phi(s_l', \pi(s_l))\tr \quad \in \Real^{K\times K} \\
\tilde{\*h} &= \frac{1}{L} \cdot \sum_{l=1}^L \left( r_l \cdot \*\phi(s_l,a_l) \right) \quad \in \Real^{K}
\end{align*}
It is important to note that some of the features above are computed for the actions that are found in the data, and others are computed for actions prescribed by the policy. This way the algorithm can approximate the value of state action pairs it has never observed in the data.

\begin{theorem}
Suppose that the Markov chain induced by $\*P$ is ergodic and assume an on-policy trajectory $\mathcal{D}$ of length $L$ and values $\tilde{\*F}(L)$, $\tilde{\*H}(L)$, and $\tilde{\*h}(L)$ expressed as functions of the dataset length $L$. Then:
\begin{align*}
\lim_{L\to\infty} \tilde{\*F}(L) &= \*\Phi\tr\*D\opt \*\Phi \\
\lim_{L\to\infty} \tilde{\*H}^\pi (L) &= \*\Phi\tr\*D\opt \*P^\pi  \*\Phi \\
\lim_{L\to\infty} \tilde{\*h}(L) &= \*\Phi\tr\*D\opt \*r~.
\end{align*}
Here, $\*D\opt$ is the diagonal matrix with the stationary distribution of the policy $\pi$ along its diagonal.
\end{theorem}

The data-driven updates are:
\begin{enumerate}
\item Iterative
\begin{equation}\label{eq:lstdq-iter-data}
\*w^{\pi}_{j+1} \;=\; \tilde{\*F}^{-1}\left(\tilde{\*h} + \gamma \cdot \tilde{\*H}^\pi  \*w_j^{\pi}\right)
\end{equation}
\item Fixed point
\begin{equation}\label{eq:lstdq-iter-data}
\*w^{\pi} \;=\; \left(\tilde{\*F} - \gamma \cdot \tilde{\*H}^\pi \right)^{-1} \tilde{\*h}
\end{equation}
\end{enumerate}

% TODO: using the Neumann series with the fixed point solution above would seem to suggest that the F^{-1} in the iterative solution above is not needed. However, that is probably only the case when the features happen to be nice enough so the simplified iteration is a contraction.

\section{LSPI}

A natural algorithm that extends LSTDQ to policy optimization is LSPI. LSPI combines LSTDQ with policy iteration.


\begin{algorithm}
  \KwIn{A dataset $\mathcal{D}$ and a feature function $\phi : \states\times \actions\to \Real$}
  \KwOut{A policy $\tilde{\pi}$ and q-function $\tilde{q}:\states\times\actions\to \Real$}
  $\pi_0 \gets$ some initial policy \;
  $\*w_0 \gets \left(\tilde{\*F} - \gamma \cdot \tilde{\*H}^{\pi_0} \right)^{-1} \tilde{\*h}$\;
  \For{$l \in 1, \ldots, L$}{
    
    $\pi_l \gets$ greedy to $\tilde{q}_{l-1}$  \;
    $\*w_l \gets \left(\tilde{\*F} - \gamma \cdot \tilde{\*H}^{\pi_l} \right)^{-1} \tilde{\*h}$\;
  }
  
  \caption{Least squares policy iteration}
  \label{alg:lspi}
\end{algorithm}

\bibliographystyle{plain}
\bibliography{biblio}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "lecture16"
%%% TeX-master: t
%%% TeX-master: t
%%% End:
