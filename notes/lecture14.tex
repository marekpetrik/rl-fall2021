\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{url}
\usepackage{bbm}
\usepackage{bm}
\usepackage{graphics}

\usepackage{fullpage}

\renewcommand{\*}{\bm}

\lectureNotesTitle{Lecture 14: Solving Markov Decision Processes}
\date{October 28th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
	\maketitle
	
	\section{Value Iteration} 
	
	Recall that the value iteration algorithm we derived in the previous lecture only computes the optimal value function $\*v\opt$ in the limit. It is often desirable to stop the computation early since the improvements in the quality of the value function diminish as the number of iterations grows. 
	
	\cref{alg:value_iteration} is an improved version of value iteration that is guaranteed to compute an $\epsilon$-approximation of the optimal value function $\*v\opt$.
	
	\begin{algorithm}
		\KwIn{Initial value function $\*x\in \Real^S$, and the desired precision $\epsilon > 0$}
		\KwOut{An approximation $\*v$ of the value funtion $\*v\opt$ such that $\|\*v - \*v\opt\|_\infty \le \epsilon$}
		$\*v_0 \gets \*x$\;
		\Repeat{$\| \*v_n - \*v_{n-1} \|_\infty \;\le\; \epsilon (1-\gamma)/ \gamma$}{
			$n\gets n+1$\;
			$\*v_n \gets L \*v_{n-1}$\;
		}
		\Return{$\*v_{n}$}\;
		\caption{Value iteration algorithm.}
		\label{alg:value_iteration}
	\end{algorithm}
	
	To prove the correctness of \cref{alg:value_iteration}, recall the following theorem from the last lecture.	
	\begin{theorem}\label{thm:error}
		Suppose that $\*v\opt \in \Real^S$ is an arbitrary value function. Then:
		\[
		\| \*v\opt - \*v\|_\infty \;\le\; \frac{1}{1-\gamma}\| L \*v - \*v \|_\infty~.
		\]
	\end{theorem}
	
	We can now state the correctness and computational complexity of the value iteration algorithm.
	\begin{theorem}
		Suppose that \cref{alg:value_iteration} is executed with some desired precision $\epsilon$ and an input $\*v_0$. Then, it
		\begin{enumerate}
			\item terminates in at most 
			\[
			n = \frac{\log(\epsilon) + \log((1-\gamma)/ \gamma ) - \log(\| \*v_0 - \*v\opt \|_\infty)}{\log(\gamma)}~.
			\]
			iterations, and
			\item returns a value function $\*v\in\Real^S$ such that $\| \*v\opt - \*v \|_\infty \le \epsilon$.
		\end{enumerate}
	\end{theorem}
	\begin{proof}
		The first part of the theorem depends on the contraction property of the Bellman operator, which implies that
		\[
		\| \*v_n - \*v\opt \|_\infty \;\le\; \gamma^n \| \*v_0 - \*v\opt \|_\infty~.
		\]
		The termination condition is, therefore, satisfied for the iteration $n$ for which
		\[
		\gamma^n \| \*v_0 - \*v\opt \|_\infty \le \epsilon (1-\gamma)/ \gamma~.
		\] 
		Assuming that the values are positive, we can take the log (an increasing function on the positive domain) of both sides of the inequality above, and obtain that 
		\[
		n \cdot \log(\gamma) + \log(\| \*v_0 - \*v\opt \|_\infty) \le \log(\epsilon) + \log((1-\gamma)/ \gamma )~.
		\]
		Solving for $n$ we obtain, since $\log{\gamma} < 0$:
		\[
		n \ge \frac{\log(\epsilon) + \log((1-\gamma)/ \gamma ) - \log(\| \*v_0 - \*v\opt \|_\infty)}{\log(\gamma)}~.
		\]
		
		Turning to the second part of the theorem, suppose that $\*v_n$ satisfies the termination condition. \Cref{thm:error} implies that
		\begin{align*}
			\| \*v\opt - \*v_{n-1} \|_\infty &\le \frac{1}{1-\gamma} \| L \*v_{n-1} - \*v_{n-1} \|_\infty = \frac{1}{1-\gamma} \| L \*v_{n-1} - \*v_{n-1} \|_\infty \\
			&\le \frac{1}{1-\gamma} \cdot \frac{1-\gamma}{\gamma} \cdot \epsilon = \frac{1}{\gamma} \epsilon~.
		\end{align*}
		Then, applying the contraction property, we obtain the desired inequality:
		\[
		\| \*v\opt - \*v_n \|_\infty  \le \gamma \cdot\| \*v\opt - \*v_{n-1} \|_\infty \le \gamma \frac{1}{\gamma} \epsilon = \epsilon~.
		\]
	\end{proof}
	
	Finally, to give some sense of the computational complexity of \cref{alg:value_iteration}, refer to \cref{fig:gamma} for a plot of the number of iterations needed to compute the $\epsilon$-optimal policy. Notice that the computational complexity increases sharply as the discount factor approach $1$. Policy iteration tends to significantly outperform value iteration when the discount rate approaches 1. 
	
	\begin{figure}
		\centering
		\includegraphics[width=0.6\linewidth]{complexity.pdf}
		\caption{The number of value function iterations needed to achieve a fixed precision $\epsilon$ as a function of $\gamma$.} \label{fig:gamma}
	\end{figure}
	%Plots.plot(γ -> (1+ log((1-γ)/γ))/log(γ), xlim = [0,0.99], xlabel="γ", ylabel = "n", legend = nothing)
	
	\bibliographystyle{plain}
	\bibliography{biblio}
\end{document}

