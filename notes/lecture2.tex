\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{mathabx}        % for \bigtimes
\usepackage{booktabs}

% diagram stuff
\usepackage{pgf}
\usepackage{tikz}

\usetikzlibrary{arrows,automata}

\lectureNotesTitle{Lecture 2: Deterministic Dynamic Programming}
\date{September 2nd, 2021}
\author{Marek Petrik}



% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\begin{document}
	\maketitle
	
	\section{Deterministic Dynamic Program}
	See \cref{tab:translation} for a table that maps the notation between RL (what we are using) and Bertsekas's RLOC book.
	
	\begin{table}
		\centering
		\begin{tabular}{lcc}
			\toprule
			\textbf{Meaning} & \textbf{RL Notation} & \textbf{RLOC Notation} \\
			\midrule
			Time step & $t$ & $k$ \\
			Horizon & $T$ & $N$ \\
			State & $s_t$ & $x_k$ \\
			Action/Control & $a_t$ & $u_k$ \\
			Reward & $r_t$ & -- \\
			Cost & -- & $g_k$ \\
			Value function & $v_k$ & $J_k$  \\
			\bottomrule
		\end{tabular}
		\caption{Translating notations. See also Section 1.4 in the RLOC textbook. } \label{tab:translation}
	\end{table}
	
	
	\paragraph{Components}
	A deterministic dynamic program consists of:
	\begin{enumerate}
		\item State set (or space): $\states$
		\item Action set (or space): $\actions$
		\item Horizon: $T \in \Natural$
		\item Reward function: $r_t : \states \times \actions \to \Real$ for $t=0,\ldots, T-1$, and $r_T : \states \to \Real$
		\item Transition function: $f_t: \states \times \actions \to \states$ for $t=0,\ldots, T-1$
		\item Initial state: $s_0 \in \states$
	\end{enumerate}
	The dynamics of the problem must satisfy for $t = 0,\ldots, T-1$:
	\begin{equation} \label{eq:dynamics}
		s_{t+1} = f_t(s_t,a_t)  ~.
	\end{equation}
	Notice that the \emph{initial state} $s_0$ is given in the problem definition.
	
	\paragraph{Solution} The solution to a deterministic dynamic program is a sequence of actions $a_0, \ldots, a_{T-1} \in \actions$ which can also be denoted more concisely as $(a_t)_{t=0}^{T-1}$. The sequence $(a_t)_{t=0}^{T-1}$ represents any solution, optimal or suboptimal one. We will denote the optimal solution by $(a_t\opt)_{t=0}^{T-1}$ with $a_t\opt\in \actions, t=0,\ldots,T-1$. The sequence of actions that solves a dynamic program is often referred to as a \textbf{policy}.
	
	Value functions will play a crucial role in defining a dynamic program's objective (goal) and computing its solutions.
	\begin{defn}[Value Function]
		For a deterministic dynamic program, we define a value function $v_t : \states \times ( \bigtimes_{k=t}^{T-1} \actions ) \to \Real$ for $t=0,\ldots,T$ and any $s_t \in \states$ as:
		\begin{equation}
			v_t(s_t; a_t, \ldots, a_{T-1}) \;=\; \sum_{k=t}^{T-1} r_k(s_k, a_k) + r_T(s_T)~,
		\end{equation}
		where $s_k, k=t+1,\ldots,T$ satisfy $s_k = f_{k-1}(s_{k-1}, a_{k-1})$. The value $v_0(s_0; a_0, \ldots, a_{T-1})$ is often referred to as the \emph{return}.
	\end{defn}
	The $;$ separating states and actions in the definition of $v_t$ is just to emphasize the distinction between the different types of parameters.
	
	
	\begin{defn}[Objective]
		Our goal when solving a dynamic program is to maximize the sum of the rewards accumulated throughout the execution.
		\begin{equation} \label{eq:objective}
			\max_{a_0, \ldots, a_{T-1} \in \mathcal{A}} \; v_0(s_0; a_0, \ldots, a_{T-1}) \;=\; \max_{a_0, \ldots, a_{T-1} \in \mathcal{A}} \; \sum_{k=0}^{T-1} r_k(s_k, a_k) + r_T(s_T) ~.
		\end{equation}
	\end{defn}
	The optimal value of the objective in~\eqref{eq:objective} is often referred to as the \textbf{optimal return} or total return.
	
	The optimal solution (or control sequence or policy) is a sequence of actions $(a_t\opt)_{t=0}^{T-1}$ that satisfies:
	\begin{equation} \label{eq:optimal_solution}
		(a_0\opt, \ldots, a_{T-1}\opt) \;\in\; \arg\max_{a_0, \ldots, a_{T-1} \in \mathcal{A}} \; v_0(s_0; a_0, \ldots, a_{T-1})~.
	\end{equation}
	
	The optimal value function is a crucial component of almost all reinforcement learning algorithms and is defined as follows.
	\begin{defn}[Optimal Value Function]
		The optimal value function $v\opt_t : \states \to \Real$ is defined for any $t=0,\ldots,T$ and $s_t\in\states$ as:
		\begin{equation}
			v\opt_t(s_t) \;=\;  \max_{a_t, \ldots, a_{T-1} \in \mathcal{A}} \; v_t(s_t; a_t, \ldots, a_{T-1}) ~.
		\end{equation}
	\end{defn}
	
	\section{Dynamic Programming}
	
	Let's now discuss how one can solve a dynamic program. A crucial property that we will leverage throughout this course is sub-problem optimality, which is summarized in the following theorem.
	
	\begin{theorem}[Principle of Optimality]
		Let $(a_t\opt)_{t=0}^{T-1}$ be an optimal control sequence in~\eqref{eq:optimal_solution} and let $(s_t\opt)_{t=0}^T$ be the corresponding state sequence with $s_0\opt = s_0$. Then the control sub-sequence $(a_t\opt)_{t=k}^{T-1}$ is optimal in
		\[
		\max_{a_k, \ldots, a_{T-1} \in \mathcal{A}} \; v_k(s_k\opt; a_k, \ldots, a_{T-1})
		\]
		for each $k=0,\ldots,T-1$.
	\end{theorem}
	
	The principle of sub-problem optimality is a concept that can be used to simplify the computation of the optimal value function. It translates to the following key recursive property that we can use to compute the optimal value function.
	
	\begin{theorem}[Dynamic Programming Equations]
		The optimal value function $v_t\opt : \states\to \Real, t=0,\ldots,T$ is the unique value that satisfies for all $s\in\states$:
		\begin{equation} \label{eq:dynamic_condition}
			\begin{aligned}
				v_T\opt(s) &\;=\; r_T(s) ~,\\
				v_t\opt(s) &\;=\; \max_{a\in\actions} \; \left\{ r_t(s,a) + v_{t+1}\opt(f_t(s,a)) \right\}, \qquad t = 0, \ldots, T-1~.
			\end{aligned}
		\end{equation}
		Furthermore, let $a_t\opt\in \arg\max_{a\in\actions} \left( r_t(s_t\opt,a) + v_{t+1}\opt(f_t(s_t\opt,a)) \right)$ and $s_{t+1}\opt = f_t(s_t\opt, a_t\opt)$ for all $t=0,\ldots,T-1$ with $s_0\opt = s_0$.
	\end{theorem}
	\begin{proof}
		The proof follows by a backward induction on $t$. The initial case with $t = T$ follows directly from the definition. Assume now that $v\opt_{t+1}$ is the unique solution to~\eqref{eq:dynamic_condition}. Then for all $s\in\states$:
		\begin{align*}
			v_t\opt(s) &= \max_{a_t, \ldots, a_{T-1} \in \actions} \; v_t(s; a_t, \ldots, a_{T-1}) \\
			&= \max_{a_t\in\actions} \left( r_t(s,a_t) + \max_{a_{t+1}, \ldots, a_{T-1} \in \actions} v_{t+1} (f_t(s, a_t); a_{t+1}, \ldots, a_{T-1})\right) \\
			&= \max_{a_t\in\actions} \left( r_t(s,a_t) + v_{t+1}\opt (f_t(s, a_t))\right)
		\end{align*}
		Here, the first two equalities follow by algebraic manipulation. The last equality then follows directly from the definition of $v_{t+1}\opt(s)$. The value $v_t\opt(s)$ is unique for each $s\in\states$ since $v_{t+1}\opt(s)$ is unique for all $s\in\states$. Finally, using a similar argument, one can easily show that the control sequence $a_0\opt, \ldots, a_{T-1}\opt$ achieves the optimal return $v_0\opt(s_0)$.
	\end{proof}
	
	See \cref{alg:dp_det} for a sketch of a dynamic programming algorithm.
	
	\begin{algorithm}
		\KwIn{Definition of a deterministic dynamic program}
		\KwOut{Optimal value function $v_t\opt(s),t=0,\ldots,T-1,s\in\states$ and control sequence $a_t\opt, t=0,\ldots, T-1$}
		\tcp{Backward pass to compute $v\opt$}
		$v\opt_T(s) \gets r_T(s), \forall s\in\states$ \;
		\For{$t = T-1, \ldots, 0$}{
			$v_t\opt(s) \gets \max_{a\in\actions} \; \left( r_t(s,a) + v_{t+1}\opt(f_t(s,a)) \right), \forall s\in\states$\;
		}
		\tcp{Forward pass to compute $a\opt$}
		$s_0\opt \gets s_0$\;
		\For{$t = 0, \ldots, T-1$}{
			$a_t\opt \gets \arg\max_{a\in\actions} \; \left( r_t(s_t\opt,a) + v_{t+1}\opt(f_t(s_t\opt,a)) \right)$ \;
			$s_{t+1}\opt \gets f_{t}(s_{t}\opt, a_t\opt)$\;
		}
		\Return{Value function $v_t\opt(s),t=0,\ldots,T-1,s\in\states$ and control sequence $a_t\opt, t=0,\ldots, T-1$.}
		\caption{Deterministic dynamic programming algorithm} \label{alg:dp_det}
	\end{algorithm}
	
	
	\section{Example}
	
	We now consider the solution for the following simple deterministic problem.
	\begin{example} \label{exm:parking_deterministic}
		Consider a deterministic version of the optimal parking problem. You are driving down a one-way road towards a store. There are 5 \emph{empty} parking boxes between you and the store. You would like to park as close as possible to the store to minimize how far you need to walk. Let $\$ 1$ represent the monetary cost of walking a single parking box. Assume that if you miss the last parking spot, you will not be able to visit the store and incur a cost of $\$ 10$.
	\end{example}
	
	Lets formulate the parking problem as a deterministic dynamic program. The components are as follows:
	\begin{enumerate}
		\item State set (or space): $\states = \{ R, S\}$ representing road and stopped (sidewalk)
		\item Action set (or space): $\actions = \{P,G\}$ representing park and go
		\item Horizon: $T = 5$
		\item Intermediate reward function for $t=0,\ldots, T-1$:
		\[ r_t(s_t, a_t) = \begin{cases}
			-(5-t-1) = t - 4 &\text{if } s_t = R \wedge a_t = P \\
			0 &\text{otherwise}
		\end{cases} \]
		\item Terminal reward function
		\[
		r_T(s_T) = \begin{cases}
			-10 &\text{if } s_T = R \\
			0 &\text{otherwise}
		\end{cases}
		\]
		\item Transition function for $t=0,\ldots, T-1$:
		\[
		f_t(s_t, a_t) = \begin{cases}
			S &\text{if } s_t = S \vee a_t = P \\
			R &\text{otherwise}
		\end{cases}
		\]
		\item Initial state: $s_0 = R$
	\end{enumerate}
	
	Please see \cref{fig:transitions} for a sketch of the transitions in this simple example. As an exercise, compute the value function and optimal control sequence in this simple example.
	
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}[->,shorten >=1pt,auto,node distance=1.5cm]
			\node (t0) {$t=0$};
			\node (t1) [right of=t0] {$t=1$};
			\node (t2) [right of=t1] {$t=2$};
			\node (t3) [right of=t2] {$t=3$};
			\node (t4) [right of=t3] {$t=4$};
			\node (t5) [right of=t4] {$t=5$};
			
			\node[state] (r0)	[below of=t0] {$R$};
			\node[state] (r1)	[below of=t1] {$R$};
			\node[state] (r2)	[below of=t2] {$R$};
			\node[state] (r3)	[below of=t3] {$R$};
			\node[state] (r4)	[below of=t4] {$R$};
			\node[state] (r5)	[below of=t5] {$R$};
			
			\node[state] (s1)	[below of=r1] {$S$};
			\node[state] (s2)	[below of=r2] {$S$};
			\node[state] (s3)	[below of=r3] {$S$};
			\node[state] (s4)	[below of=r4] {$S$};
			\node[state] (s5)	[below of=r5] {$S$};
			
			\path (r0) edge node {$G$} (r1);
			\path (r1) edge node {$G$} (r2);
			\path (r2) edge node {$G$} (r3);
			\path (r3) edge node {$G$} (r4);
			\path (r4) edge node {$G$} (r5);
			
			\path (r0) edge node {$P$} (s1);
			\path (r1) edge node {$P$} (s2);
			\path (r2) edge node {$P$} (s3);
			\path (r3) edge node {$P$} (s4);
			\path (r4) edge node {$P$} (s5);
		\end{tikzpicture}
		\caption{A sketch of the transitions in \cref{exm:parking_deterministic}.} \label{fig:transitions}
	\end{figure}
	
	
\end{document}