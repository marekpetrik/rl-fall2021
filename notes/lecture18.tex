
\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{url}
\usepackage{bbm}
\usepackage{bm}
\usepackage{graphics}

\usepackage{fullpage}

\renewcommand{\*}{\bm}

\lectureNotesTitle{Lecture 18: SARSA and TD}
\date{November 18th, 2021}
\author{Marek Petrik and Sarah Hall}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers
\renewcommand{\vec}[1]{\bm{#1}}

\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
\maketitle

\section{Introduction}

This lecture shows how to derive the TD method from LSTD. Chronologically, TD predates LSTD, but its derivation was largely based on a heuristic. LSTD provides a more convenient way for understanding and analyzing TD.

These notes are quite brief. I recommend that you read Section 9.1, 9.2, 9.3 of (available online):
\begin{quote}
    Sutton, R S, and A G Barto. Reinforcement Learning: An Introduction. 2nd ed. The MIT Press, 2018.
\end{quote}
which has a nice in-depth description of this method~\cite{Sutton2018}.  
\section{Gradient Descent}

We are looking for some $x$ to minimize $f(x)$:
\[
  \min_{x \in \mathbb{R}}f(x)
\]

Assume that the function $f:\Real^K\to \Real$ is differentiable. Remember, the gradient $\nabla f$ is a vector of dimension $K$.
Recall also that gradient is \emph{orthogonal} to the contour.

How do we find the minimum of function $f$? A general approach is to use \emph{gradient descent}. It generates a sequence of values \(x, x_2, x_3, x_4... \in \mathbb{R}^K\) with decreasing function values:
\[f(x_1) \geq f(x_2) \geq f(x_3), \ldots \]
The idea of gradient descent is to use Taylor Series approximation of $f$ to keep improving the value. The gradient descent update rule is
\[x_{k+1} = x_k - \alpha \cdot \nabla_x f(x_k)\]
where $\alpha > 0$ is the \emph{step size}.

You need a step size to ensure that you don't take too ``large'' steps and accidentally ``step past'' a good solution into worse and worse solutions.

Generally, the step size, should depend on your iteration $k$. A good way to choose step size for gradient descent is:
\[\alpha_k = \frac{1}{k}\]
so that:
\begin{enumerate}
\item step size is not too large:
\[\lim_{k \to \infty} \alpha_k = 0\]
\item step size is not too small/is large enough:
\[\lim_{k \to \infty} \sum^k_{j=1}\alpha_j = \infty\]
\end{enumerate}

\section{Recall Iterative LSTD}

Recall that thus far we have assumed that value function is approximated as a linear combination of state features:
\[\tilde{v}(s) = \sum^K_{i=1} \phi(s)_iw_i ~.\]
A general way of writing the iterative LSTD equation update is as a projection:
\begin{equation*}
  \vec{w}_{k+1} = (\vec{\Phi}^\top\vec{\Phi})^{-1}\vec{\Phi}^\top(\vec{r}^\pi + \gamma \cdot\vec{P}^\pi \Phi \vec{w}_k) \in \arg\min_{w} \| \vec{\Phi} \vec{w} - (\vec{r}^\pi + \gamma \vec{P}^\pi \Phi \vec{w}_k)\|^2_2
\end{equation*}
The linear projection allows only for linear value function approximation. It is often desirable to allow for a more general approximation of value functions. In general we will not assume that the approximate value function $\tilde{v}(s,\*w)$ for a state $s$ is differentiable in $\*w$. When $\tilde{v}$ is represented as a vector over states is represented as $\tilde{\*b}_{\vec{w}}$.

It is not possible to compute the optimal projection when the 



\[\*w_{k + 1} = \*w_k - \alpha \cdot \nabla_{\vec{w}} \| \tilde{\vec{v}}_{\vec{w}} - \vec{r}^\pi - \gamma \cdot \vec{P} ^\pi  \tilde{\vec{v}}_{\vec{w}_k}\|^2_2 \]

Note that above we are taking the gradient with respect to $\vec{w}$, so treat $\vec{w}_k$ like a fixed point.
\subsection{Example: Cart-pole, revisited} 

\[s = (\theta, \dot{\theta}, x, \dot{x})\]

\[\tilde{v}(s; \vec{w}) = w_5 \cdot\frac{e^{w_1\theta+w_2\dot\theta}}{1 + e^{w_1\theta+w_2\dot\theta}} + w_6 \cdot\frac{e^{w_3 x+w_4\dot x}}{1 + e^{w_3 x+w_4\dot x}}\]
Will need:

\[\nabla_{\vec{w}} \tilde v(s; \vec{w}) \]

PyTorch and TensorFlow can be used to compute a gradient, but sometimes, for small problems, it's much much faster to compute it by hand.


\section{TD (temporal difference)}

The TD update for the each time step from $s$ to $s'$ is:
\[\vec{w}_{k+1} =  \vec{w}_k - \alpha_k\cdot \left(\tilde v(s; \vec{w}_{k}) - r(s, \pi(s)) - \gamma \cdot \tilde v(s'; \vec{w}_k) \right) \cdot \nabla_{\vec{w}} \tilde v(s; \vec{w}_k) \]

To derive this update from LSTD, we use stochastic gradient descent. Stochastic gradient descent is designed to minimize a function like

\[\min_{\vec{x} \in \mathbb{R}^K} \mathbb{E}_\omega \left[ f(\vec{x}, \omega)\right] \]

Think of this problem as optimizing weights $\vec{w}$ and think of the expectation as the transition probabilities, $\vec{P}^\pi$.

If you apply the gradients to the above, you get something like:

\[\vec{x}_{k+1} = \vec{x}_k - \alpha_k \cdot\nabla_{\vec{x}} \mathbb{E}_\omega [f(\vec{x},\omega)]\]
And this would be gradient descent.

The problem with this is we might have a ton of transition probabilities, and we need to compute this expectation w.r.t all possible transition probabilities. 

And we also need to take many steps to calculate the descent!

So we can do this:

\[\vec{x}_{k+1} \approx \vec{x}_k - \alpha_k \cdot\nabla_{\vec{x}}f(\vec{x}, \hat \omega) \]
Where $\hat \omega$ is a sample of $\omega$.

In stochastic gradient descent and TD, the step size must satisfy the following properties in order to converge to a solution:

\begin{align*}
\lim_{j \to \infty} \; \sum^j_{k=1}\alpha_k &= \infty  \\
\lim_{j \to \infty} \; \sum^j_{k=1}\alpha_k^2 &< \infty 
\end{align*}


\section{SARSA}

See Sections 6.4 and 10.1 in the ``Introduction to RL'' book for the description of the algorithm~\cite{Sutton2018}.

\section{DQN}

SARSA and TD can be generalized to use various approximation methods. A popular approximation to use is a neural network. A good introduction to the algorithm can be found in this paper: \cite{Mnih2013}.

% \section{RL is really an optimization problem} 

% * Objective Function: Return $\rho: \Pi_R \to \mathbb{R}$
% * Decision Variable: $\pi \in \Pi_R$

% \[\max_{\pi \in \Pi} \rho(\pi) \]

% \subsection{T=1}

% \begin{align*}
% \pi \in \Pi_R\\
% S \times A \to \mathbb{R}\\
% \rho(\pi) = \sum_{a \in A} \pi(s_0, a) \cdot r(s_0,a)
% \end{align*}

% A simple example:

% \begin{align*}
% S = \{ s_0\}\\
% A = \{a_1, a_2, a_3\}\\
% r(s,a) = \{1, a =a_1, 2, a = a_2, 3, a = a_3\}
% \end{align*}
% So this means:

% \[\max_{\pi \in \mathbb{R}^3} 1 \cdot \pi(1) +  2 \cdot \pi(2) +  3 \cdot \pi(3) \]

% Subject to:

% \[\pi(1) \geq 0; \pi(2) \geq 0;  \pi(3) \geq 0; \]

% \[\pi(1) + \pi(2) + \pi(3) =1 \]

% You can't compute this using gradient descent because you may end up outside of your constraints.

% The optimal solution can be seen just by looking at the problem:

% \begin{align*}
% \pi^* = \begin{bmatrix}
% 0 \\
% 0 \\ 
% 1
% \end{bmatrix}
% \end{align*}

% \[\rho(\pi^*) = 3 \]

\bibliographystyle{plain}
\bibliography{biblio}

\end{document}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
