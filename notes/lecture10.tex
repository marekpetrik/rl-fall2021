\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{url}


\lectureNotesTitle{Lecture 10: Policy Gradient Descent}
\date{October 7th, 2021}
\author{Harrison Geissler and Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
	\maketitle

	\section{Introduction} \label{sec:intro}

\cref{sec:gradients,sec:descent} give a very brief overview of basic ideas that underlie gradient descent methods. For more details on this topic I recommend:
\begin{quote}
    Bertsekas, Dimitri P. "Nonlinear programming." Athena Scientific. (2016)
\end{quote}

Also, throughout this class, we follow the convention that $\log$ represents the natural, base $e$, algorithm. This convention is widely adopted in machine learning and statistic.

For more details describing the policy gradient method, see \emph{RLOC (Bertsekas, 2019)} pg. 270--281 and Section 13.1 in \emph{RL (Sutton and Barto, 2019)}

\section{Gradients of Vector Functions}\label{sec:gradients}

The Taylor series formulation allows any differentiable function $g:\Real \to \Real$ to be represented as a linear combination of polynomial functions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Taylor Series Definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{defn}
    Given a function $g: \Real\to\Real$, such that $g(x)$ is differentiable and continuous when $x\in\mathbb{R}$. It follow from the Taylor series:
    \[
    g(x) = \sum_{n=0}^{\infty}{\frac{g^{(n)}(a)}{n!}(x-a)^{n}}~,
    \]
    where $g^{(n)}$ represents the $n$-th derivative of $g$.
\end{defn}


To generalize the Taylor series to functions defined in terms of vectors, we need to introduce the gradient operator $\nabla$. The gradient is a derivative in multiple dimensions with respect to some function as given in the following definition.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gradient Definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{defn}
    The gradient $\nabla$ function $f: \Real^n \to \Real$ at $\hat{x}\in\Real^n$ is defined as
    \[
    \nabla_x f(\hat{x}) \;=\;
    \begin{bmatrix}
        \frac{d}{d x_1} f(\hat{x}) \\
        \frac{d}{d x_2} f(\hat{x})\\
        \vdots \\
        \frac{d}{d x_n} f(\hat{x})
    \end{bmatrix}~.
    \]
    Note that the subscript in $\nabla_x$ denotes the variable that is being differentiated and $\hat{x}$ denotes the point in which the gradient is evaluated.
\end{defn}

%	Gradients are important because of the following two main reasons. The gradient $\nabla_x f(\hat{x})$ is the normal vector to a level curve $f(x_1, \ldots, x_n) = k, \;k\in\mathbb{R}$



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Linear Taylor Representation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Gradient descent, which we describe below, generates a sequence of vectors $x^0, x^1, \ldots$ with decreasing function values $f(x^{i+1}) < f(x^{i})$ until it reaches a local \emph{minimum} of $f$ (or a stationary point in general). Gradients are useful to use because they can be used to determine how to construct $x^{i+1}$ from $x^i$ in order to reduce the function value. The following multi-dimensional generalization of the Taylor series shows that a linear function on small scales can approximate any function.
\begin{theorem}
    Given a function $f: \Real^K \to \Real$  such that $f(x)$ is differentiable and continuous for some $\hat{x} \in \mathbb{R}$. Then:
    \[
    f(x) = f(\hat{x}) + \nabla f(\hat{x})\tr (x-\hat{x}) + o(\|x-\hat{x}\|)~,
    \]
    where $o(\|x-\hat{x}\|)$ denotes an error function that decreases superlinearly with $\|x-\hat{x}\| \to 0$.
\end{theorem}


\section{Gradient Descent} \label{sec:descent}

The goal of the gradient descent algorithm is, for a given $f:\Real^K \to \Real$, to solve:
\begin{equation} \label{eq:minimization}
    \min_{x\in\Real^K} f(x)
\end{equation}
This minimization problem can be written alternatively as
\[
\min_{x_1,\ldots,x_n}f(x_1, \ldots, x_n)
\]
The premise is that we want to follow the negative gradient. To visualize this, imagine dropping a ball in a sink. The ball is going to follow the steepest path. By following the negative gradient, eventually, the gradient will be zero. If the function $f$ is locally convex, continuous, and differentiable around a local minimum, then it can find it.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gradient Descent Definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{defn}[Gradient Descent] \label{def:gradient_descent}
    The gradient descent algorithm for solving~\eqref{eq:minimization} generates a sequence of points $x^{i} \in \mathbb{R}^K, i\in\mathbb{N}$ that satisfy
    \begin{equation}\label{eq:gradient}
        x^{i+1} \;=\; x^i + \alpha_i \cdot d^i~.
    \end{equation}
    Here, $\alpha_i \in \Real, i > 0$ is the \emph{step size} and $d^i \in \Real^K$ is a \emph{descent direction} which satisfies
    \[
    \nabla f(x^i)\tr d^i < 0~.
    \]
\end{defn}

The gradient descent method in \eqref{eq:gradient} can be used with various choices of the step size $\alpha_i$ and gradient direction $d^i$. A simple, but not necessarily good, descent direction is to follow the negative gradient
\[
d^i = -\nabla f(x^i)~.
\]
Please see the reference mentioned in \cref{sec:intro} for several better choices of the descent direction. A good choice for the step size is to let
\[
\alpha_i = i^{-1}~.
\]
In order for gradient descent to converge to a stationary point (a local minimum) the step sizes must in general satisfy the following constraints:
\begin{align*}
    \lim_{i\to\infty} \alpha_i &= 0  & \sum_{i=0}^{\infty} \alpha_i = \infty~,
\end{align*}
and the gradients of the function $f$ must be Lipschitz continuous.

%	The next value $f(\mu_{k+1})$ will be less than $f(\mu_{k})$ because the alpha is a positive time step in the direction of $\delta_k$. If $\nabla f(x_1, \ldots, x_n)\tr *\delta_k < 0$, it follows that the $\delta_k$ is in the direction of $-\nabla f(x_1, \ldots, x_n)$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Convergence Proof using Monotonic Property    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	\begin{theorem}
    %	    Given point $\mu_0$
    %	    \begin{enumerate}
        %	        \item If $f(x_1, \ldots, x_n)$ is locally convex at $\mu_0$, continuous and differentiable
        %	        \item Then it is equivalent to say,
        %	        \[
        %	        f(\mu_0) \geq f(\mu_1) \geq f(\mu_2) \geq \cdots
        %	        \]
        %	        \item Meaning that Gradient Descent will eventually converge to a minimum point.
        %	    \end{enumerate}
    %	\end{theorem}


\section{Policy Gradient}
%%%           How to find best policy               %%%%%

In this section, we derive a basic policy gradient approach that works for finite-horizon problems. In order to avoid unnecessary complications that would detract from the main ideas, we consider policies $\pi$ to be stationary and drop the subscript $t$. The ideas extend easily to problems with non-stationary policies.

To use gradient descent, we must make sure that the objective function is differentiable. The standard approach in RL to constructing differentiable value functions is to compute \emph{randomized} instead of deterministic policies.
\begin{defn}[Randomized Policy]
    A randomized policy $\pi: \states\times\actions \to \Real_+$ is a function, such that $\sum_{a\in\actions} \pi(s,a) = 1$. The set of all randomized policies is denoted as $\Pi_R$.
\end{defn}

Another way to define randomized policies is to first define a probability simplex $\Delta^\actions \subseteq \Real^\actions$ as
\[
\Delta^\actions \;=\; \left\{ p : \actions \to \Real_+ \;\mid\; \sum_{a\in\actions} p(a) = 1 \right\}~.
\]
Here, $\Real^\actions$ represents the set of all functions from $\actions$ to $\Real$. Randomized policy is then a mapping $\pi: \states \to \Delta^\actions$.

Randomized policies generalize deterministic policies. Or, in other words, any deterministic policy can be expressed as a randomized policy.

As with fitted value iteration, we use features $\phi: \states\times\actions \to \Real^K$ in order to define \emph{parametric} randomized policies in problems that have an infinite number of states and actions as follows.

\begin{defn}[Softmax Policy] \label{def:softmax}
    The softmax randomized policy $\pi_\theta: \states \times \actions \to \Real_+$ for some weights $\theta\in\Real^K$ is defined as
    \[
    \pi_\theta(s,a) \;=\; \frac{e^{\phi(s,a)\tr\theta}}{\sum_{a' \in \actions}{e^{\phi(s,a')\tr\theta}}} ~.
    \]
\end{defn}

To get some intuitive feeling for softmax policies, let $\tilde{q}_\theta(s, a) = \phi(s,a')\tr \theta$ represent some approximate q-function. The softmax policy then becomes
\[
\pi_\theta(s,a) = \frac{e^{\tilde{q}(s, a)}}{\sum_{a' \in \actions}{e^{\tilde{q}(s, a)}}}~.
\]
This can be seen as an approximation of the greedy policy with respect to $\tilde{q}$ which would be
\[
\pi_\theta(s,a) = \begin{cases} 1 &\text{if } a = a\opt \\
    0 &\text{otherwise} \end{cases}~,
\]
where $a\opt \in \arg\max_{a\in \actions} \, \tilde{q}(s, a)$ is a greedy action. Whereas the greedy policy takes action $a\opt$ with probability 1, the softmax policy simply takes $a\opt$ with a higher probability than other actions.

\begin{remark}
    Softmax functions are used widely in other machine learning contexts, including logistic regression and neural networks.
\end{remark}

We are now ready to derive the gradient descent method for optimizing softmax policies. Our goal is to use gradient descent to choose a policy that maximizes the value function of the dynamic program.  That is, we seek to solve
\begin{equation} \label{eq:objective}
    \max_{\pi\in\Pi_R} v_0(s_0; \pi) = \max_{\pi\in\Pi_R} \Ex{\sum_{t=0}^{T-1}{r_t(S_t,A_t,W_t)} + r_T(S_T) \mid  \begin{matrix}S_{t+1} = f_t(S_t,A_t,W_t) \\ A_t \sim \pi(S_t) \end{matrix}}
\end{equation}
Again, it is important to emphasize that randomized policies do not simply pick one action but instead define a distribution over actions. When following a randomized policy, one samples the action from the prescribed distribution every time a state is visited. As we have shown earlier, a randomized policy cannot outperform the best deterministic policy in a regular stochastic dynamic program.

The maximization in \eqref{eq:objective} can easily be turned into a minimization problem by negating the objective of the optimization as
\begin{equation} \label{eq:objective_min}
    \max_{\pi\in\Pi_R} v_0(s_0; \pi) = - \min_{\pi\in\Pi_R}  \Ex{\sum_{t=0}^{T-1}{- r_t(S_t,\pi(S_t),W_t)} - r_T(S_T) \mid \begin{matrix}S_{t+1} = f_t(S_t,A_t,W_t) \\ A_t \sim \pi(S_t) \end{matrix}}.
\end{equation}
Finally, if we restrict our attention to parametric softmax policies, the optimization in \eqref{eq:objective_min_theta} can be expressed as
\begin{equation} \label{eq:objective_min_theta}
    - \min_{\theta\in\Real^K}  \Ex{\sum_{t=0}^{T-1}{- r_t(S_t,A_t,W_t)} - r_T(S_T) \mid \begin{matrix}S_{t+1} = f_t(S_t,A_t,W_t) \\ A_t \sim \pi_\theta(S_t, \cdot) \end{matrix}}.
\end{equation}

As described in \cref{def:gradient_descent}, gradient descent is another iterative algorithm. It starts with some parameters $\theta^i$ and then takes the gradient step to obtain $\theta^{i+1}$ as
\begin{equation}\label{eq:policy_gradient_upd}
    \theta^{i+1} = \theta^i - \alpha_i \cdot \nabla_\theta \, v_0(s_0; \pi_{\theta^i}).
\end{equation}
As before, $\alpha_i$ is the positive step size. Thanks to using softmax policies, the function $v_0(s_0; \pi_{\theta})$ is differentiable in $\theta$. All that remains to do is to actually derive the form for the gradient $\nabla_\theta \, v_0(s_0; \pi_{\theta^i})$.

In order to derive the gradient expression $\nabla_\theta \, v_0(s_0; \pi_{\theta^i})$, we represent expectation in~\eqref{eq:objective} using trajectories. A \emph{trajectory} is a complete evolution of states, actions, and disturbances from the beginning of the execution to the end of the horizon. The derivation below assumes that all components of the dynamic program are finite to avoid needing to study measurability, but the ideas extend easily to programs with infinite state and action spaces. In particular, a trajectory $j$ consists of $(\hat{s}^j,\hat{a}^j,\hat{w}^j)$ where
\begin{align*}
    \hat{s}^j &= (\hat{s}^j_0, \hat{s}^j_1, \ldots, \hat{s}^j_T) \\
    \hat{a}^j &= (\hat{a}^j_0, \hat{a}^j_1, \ldots, \hat{a}^j_{T-1}) \\
    \hat{w}^j &= (\hat{w}^j_0, \hat{w}^j_1, \ldots, \hat{w}^j_{T-1})~.
\end{align*}


Assume that there are $N$ total possible trajectories representing every possible combination of states, actions, and disturbances. We can then express the objective function in~\eqref{eq:objective_min} by turning the expectation operator to a sum from its definition as
\begin{align*}
    - v_0(s_0; \pi_\theta) &\;=\; \Ex{\sum_{t=0}^{T-1}{- r_t(S_t,\pi(S_t),W_t)} - r_T(S_T) \mid \begin{matrix}S_{t+1} = f_t(S_t,A_t,W_t) \\ A_t \sim \pi_\theta(S_t, \cdot) \end{matrix}} \\
    &\;=\; \frac{1}{N} \sum_{j=1}^{N}{\left( {\sum_{t=0}^{T-1}  - r_t(\hat{s}^j_t,\hat{a}^j_t,\hat{w}^j_t) - r_T(\hat{s}^j_T)} \right) } \cdot \Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_\theta}
    ~.
\end{align*}
The product of $\Pr{\hat{w}^j}$ and $\Pr{\hat{a}^j \mid \pi_\theta}$ computes the probability of the trajectory, and the terms represent the joint probabilities of the disturbances $\hat{w}^j$ and actions $\hat{a}^j$ respectively. They are defined as:
\begin{equation}\label{eq:p_defs}
    \begin{aligned}
        \Pr{\hat{w}^j} &\;=\; \prod_{t=0}^{T-1} \Pr{\hat{w}_t^j} \\
        \Pr{\hat{a}^j \mid \pi_\theta} &\;=\; \prod_{t=0}^{T-1}{\pi_\theta(\hat{s}^j_t,\hat{a}^j_t)} = \prod_{t=0}^{T-1}{\frac{e^{\phi(\hat{s}^j_t,\hat{a}^j_t)\tr \theta}}{\sum_{a' \epsilon A}{e^{\phi(\hat{s}^j_t,a')\tr \theta}}}} ~.
    \end{aligned}
\end{equation}

Finally, to simplify our notation, we let
\begin{equation} \label{eq:zj}
    z_j = \sum_{t=0}^{T-1} -r_t(\hat{s}^j_t,\hat{a}^j_t,\hat{w}^j_t) - r_T(\hat{s}^j_T)
\end{equation}
and therefore
\begin{equation} \label{eq:value_simplified}
    - v_0(s_0; \pi_\theta) \;=\; \frac{1}{N} \sum_{j=1}^{N} z_j \cdot \Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_\theta} ~.
\end{equation}

At last, we can compute the gradient $-\nabla_\theta v_0(s_0; \pi_{\theta^i})$ in some $\theta^i$ by algebraic manipulation of~\eqref{eq:value_simplified} as
\begin{subequations} \label{eq:grad_all}
    \begin{align}
        -\nabla_\theta v_0(s_0; \pi_{\theta^i}) &=
        \nabla_\theta\left(\frac{1}{N}  \sum_{j=1}^{N} z_j \cdot \Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}} \right)\\
        &= \frac{1}{N}  \sum_{j=1}^{N} z_j \cdot \nabla_\theta \left(\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) \\
        \label{eq:grad_mul1}
        &= \frac{1}{N}  \sum_{j=1}^{N} z_j \cdot \frac{\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}}{\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}} \cdot \nabla_\theta \left(\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) \\
        \label{eq:grad_logtrick}
        &= \frac{1}{N}  \sum_{j=1}^{N} z_j \cdot \Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}} \cdot \nabla_\theta \log \left(\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) \\
        &= \frac{1}{N}  \sum_{j=1}^{N} z_j \cdot \Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}} \cdot \nabla_\theta \log \left(\Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) \\
        \label{eq:grad_expectation}
        &= \Exx{J\sim \pi_{\theta^i}}{z_J \cdot \nabla_\theta \log \left(\Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) }~.
    \end{align}
\end{subequations}
Let's discuss the steps in the derivation above. In~\eqref{eq:grad_mul1}, we simply multiply the expression by $1$. Then, in~\eqref{eq:grad_logtrick}, we use the property of the gradient of a natural logarithm
\[
\nabla_\theta \log \left(\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) = \frac{\nabla_\theta \left(\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}\right)}{\Pr{\hat{w}^j} \cdot \Pr{\hat{a}^j \mid \pi_{\theta^i}}}
\]
The substitution above is crucial to compute the gradient of the product in the definition of $\Pr{\hat{a}^j \mid \pi_{\theta^i}}$. Finally, in~\eqref{eq:grad_expectation}, we express the sum as the expectation over the random variable $J$ of trajectories generated by the policy $\pi_{\theta^i}$.

Summarizing the derivation above, we get the following results.
\begin{theorem} \label{thm:grad}
    Consider some $\hat{\theta} \in\Real^K$. Then, the gradient of the return for a softmax policy $\pi_{\hat{\theta}}$ is
    \[
    - \nabla_\theta\, v_0(s_0; \pi_{\hat{\theta}}) \;=\; \Exx{J\sim \pi_{\hat{\theta}}}{z_J \cdot \nabla_\theta \log \left(\Pr{\hat{a}^j \mid \pi_{\hat{\theta}}}\right) }~,
    \]
    where $J$ is a random variable representing a trajectory, $z_j$ is defined in~\eqref{eq:zj}, and $\Pr{\hat{a}^j \mid \pi_{\hat{\theta}}}$ is defined in~\eqref{eq:p_defs}.
\end{theorem}

It now remains to derive the gradient of the logarithm of the trajectory probability. This can be done from elementary calculus as:
\begin{equation} \label{eq:gradient_formula}
    \begin{aligned}
        \nabla_\theta \log \left(\Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) &=
        \nabla_\theta \log \left(\prod_{t=0}^{T-1} \pi_{\theta^i}(\hat{s}^j_t,\hat{a}^j_t) \right)
        =  \sum_{t=0}^{T-1} \nabla_\theta \log \pi_{\theta^i}(\hat{s}^j_t,\hat{a}^j_t) \\
        &= \sum_{t=0}^{T-1} \frac{\nabla_\theta \pi_{\theta^i}(\hat{s}^j_t,\hat{a}^j_t)}{\pi_{\theta^i}(\hat{s}^j_t,\hat{a}^j_t)}~.
    \end{aligned}
\end{equation}
The actual formula for a softmax policy then follows by algebraic manipulation.

For a sketch of a complete policy gradient algorithm, see \cref{alg:policy_gradient}. The algorithm is based on gradient descent and uses Monte-Carlo to estimate the expectation in \eqref{eq:grad_expectation}.

\begin{algorithm}
    \KwIn{A stochastic dynamic program, a feature function $\phi:\states\times\actions\to\Real^K$, Monte-Carlo sample size $M$, step sizes $\alpha_0, \ldots$, gradient bound $\delta$}
    \KwOut{Parameter $\theta \in \Real^K$ used to construct the randomized policy}
    $i \gets 0$\;
    Initialize $\theta^0$ arbitrarily \;
    \While{$\|d^{i}\| \ge \delta$}{
        Sample $M$ trajectories $j_1, j_2, \ldots, j_M$ following $\pi_{\theta^i}$\;
        \tcp{Compute approximate direction from \cref{thm:grad} and~\eqref{eq:gradient_formula}}
        $d^i \gets \nabla_\theta\, v_0(s_0; \pi_{\theta^i}) \approx - \nicefrac{1}{M}\sum_{l=1}^M{z_{j_l} \cdot \nabla_\theta \log \left(\Pr{\hat{a}^{j_l} \mid \pi_{\theta^i}}\right) }$\;
        $\theta^{i+1} \gets \theta^i + \alpha_i \cdot d^i$\;
        $i \gets i + 1$\;
    }
    \Return{$\theta^i$}
    \caption{Policy gradient} \label{alg:policy_gradient}
\end{algorithm}



\end{document}
