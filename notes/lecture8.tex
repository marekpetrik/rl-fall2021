\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{mathabx}        % for \bigtimes
\usepackage{booktabs}
\usepackage{mathtools}
\usepackage{bbold}


% diagram stuff
\usepackage{pgf}
\usepackage{tikz}
\usepackage{url}

\usetikzlibrary{arrows,automata}

\lectureNotesTitle{Lecture 8: Linear Regression with FVI}
\date{September 30th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
	\maketitle

	\section{Linear Regression}

	Because this entire lecture focuses on a single time step $t$, we omit the time subscript throughout.

	We will now discuss how to approximate the value function using linear value function approximation. Recall that we are assuming that the optimal value function can be approximated using features $\phi: \states \to \Real^K$ for some $w$ as
	\[
	v\opt(s) \;\approx\; \tilde{v}(s) \;=\; \phi(s)\tr w = \sum_{i=1}^K \phi(s)_i w_i~.
	\]
	With this assumption, we are given some values $\hat{v}(\hat{s})$ for some small set (sample) of states $\hat\states \subseteq \states$. The goal is compute the best linear fit by finding weights $w\in\Real^K$ that solve the following optimization problem:
	\begin{equation} \label{eq:pinnacle}
		\min_{w\in\Real^K} \, \sum_{\hat{s}\in\hat{\states}} (\phi(\hat{s})\tr w - \hat{v}{(\hat{s})})^2~.
	\end{equation}
	This optimization problem is known under various names, but particularly, it is called \emph{linear regression} or \emph{least squares}.

	The most convenient way to solve for the optimal solution of the optimization problem in~\eqref{eq:pinnacle} is to formulate it using linear algebra. To this effect, we need the following definition~\footnote{\url{https://en.wikipedia.org/wiki/Design_matrix}}.
	\begin{defn}
		Assume some arbitrary, but fixed, order of the sampled states $\hat{\states} = \{\hat{s}_1, \ldots, \hat{s}_N\}$.
		Then, the \emph{design matrix} $A \in \Real^{N \times K}$ for the least squares problem~\eqref{eq:pinnacle} is defined as:
		\[
		A \;=\; \begin{bmatrix}
			- & \phi(\hat{s}_1)\tr & -\\
			- & \phi(\hat{s}_2)\tr & -\\
			- & \vdots & - \\
			- & \phi(\hat{s}_N)\tr & -
		\end{bmatrix}~.
		\]
		We will assume that $A$ is rank $K$. The \emph{target vector} $\hat{v}\in \Real^N$ is defined as:
		\[
		\hat{v} \;=\; \begin{bmatrix}
			\hat{v}(\hat{s}_1)\\
			\hat{v}(\hat{s}_2)\\
			\vdots  \\
			\hat{v}(\hat{s}_N) \\
		\end{bmatrix}~.
		\]
	\end{defn}


	Equipped with the definitions above, we can now solve the least squares problem as the following proposition states.
	\begin{prop}
		Consider the least-squares optimization problem
		\begin{equation} \label{eq:ls_opt}
			\min_{w\in\Real^K} \, \sum_{\hat{s}\in\hat{\states}} (\phi(\hat{s})\tr w - \hat{v}{(\hat{s})})^2 = \| A w - \hat{v} \|_2^2~.
		\end{equation}
		Whenever $\operatorname{rank}(A) = K \le N$, then
		\[
		w\opt = (A\tr A)^{-1} A\tr \hat{v}
		\]
		solves~\eqref{eq:ls_opt}.
	\end{prop}

	\section{Feature Construction}

	This section provides a brief summary of selected useful feature construction techniques. I suggest consulting Section 9.5 in the RL textbook for more details and other feature construction ideas.

	To keep things simple, we assume that the state space is single-dimensional and real: $\states \subseteq \Real$. List of simple state construction methods follows:
	\begin{enumerate}
		\item \emph{Linear}: Value functions are linear in the state space:
		\[
		\phi(s) \;=\;
		\begin{bmatrix}
			1 & s
		\end{bmatrix}\tr~.
		\]
		Note the transpose in the equation above; the vectors are specified in a row form to save on space.
		\item \emph{Polynomial}: Given some degree $K-1$:
		\[
		\phi(s) \;=\;
		\begin{bmatrix}
			1 & s & s^2 & \ldots & s^{K-1}
		\end{bmatrix}\tr~.
		\]
		\item \emph{Linear splines}: Given some knots $x_1, \ldots, x_{K-2}$:
		\[
		\phi(s) \;=\;
		\begin{bmatrix}
			1 & s & \max\{0, s-x_1\} & \max\{0, s-x_2\} & \ldots & \max\{0, s-x_{K-2}\}
		\end{bmatrix}\tr~.
		\]
		\item \emph{Piecewise-constant functions} are also known as aggregation and are a special case of tiling. Given breakpoints $x_1, \ldots, x_{K-1}$
		\[
		\phi(s) \;=\;
		\begin{bmatrix}
			\one\{s \le x_1 \} & \one\{x_1 \le s \le x_2 \} & \ldots & \one\{x_{K-2} \le s \le x_{K-1} \} & \one\{x_{K-1} \le s \}
		\end{bmatrix}\tr~.
		\]
		Here, $\one$ is the indicator function which is 1 when the argument is true and 0 otherwise.
		\item \emph{Radial basis functions} are a soft version of the piecewise constant functions. They are defined for some centers $c_1, \ldots, c_K$ and widths $\sigma_1, \ldots, \sigma_K$ as:
		\[
		\phi(s) \;=\;
		\begin{bmatrix}
			\exp\left(-\frac{(s - c_1)^2}{2 \sigma_1^2 }\right) & \exp\left(-\frac{(s - c_2)^2}{2 \sigma_2^2 }\right) & \ldots & \exp\left(-\frac{(s - c_K)^2}{2 \sigma_K^2 }\right)
		\end{bmatrix}\tr~.
		\]
	\end{enumerate}



	%	\bibliographystyle{plain}
	%	\bibliography{biblio}

\end{document}