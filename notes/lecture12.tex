\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{nicefrac}
\usepackage{url}
\usepackage{bbm}
\usepackage{bm}


\usepackage{fullpage}

\renewcommand{\*}{\bm}

\lectureNotesTitle{Lecture 12: Infinite Horizon Models: Policy Evaluation}
\date{October 21th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}

\newcommand{\tr}{^{\mathsf{T}}}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}

\newcommand{\one}{\mathbb{1}}


\begin{document}
	\maketitle
	
	\section{Introduction} \label{sec:intro}
	
	In addition to Chapter 4 in RLOC, I recommend that you also consult the following other resources. You can find a rigorous and clear treatment of the infinite horizon discounted MDPs in Chapter 6 of
	\begin{quote}
		Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. Wiley-Interscience.
	\end{quote}
	I suggest that you also take a look at Chapter 3 of
	\begin{quote}
		Sutton, R. S., \& Barto, A. G. (2018). Reinforcement learning: An introduction (2nd ed.). The MIT Press.
	\end{quote}
	The second book's pdf is available online and can be accessed using a link from the class website.
	
	This lecture continues the coverage of the foundations of infinite horizon models, targets the infinite horizon. We then also introduce the Markov Decision Process, which is the model used to optimize actions in sequential environments.
	
	\section{Markov Reward Process: Bellman Operator}
	
	Our goal in this lecture is to compute the value function for an infinite-horizon discounted return for a Markov reward process, which we defined in the last lecture. 
	
	As before, we assume a \emph{discount factor} $\gamma \in [0,1)$. The discount factor must be less than $1$ in order to avoid infinite value functions.
	
	It will be convenient to define the Bellman operator, which makes it convenient to analyze the properties of the value function.
	\begin{defn}[Bellman Operator]
		The Bellman operator $L:\Real^S \to \Real^S$ for a Markov reward process is defined for $\*x \in \Real^S$ as
		\[
		L \*x \;=\; \*r + \gamma \cdot \*P \*x~.
		\]
	\end{defn}
	
	The Bellman operator resembles a matrix, which is also an operator that works on vectors. The difference is that matrices are linear, and the Bellman operator is not linear.
	
	Using the Bellman operator, we can simplify the Bellman equations as follows.
	\begin{cor}
		The value function $v_{t,T}:\states \to \Real$ for $0 \le t \le T$ of a Markov reward process satisfies:
		\begin{align*}
			\*v_{t,T} &\;=\; L \*v_{t+1,T} \\
			\*v_{t,T+1} &\;=\; L \*v_{t,T} 
		\end{align*}
	\end{cor}
	
	\section{Infinite-Horizon Value Function}
	
	The infinite-horizon value function $v_{0,\infty}: \states \to \Real$ is defined for each $s\in\states$ as
	\[
	v_{0,\infty}(s) \;=\; \lim_{T \to\infty} v_{0,T}(s)~.
	\]
	As we will see, this limit always exists. 
	
	A convenient representation for the value function is as follows.
	\begin{prop}
		The value function $v_{0,T}: \states \to \Real$ for any $T \ge 0$ satisfies that
		\[
		\*v_{0,T} \;=\; \sum_{l = 0}^T \gamma^l \*P^l \*r~.
		\]
	\end{prop}
	\begin{proof}
		The proof proceeds by induction on $T$. The base case follows directly from the definition of the terms. For the inductive case, assume that the equality holds for $T$, and we need to prove it for $T+1$. Then
		\[
		\*v_{0,T+1} = L \*v_{0,T} = \*r + \gamma \*P \*v_{0,T} = \*r + \gamma \*P \sum_{l = 0}^T \gamma^l \*P^l \*r = \sum_{l = 0}^{T+1} \gamma^l \*P^l \*r~,
		\]
		which proves the result.
	\end{proof}
	
	An important concept that we need is the notion of the $L_\infty$ norm, which is defined for any $\*x\in\Real^S$ as:
	\[
	\|\*x\|_\infty = \max_{i = 1, \ldots, S} |x_i|~.
	\]
	
	The Bellman operator is a contraction under the $L_\infty$ norm.
	\begin{theorem}
		The Bellman operator $L:\Real^S \to \Real^S$ is a $\gamma$-contraction in the $L_\infty$ norm. That is,
		\[
		\| L \*x - L \*y \|_\infty \;\le\; \gamma\cdot \| \*x - \*y \|_\infty~,
		\]
		for any $\*x, \*y \in \Real^S$.
	\end{theorem}
	See Proposition 6.2.4 in \cite{Puterman2005} for the proof.
	
	Because the Bellman operator is a contraction, we can employ the well-known Banach fixed point theorem, which is stated next.
	\begin{theorem}[Banach Fixed Point Theorem, Proposition 6.2.3 in \cite{Puterman2005}] \label{thm:banach}
		Suppose that $\mathcal{U}$ is a Banach space and $T : \mathcal{U} \to \mathcal{U}$ is a contraction mapping for some norm. Then:
		\begin{enumerate}
			\item There exists a unique fixed point $\*v\opt \in \mathcal{U}$ such that $T \*v\opt = \*v\opt$, and 
			\item For arbitrary $\*x\in\mathcal{U}$ we have that
			\[
			\lim_{n\to\infty} T^n \*x = \*v\opt~.
			\]
		\end{enumerate}
	\end{theorem}
	
	Combining the fixed point \cref{thm:banach} with the fact that the Bellman operator $L$ is a contraction, we get that
	\begin{align}
		\label{eq:value_stationary}
		\*v_{0,\infty} &= L \*v_{0,\infty}~, \\
		\label{eq:value_limit}
		\*v_{0,\infty} &= \lim_{n\to\infty} T^n \*x,
	\end{align}
	for any $\*x \in \Real^S$. 
	
	\section{Computing Value Function}
	
	We now consider two methods for computing the value function for a Markov reward process using the properties established above.
	
	
	The first algorithm is an iterative method based on \eqref{eq:value_limit} and it is summarized in \cref{alg:value_iteration}. The algorithm runs a number of iterations, and in each one, it updates the value function. It is given a number of iterations and outputs an approximation of $\*v_{0,\infty}$. The approximation gets better with an increasing number of iterations $N$.
	
	\begin{algorithm}
		\KwIn{Initial value function $\*x\in \Real^S$, and number of iterations $N$}
		\KwOut{An approximation of the value funtion $\*v_{0,\infty}$}
		$\*v_0 \gets \*x$\;
		\For{$n \in 1, \ldots, N$}{
			$\*v_n \gets L \*v_{n-1}$\;
		}
		\Return{$\*v_N$}\;
		\caption{Value iteration algorithm for a Markov reward process.}
		\label{alg:value_iteration}
	\end{algorithm}
	
	\cref{alg:value_iteration} is also known as the Jacobian version of value iteration. See Chapter 6 of \cite{Puterman2005} for a Gauss-Seidel version of value iteration.
	
	Another approach, which is generally faster and more precise, is to solve the fixed point equation in~\eqref{eq:value_stationary}. The equation translates to 
	\[
	\*v_{0,\infty} = L \*v_{0,\infty} = \*r + \gamma \cdot \*P \*v_{0,\infty}~.
	\]
	Subtracting $\gamma \cdot \*P$ from both sides and multiplying by $(\*I - \gamma \cdot \*P)^{-1}$ on the left we get that
	\[
	\*v_{0,\infty} \;=\; (\*I - \gamma \cdot \*P)^{-1} \*r~.
	\]
	That means that the value function can be computed by solving a system of linear equations. \cref{alg:inverse} summarizes this procedure.
	
	\begin{algorithm}
		\KwOut{Value funtion $\*v_{0,\infty}$}
		\Return{$(\*I - \gamma \cdot \*P)^{-1} \*r$}\;
		\caption{Value function computation via linear equations for a Markov reward process.}
		\label{alg:inverse}
	\end{algorithm}	
	
	
	\bibliographystyle{plain}
	\bibliography{biblio}
\end{document}

