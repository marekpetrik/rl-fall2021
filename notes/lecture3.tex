\documentclass[letterpaper,11pt]{article}

\usepackage{lectureNotes}
\usepackage{mathabx}        % for \bigtimes
\usepackage{booktabs}
\usepackage{mathtools}

% diagram stuff
\usepackage{pgf}
\usepackage{tikz}

\usetikzlibrary{arrows,automata}

\lectureNotesTitle{Lecture 3 and 4: Stochastic Dynamic Programming}
\date{September 7th and 9th, 2021}
\author{Marek Petrik}

\newcommand{\dist}{\mathcal{W}}


% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers


\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\begin{document}
	\maketitle
	
	\section{Primer on Random Variables and Expected Value}
	
	We will use capital letters to denote random variables. A real-valued random variable $X : \Omega \to \Real$ is a mapping from the space of outcomes $\Omega$ to real numbers. It is a common convention to denote all random variables as upper case.
	
	For example, the set $\Omega$ can represent the customers of a store. One random variable may represent the income of the customers. That is, it is a function that assigns the income to each one of the customers. Another random variable defined on the same space $\Omega$ may represent the height of the customers.
	
	One can compute the expected value of any real-valued random variable as:
	\[ 
	\Ex{X} = \sum_{\omega\in\Omega} p(\omega) \cdot X(\omega)
	\]
	when $\Omega$ is finite and $p : \Omega \to \Real$ is the probability measure.
	
	Random variables may map outcomes to any other quantity, such as states. For instance, we can define a random variable $S:\Omega \to \states$. But unless $\states \subseteq \Real$ we may be unable to compute $\Ex{S}$.
	
	Occasionally, we will use the subscript notation $\Exx{X \sim p}{X}$ to emphasize the random variable and its distribution in the definition of the expectation.  We will omit the subscript when the random variables are obvious from the context. 
	
	I recommend Wikipedia as a good and quick source of properties of the expectation operator. A property that is important for these lectures is the law of total expectation, also known as the tower property.
	
	\begin{theorem}[Tower property] \label{thm:tower}
		Let $X$ and $Y$ be two random variables. Then:
		\[ \Exx{X}{X} = \Exx{Y}{\Exx{X}{X \mid Y} } ~.\]
	\end{theorem}
	
	\section{Stochastic Dynamic Program}
	See \cref{tab:translation} for the correspondence between the notation common in RL (what we are using) and Bertsekas's RLOC book.
	
	\begin{table}
		\centering
		\begin{tabular}{lcc}
			\toprule
			\textbf{Meaning} & \textbf{RL Notation} & \textbf{RLOC Notation} \\
			\midrule
			Time step & $t$ & $k$ \\
			Horizon & $T$ & $N$ \\
			State & $s_t$ & $x_k$ \\
			Action/Control & $a_t$ & $u_k$ \\
			Reward & $r_t$ & -- \\
			Cost & -- & $g_k$ \\
			Value function & $v_k$ & $J_k$  \\
			\bottomrule
		\end{tabular}
		\caption{Translating notations. See also Section 1.4 in the RLOC textbook. } \label{tab:translation}
	\end{table}
	
	
	\paragraph{Components}
	A \emph{stochastic} dynamic program consists of:
	\begin{enumerate}
		\item State set (or space): $\states$
		\item Action set (or space): $\actions$
		\item Disturbance set: $\dist$
		\item Horizon: $T \in \Natural$
		\item Distribution $p_t$ for the disturbance at time $t$. The random events are denoted as $W_t$ and distributed as $W_t \sim p_t$
		\item Reward function: $r_t : \states \times \actions \times \dist \to \Real$ for $t=0,\ldots, T-1$, and $r_T : \states \to \Real$
		\item Transition function: $f_t: \states \times \actions \times \dist \to \states$ for $t=0,\ldots, T-1$
		\item Initial state: $s_0 \in \states$
	\end{enumerate}
	To avoid technicalities that would distract from the main ideas, we are assuming that all sets involved in the definition above are finite.
	
	A crucial assumption that we are making here is that the disturbances $W_t$ at each time $t = 0,\ldots,T-1$ step are all \emph{mutually independent}:
	\begin{equation} \label{eq:independence}
		\Pr{W_0 = w_0, \ldots , W_{T-1} = w_{T-1}} = \Pr{W_0 = w_0} \cdot \ldots \cdot \Pr{W_{T-1} = w_{T-1}}~,
	\end{equation}
	for each $w_0, \ldots, w_T \in \mathcal{W}$. Note that pairwise independence is not sufficient.
	
	\begin{defn}[Policy] \label{def:policy}
		The solution to a stochastic dynamic program is a sequence of decision rules $\pi_0, \ldots, \pi_{T-1}: \states \to \actions$ which can also be denoted more concisely as $\pi = (\pi_t)_{t=0}^{T-1}$. 
	\end{defn}
	
	As in the deterministic setting, not all solutions/policies are  optimal. We will denote the optimal solution by $\pi\opt = (\pi_t\opt)_{t=0}^{T-1}$.
	
	The policies in~\cref{def:policy} are referred to as Markovian deterministic policies. They are Markovian because they depend only on the most recent state, and they are deterministic because they prescribe a specific action instead of choosing one randomly. The set of Markovian deterministic policies is denoted as $\Pi_D$.
	
	Value functions will play a crucial role in defining the objective (goal) of a dynamic program and computing its solutions.
	\begin{defn}[Value Function] \label{def:value_function}
		For a stochastic dynamic program, we define the value function $v_t : \states \times \Pi_D \to \Real$ for $t=0,\ldots,T$ and policy $\pi\in\Pi_D$ any $s_t \in \states$ as:
		\begin{equation*}
			v_t(s_t; \pi) \;=\; \Ex{\sum_{k=t}^{T-1} r_k(S_k, A_k, W_k) + r_T(S_T) ~\middle\vert~ 
				\begin{matrix} S_k = f_{k-1}(S_{k-1}, A_{k-1}, W_{k-1}),\\ S_0 = s_0, A_k = \pi_k(S_{k}) \end{matrix}}
		\end{equation*}
		The value $v_0(s_0; \pi)$ is often referred to as the \emph{return} of the policy $\pi$.
	\end{defn}
	The $;$ separating states and actions in the definition of $v_t$ is just to emphasize the distinction between the different types of parameters.
	
	When the set of disturbances is finite, we can express the value function for each $s_t\in\states$ using the following sum:
	\begin{equation} \label{eq:sum_representation}
		\begin{aligned}
			v_t(s_t; \pi) &\;=\; \Ex{\sum_{k=t}^{T-1} r_k(S_k, A_k, W_k) + r_T(S_T) ~\middle\vert~ \begin{matrix}S_k = f_{k-1}(S_{k-1}, A_k, W_{k-1}),\\ S_0 = s_0, A_k = \pi_k(S_{k-1})\end{matrix} } \\
			&\;=\; \sum_{(w_k)_{k=t}^{T-1} \in \dist^{T-t}} \Pr{W_t = w_t, \ldots , W_{T-1} = w_{T-1}}  \cdot \left(\sum_{k=t}^{T-1} r_k(s_k, a_k, w_k) + r_T(s_T) \right) \\ 
			&\;=\; \sum_{(w_k)_{k=t}^{T-1} \in \dist^{T-t}} \prod_{k=t}^{T-1}\Pr{W_k = w_t}  \cdot \left(\sum_{k=t}^{T-1} r_k(s_k, a_k, w_k) + r_T(s_T) \right) ~,
		\end{aligned}
	\end{equation}
	where the last equality follows from the independence assumption in~\eqref{eq:independence} and 
	\[
	s_k = f_{k-1}(s_{k-1}, a_{k-1}, w_{k-1})
	\]
	for $k = t+1, \dots, T-1$.
	
	\begin{defn}[Objective]
		Our goal when solving a dynamic program is to maximize the sum of the rewards accumulated throughout the execution.
		\begin{equation} \label{eq:objective}
			\max_{\pi\in\Pi_D} \; v_0(s_0; \pi) ~,
		\end{equation}
		where $v_0$ is defined in \cref{def:value_function}.
	\end{defn}
	The optimal value of the objective in~\eqref{eq:objective} is often referred to as the \textbf{optimal return} or total return.
	
	The \emph{optimal policy} is a policy $\pi\opt$ that satisfies:
	\begin{equation} \label{eq:optimal_solution}
		\pi\opt \;\in\; \arg\max_{\pi\in\Pi_D} \; v_0(s_0; \pi)~.
	\end{equation}
	
	The optimal value function is a crucial component of almost all reinforcement learning algorithms and is defined as follows.
	\begin{defn}[Optimal Value Function]
		The optimal value function $v\opt_t : \states \to \Real$ is defined for any $t=0,\ldots,T$ and $s_t\in\states$ as:
		\begin{equation}
			v\opt_t(s_t) \;=\;  \max_{\pi\in\Pi_D} \; v_t(s_t; \pi) ~.
		\end{equation}
	\end{defn}
	
	\section{Policy Evaluation: Dynamic Programming Approach}
	
	We first tackle the problem of computing the value function $v_t$ for a fixed (given) policy $\pi$. Although one can compute the value function using simulation to approximate the sum in~\eqref{eq:sum_representation}, this is not efficient. Using the dynamic program that we propose here has time complexity that is linear in the number of states, actions, and disturbances.
	
	If you would like to see how the results in this section can be derived formally, please see Chapter 3 in~\cite{Shapiro2014}.
	
	The following key recursive property will be instrumental when computing the value function for a given policy.
	
	\begin{theorem}[Bellman Evaluation Equation]
		The value function $v_t : \states \times \Pi_D\to \Real, t=0,\ldots,T$ for a deterministic policy $\pi = (\pi_k)_{k=0}^{T-1}, \pi_k : \states \to \actions$ is the unique value that satisfies :
		\begin{equation} \label{eq:dynamic_condition}
			\begin{aligned}
				v_T(s;\pi) &\;=\; r_T(s) ~,\\
				v_t(s;\pi) &\;=\; \Exx{W_t \sim p_t}{r_t(s,\pi_t(s), W_t) + v_{t+1}(f_t(s,\pi_t(s), W_t);\pi) }, \qquad t = 0, \ldots, T-1~,
			\end{aligned}
		\end{equation}
		for all states $s\in\states$.
	\end{theorem}
	\begin{proof}
		The proof follows by a backward induction on $t$. The initial case with $t = T$ follows directly from the definition. Assume now that $v_{t+1}$ is the unique solution to~\eqref{eq:dynamic_condition}. Then for all $s\in\states$:
		\begin{align*}
			v_t(s;\pi) &= \Ex{\sum_{k=t}^{T-1} r_k(S_k, \pi_k(S_k), W_k) + r_T(S_T) \mid S_k = f_{k-1}(S_{k-1}, \pi_{k-1}(S_{k-1}), W_{k-1}), S_0 = s_0} \\
			&= \Exx{W_t}{r_t(s,\pi_t(s),W_t) + \Exx{W_{t+1}}{r_{t+1}(S_{t+1}, \pi_{t+1}(S_{t+1}), W_{t+1}) + \Ex{\ldots} \mid W_t}} \\
			&= \Exx{W_t}{r_t(s,\pi_t(s),W_t) + v_{t+1}(S_{t+1}; \pi) } \\
			&= \Exx{W_t \sim p_t}{r_t(s,a, W_t) + v_{t+1}(f_t(s,a, W_t);\pi) }~.
		\end{align*}
		The second equality follows from the tower property (see \cref{thm:tower}), and the remainder follows from algebraic manipulation and basic properties of the expectation operator. 
	\end{proof}
	
	See \cref{alg:policy_eval} for a sketch of a dynamic programming algorithm.
	
	\begin{algorithm}
		\KwIn{Definition of a stochastic dynamic program}
		\KwOut{Value function $v_t(s;\pi),t=0,\ldots,T-1,s\in\states$ for a policy $\pi$}
		\tcp{Backward pass to compute $v$}
		$v_T(s;\pi) \gets r_T(s), \forall s\in\states$ \;
		\For{$t = T-1, \ldots, 0$}{
			$v_t(s;\pi) \gets \Exx{W_t}{r_t(s,\pi_t(s), W_t) + v_{t+1}(f_t(s,\pi_t(s),W_t);\pi) } , \forall s\in\states$\;
		}
		\Return{Value function $v_t(s;\pi),t=0,\ldots,T-1,s\in\states$.}
		\caption{Policy evaluation} \label{alg:policy_eval}
	\end{algorithm}
	
	
	\section{Policy Optimization: Dynamic Programming Approach}
	
	We are now ready to discuss a procedure that can solve for the optimal policy. 
	
	It will be convenient to define q-values, also known as q-factors or state-action value functions. For now, they will just make some of our notation more convenient but will be instrumental later when working with model-free algorithms and Q-learning.
	\begin{defn}[State-action Value Function, Q-values] 
		Consider a given deterministic policy $\pi\in\Pi_D$. Then the state-action value function $q_t: \states \times \actions \times \Pi_D \to \Real$ for each $s\in\states, a\in\actions$ and $t=0,\ldots,T-1$ is defined as
		\[
		q_t(s,a;\pi) = \Exx{W_t}{r_t(s,a, W_t) + v_{t+1}(f_t(s,a,W_t);\pi) }~.
		\]
		Similarly, the optimal state-action value function $q_t\opt: \states \times \actions \to \Real$ for each $s\in\states, a\in\actions$ and $t=0,\ldots,T-1$ is defined as
		\[
		q_t\opt(s,a) = \Exx{W_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a,W_t)) }~.
		\]
	\end{defn}
	
	
	The following theorem states the dynamic programming results for a finite horizon problem. 
	
	\begin{theorem}[Bellman Optimality Equation]
		The optimal value function $v_t\opt : \states \times \Pi_D\to \Real, t=0,\ldots,T$ is the unique value that satisfies
		\begin{equation} \label{eq:dynamic_condition}
			\begin{aligned}
				v_T\opt(s) &\;=\; r_T(s) ~,\\
				v_t\opt(s) &\;=\; \max_{a\in\actions}\; \Exx{W_t \sim p_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a, W_t)) }, \qquad t = 0, \ldots, T-1~,
			\end{aligned}
		\end{equation}
		for all states $s\in\states$.
	\end{theorem}
	\begin{proof}[Proof sketch]
		The proof follows by a backward induction on $t$. The initial case with $t = T$ follows directly from the definition. Assume now that $v_{t+1}$ is the unique solution to~\eqref{eq:dynamic_condition}. Then for all $s\in\states$:
		\begin{align*}
			v_t\opt(s) &= \max_{\pi\in\Pi_D} \; v_t(s;\pi) \\
			&= \max_{\pi\in\Pi_D} \;  \Ex{\sum_{k=t}^{T-1} r_k(S_k, \pi_k(S_k), W_k) + r_T(S_T) \mid \ldots} \\
			&= \max_{\pi\in\Pi_D} \; \Exx{W_t}{r_t(s,\pi_t(s),W_t) + \Exx{W_{t+1}}{r_{t+1}(S_{t+1}, \pi_{t+1}(S_{t+1}), W_{t+1}) + \Ex{\ldots} \mid W_t}} \\
			&= \max_{a\in\actions} \; \Exx{W_t}{r_t(s,a,W_t) + \max_{\pi\in\Pi_D} \; \Exx{W_{t+1}}{r_{t+1}(S_{t+1}, \pi_{t+1}(S_{t+1}), W_{t+1}) + \Ex{\ldots} \mid W_t}} \\		
			&= \max_{a\in\actions} \; \Exx{W_t}{r_t(s,a,W_t) + v_{t+1}\opt(S_{t+1}) } \\
			&= \max_{a\in\actions}\; \Exx{W_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a, W_t)) }~.
		\end{align*}
		The third equality follows from the tower property (see \cref{thm:tower}) and the fourth equality follows from Theorem 7.92 in \cite{Shapiro2014}. The remainder follows from algebraic manipulation and basic properties of the expectation operator. 
	\end{proof}
	
	Note that the second equality in~\eqref{eq:dynamic_condition} can also be restated as
	\[
	v_t\opt(s) \;=\; \max_{a\in\actions} \; q\opt_{t}(s,a), \qquad \forall s \in \states~.
	\]
	
	\begin{algorithm}
		\KwIn{Definition of a stochastic dynamic program}
		\KwOut{Optimal value function $v\opt$ and policy $\pi\opt$ }
		\tcp{Backward pass to compute $v\opt$}
		$v_T\opt(s) \gets r_T(s), \forall s\in\states$ \;
		\For{$t = T-1, \ldots, 0$}{
			$v_t\opt(s) \gets \max_{a\in\actions} \Exx{W_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a,W_t)) } , \forall s\in\states$\;
			$\pi_t\opt(s) \gets \arg\max_{a\in\actions} \; \Exx{W_t}{r_t(s,a, W_t) + v_{t+1}\opt(f_t(s,a,W_t)) } , \forall s\in\states$ \;
		}
		\tcp{Note that the policy can be computed in the backward pass}
		\Return{Optimal  value function $v\opt$ and policy $\pi\opt$.}
		\caption{Policy optimization} \label{alg:policy_optim}
	\end{algorithm}
	
	See \cref{alg:policy_optim} for a sketch of a dynamic programming algorithm that computes the optimal value function and policy.
	
	
	
	
	\bibliographystyle{plain}
	\bibliography{biblio}
	
\end{document}