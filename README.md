# CS 780 / 880: Reinforcement Learning


### When and Where

Lectures: TR 3:40pm - 5pm
 
Where: Parsons N116

See [overview and rules](overview/overview.pdf) for more information about the class.

## Syllabus: Lectures

Regarding *reading materials* see the section on textbooks below. The schedule is tentative and may be adjusted as we progress through the class.


| Date   | Day | Topic                                             | Reading               | Code                                |
|--------|-----|---------------------------------------------------|-----------------------|-------------------------------------|
| Aug 31 | Tue | Introduction                                      | RLOC 1                |                                     |
| Sep 02 | Thu | [Dynamic programming](notes/lecture2.pdf)         | RLOC 1.1              |                                     |
| Sep 07 | Tue | [Stochastic DP](notes/lecture3.pdf)               | RLOC 1.2              |                                     |
| Sep 09 | Thu | [Stochastic DP II](notes/lecture3.pdf)            | RLOC 1.2              |                                     |
| Sep 14 | Tue | [Approximations](notes/lecture4.pdf)              | RLOC 2.1              |                                     |
| Sep 16 | Thu | [Lookeahead](notes/lecture5.pdf)                  | RLOC 2.2-3            |                                     |
| Sep 21 | Tue | [Rollouts](notes/lecture6.pdf)                    | RLOC 2.4-6            |                                     |
| Sep 23 | Thu | [Solutions, discussion](solutions/assignment2)    |                       |                                     |
| Sep 28 | Tue | [Fitted value iteration](notes/lecture7.pdf)      | RLOC 3.3              |                                     |
| Sep 30 | Thu | [Linear regression](notes/lecture8.pdf)           | RLOC 3.1-2            | [py](examples/linear_regression.py) |
| Oct 05 | Tue | [Approximation errors](notes/lecture9.pdf)        | RLOC 3.3              |                                     |
| Oct 07 | Thu | [Policy gradient](notes/lecture10.pdf)            | RLOC 3, 4.11, RL 13.1 |                                     |
| Oct 12 | Tue | Discussion, demos, solutions, overview            |                       |                                     |
| Oct 14 | Thu | Midterm (class optional)                          |                       |                                     |
| Oct 19 | Tue | [Markov chains](notes/lecture11.pdf)              | RLOC 4.1-3            |                                     |
| Oct 21 | Thu | [Markov reward process](notes/lecture12.pdf)      | RLOC 4.3              |                                     |
| Oct 26 | Tue | [Markov decision process](notes/lecture13.pdf)    | RLOC 4.3,4.5,4.6      |                                     |
| Oct 28 | Thu | [Value iteration](notes/lecture14.pdf)            | RLOC 5.1-2, RL 9.8    |                                     |
| Nov 02 | Tue | [LSTD](notes/lecture15.pdf)                       | RL 9.8, RLOC 5.1-2    |                                     |
| Nov 04 | Thu | [Data-driven LSTD](notes/lecture16.pdf)           | RL 9.4-8, RLOC 5.5    |                                     |
| Nov 09 | Tue | [LSPI](notes/lecture17.pdf)                       | RLOC 5.4              |                                     |
| Nov 11 | Thu | *No class*                                        |                       |                                     |
| Nov 16 | Tue | Discussion, demos, solutions                      |                       |                                     |
| Nov 18 | Thu | [TD and SARSA](notes/lecture18.pdf)               | RL 5.7, 10.1          |                                     |
| Nov 23 | Tue | [Linear programs](notes/lecture19.pdf)            | Puterman 6            |                                     |
| Nov 25 | Thu | *No class*                                        |                       |                                     |
| Nov 30 | Tue | Policy gradient                                   |                       |                                     |
| Dec 02 | Thu | Exploration / exploitation tradeoff               | RL 1,2                |                                     |
| Dec 07 | Tue | Project presentations                             |                       |                                     |
| Dec 09 | Thu | Project presentations                             |                       |                                     |


## Office Hours

*Piazza*: [piazza.com/unh/fall2021/cs780 ](https://piazza.com/unh/fall2021/cs780)

- *Marek*: Tue 8:30-9:30am in Kingsbury N215b or [https://unh.zoom.us/j/5833938643](https://unh.zoom.us/j/5833938643) (email if using zoom)
- *Marek*: Thu 1-2pm in Kingsbury N215b or [https://unh.zoom.us/j/5833938643](https://unh.zoom.us/j/5833938643) (email if using zoom)
- *Bahram (TA)*: Fri 1:00pm-2:30pm in Kingsbury W244


## Assignments

There will be 12 (approximately) weekly assignments. Tentative due dates for the assignments are below. The dates on canvas are authoritative.

| Assignment                         | Source                              |  Due Date            |
|----------------------------------- | ----------------------------------- | ---------------------|
| [1](assignments/assignment1.pdf)   | [1](assignments/assignment1.tex)    | Wed 09/08 at 11:59PM |
| [2](assignments/assignment2.pdf)   | [2](assignments/assignment2.tex)    | Wed 09/15 at 11:59PM |
| [3](assignments/assignment3.pdf)   | [3](assignments/assignment3.tex)    | Wed 09/22 at 11:59PM |
| [4](assignments/assignment4.pdf)   | [4](assignments/assignment4.tex)    | Wed 09/29 at 11:59PM |
| [5](assignments/assignment5.pdf)   | [5](assignments/assignment5.tex)    | Wed 10/06 at 11:59PM |
| [6](assignments/assignment6.pdf)   | [6](assignments/assignment6.tex)    | Thu 10/21 at 11:59PM |
| [7](assignments/assignment7.pdf)   | [7](assignments/assignment7.tex)    | Fri 10/29 at 11:59PM |
| [8](assignments/assignment8.pdf)   | [8](assignments/assignment8.tex)    | Thu 11/04 at 11:59PM |
| [9](assignments/assignment9.pdf)   | [9](assignments/assignment9.tex)    | Thu 11/18 at 11:59PM |
| [10](assignments/assignment10.pdf) | [10](assignments/assignment10.tex)  | Fri 12/03 at 11:59PM |
| [11](assignments/assignment11.pdf) | [11](assignments/assignment11.tex)  | Mon 12/13 at 11:59PM |
| [12](assignments/assignment12.pdf) | [12](assignments/assignment12.tex)  | Canceled             |


See the folder [solutions](solutions) for the solutions to selected problems.

## Exams

Both midterm and final exams will be take-home. You will have at least 24 hours to complete each one.

## Quizzes

There will be two quizzes during the semester. One before the midterm and one before the final. They will help with the review for the exam.

## Project


The project will involve a group (1-5) of students working on a project of their choice. The deliverable is to produce a report and a presentation at the end of the class. Good  ideas for a class project are:

- solve a simple, realistic problem using reinforcement learning
- implement and investigate the behavior of an algorithm that we have not covered in the class
- implement an algorithm from a research paper 
- understand and extend a theoretical result from beyond the class. You need to be able to give the gist of the argument in a 10-minute presentation to your classmates. 
- prove a new result (a small one) about a reinforcement learning / MDP algorithm 

The final presentation should be about 10 minutes long, but we will adjust the length based on the number of projects. The final report should be about 1-3 pages long.

If you would like to look for some inspiration, I suggest that you browse some relevant reinforcement learning research papers. You can find such papers in machine learning conferences and journals. Some examples follow.

Most-relevant machine learning conferences:

- International Conference on Machine Learning (ICML)
- Neural Information Processing Systems (NeurIPS)
- Artificial Intelligence and Statistics (AI-Stats)

Machine learning journals:

- Journal of Machine Learning Research
- Machine Learning
- Journal of Artificial Intelligence Research

You are also likely to find some exciting, relevant research in Operations Research journals. I recommend:

- Operations Research (springer)
- European Journal on Operational Research (EJOR)
- Mathematics Operations Research
- Management Science

I am happy to talk about some project ideas and suggest some papers and topics any time during my office hours. It is probably easier to chat in person than to bounce ideas over email. 



## Textbooks ##

## Main References
- **RLOC**: Berstekas (2019), [Reinforcement Learning and Optimal Control](http://web.mit.edu/dimitrib/www/RLbook.html)
- **RL**: Sutton, R. S., & Barto, A. (2018), [Reinforcement Learning](http://incompleteideas.net/book/the-book-2nd.html). MIT Press. **2nd Edition**
- **MDP**: Puterman, M. L. (2005), Markov decision processes: Discrete stochastic dynamic programming. John Wiley & Sons, Inc.

### Other sources

- **ARL**: Szepesvári, C. (2010), [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html).
- **ADP**: Powell (2011), [Approximate Dynamic Programming: Solving the Curses of Dimensionality](https://www.amazon.com/Approximate-Dynamic-Programming-Dimensionality-Probability/dp/047060445X)
- **NDP**: Bersekas and Tsitsiklis (1996), [Neuro-dynamic Programming](http://www.athenasc.com/ndpbook.html)

### Linear Algebra:
- **LAO**: Hefferon, J. [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)
- **LA**: Strang, G. [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/). (2016) *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)
- **LAR**: [Introductory Linear Algebra with R](http://bendixcarstensen.com/APC/linalg-notes-BxC.pdf)


### Probability and Statistics ###
- **IP**: Grinstead, C., & Snell, J. (1998). [Introduction to probability](https://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/amsbook.mac.pdf).
- **IST**: Lavine, M. (2005). [Introduction to statistical thought](http://people.math.umass.edu/~lavine/Book/book.html).


