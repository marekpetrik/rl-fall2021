\documentclass[letterpaper]{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{times}

\renewcommand{\*}{\bm}

\newcommand{\real}{\mathbb{R}}
\newcommand{\tr}{^\top}
\newcommand{\opt}{^\star}
\newcommand{\eye}{\bm{I}}
\DeclareMathOperator{\rank}{rank}
\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0mm}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers

\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\title{Assignment 8}
\author{Reinforcement Learning}
\date{\textbf{Solution}}


\begin{document}
\maketitle


\section*{Problem 1:  State-action Value Function}

For this problem, you may want to refer to lecture notes 11 and 12.

Recall the definition of the q-function, or the state-action value function $q$ from earlier. The state-action value function $q^\pi: \states \times \actions \to \Real$ for a stationary policy $\pi:\states\to\actions$ in the infinite horizon setting is defined as
\[
q^\pi(s,a) \;=\; \Ex{r(s, a) + \sum_{t=1}^\infty \gamma^{t} \cdot r(S_t, \pi(S_t)) \mid S_0 = s, S_{t+1} \sim P(S_t, \pi(S_t), \cdot)}~.
\]
Let $\pi\opt$ be the optimal policy as defined in the lecture notes. Then, the optimal state-action value function $q\opt: \states\times\actions$ is defined as $q\opt(s,a) = q^{\pi\opt}(s,a)$ for each state $s$ and action $a$.

Please answer the following questions, assuming some arbitrary stationary policy $\pi$:
\begin{enumerate}
\item Express $q^\pi(s,a)$ as a function of $v^\pi$.
\paragraph{Solution:}
\begin{align*}
q^\pi(s,a) &= \Ex{R_{t+1} + \gamma v^\pi(S_{t+1}) ~|~ S_t = s, A_t = a } \\
&= r(s,a) + \gamma \cdot \sum_{s' \in\states} p(s,a,s')  v^\pi(s')~.
\end{align*}

\item Derive the Bellman equation for $q^\pi$. The Bellman equation should not involve the value function.

\paragraph{Solution:}
\begin{align*}
q^\pi(s,a) &= \Ex{R_{t+1} + \gamma q^\pi(S_{t+1}, \pi(S_{t+1})) ~|~ S_t = s, A_t = a } \\
&= r(s,a) + \gamma \cdot \sum_{s'\in\states} p(s,a,s') q^\pi(s', \pi(s'))~.
\end{align*}
\item Express $q\opt(s,a)$ as a function of $v\opt$.
\paragraph{Solution:}
\begin{align*}
q\opt(s,a) &= \Ex{R_{t+1} + \gamma  v\opt(S_{t+1}) ~|~ S_t = s, A_t = a } \\
&= r(s,a) + \gamma \cdot \sum_{s'\in\states} p(s,a,s') v\opt(s')~.
\end{align*}



\item Derive the Bellman equation for $q\opt$. The Bellman equation should not involve the value function.

\paragraph{Solution:}
\begin{align*}
q\opt(s,a) &= \Ex{R_{t+1} + \gamma \max_{a'\in\actions} q\opt(S_{t+1}, a') ~|~ S_t = s, A_t = a } \\
&=  r(s,a) + \gamma \cdot \sum_{s' \in\states} p(s,a,s') \max_{a'} q\opt(s', a')~.
\end{align*}
\end{enumerate}

\section*{Problem 2: LSTD}


In this problem, we will investigate the formulation and solution to the LSTD fixed point problem. Recall that in this setting, we assume that we have a feature function $\*\phi: \states \to \Real^K$ defined for each state. The feature matrix $\*\Phi \in \Real^{S\times K}$ consists of row vectors as features. Given features, LSTD assumes that the approximate value function $\tilde{\*v}^\pi$ for some policy $\pi$ can be expressed as a function of $\*w\in\Real^K$ as
\[
\tilde{\*v}^\pi = \*\Phi \*w~.
\]
Given the assumptions above, the formula for LSTD is
\begin{equation}\label{eq:lstd}
\*w \;=\; (\*\Phi\tr \*\Phi - \gamma \cdot \*\Phi\tr \*P^\pi \*\Phi)^{-1} \*\Phi\tr \*r^\pi~.
\end{equation}

With the information above, please answer the following questions.
\begin{enumerate}
\item Prove that the LSTD solution solves for the fixed point of the projected Bellman equation:
\[
\*w \;=\; (\*\Phi\tr\*\Phi)^{-1} \*\Phi\tr (\*r^\pi + \gamma \cdot \*P^\pi \*\Phi \*w)~.
\]
\paragraph{Solution:}
\begin{align*}
\tilde{\*v}^\pi &= L^\pi \tilde{\*v}^\pi \\
\*\Phi \*w &= \*r^\pi + \gamma \cdot \*P^\pi  \*\Phi \*w \\
\*\Phi\tr \*\Phi \*w &= \*\Phi\tr (\*r^\pi + \gamma  \cdot \*P^\pi ) \*\Phi \*w \\
(\*\Phi\tr \*\Phi) \*w &= \*\Phi\tr r^\pi + \gamma \cdot \*\Phi\tr \*P^\pi  \*\Phi \*w \\
(\*\Phi\tr \*\Phi) \*w - \gamma \cdot \*\Phi\tr \*P^\pi  \*\Phi \*w &= \*\Phi\tr \*r^\pi  \\
(\*\Phi\tr \*\Phi  - \gamma \cdot \*\Phi\tr \*P^\pi  \*\Phi )\*w &= \*\Phi\tr \*r^\pi \\
\*w \;&=\; (\*\Phi\tr \*\Phi - \gamma \cdot \*\Phi\tr \*P^\pi \*\Phi)^{-1} \*\Phi\tr \*r^\pi~.
\end{align*}
\item Assume that $S$ is reasonably small, what choice of the feature function $\phi:\states \to \Real^K$ allows for a solution $\*w$ such that
\[
\tilde{\*v}^\pi = \*\Phi \*w = (\eye - \gamma \cdot \*P^\pi)^{-1} \*r^\pi~.
\]
An extra challenge is to think of such a feature matrix $\*\Phi$ for any $K > 0$.

\paragraph{Solution:}
Having $\*v^\pi \in \operatorname{span}(\*\Phi)$, which means that there exists $\*w$ such that $\*v^\pi = \*\Phi \*w$, is sufficient for $\tilde{\*v^\pi} = \*v^\pi$ to be a solution to the LSTD problem.
%			The feature matrix has to satisfy the following property
%
%		  $$\rank(\*\Phi) = \rank(P^\pi)$$

\item We will need to be able to compute the LSTD solution when the number of states in the problem is very large. The following two identities play a crucial role in this formulation:
\begin{align}
    \label{eq:sum_i}
    \*\Phi\tr \*\Phi &= \sum_{s\in\states} \*\phi(s)\*\phi(s)\tr \\
    \label{eq:sum_p}
    \*\Phi\tr \*P^\pi \*\Phi &= \sum_{s\in\states} \sum_{s'\in\states} \*\phi(s) P^\pi(s,s')\*\phi(s')\tr
\end{align}
Recall that $\phi(s)$ is a column vector. What are the dimensions of $\*\phi(s)\*\phi(s)\tr$?  What is its rank?
\paragraph{Solution:}
$\*\phi(s)\*\phi(s)\tr$ is a $K \times K$ matrix with rank one.
%	\item Using the definition of the matrix product and the definition of the terms, prove Eq.~\eqref{eq:sum_i}.
%	\item Using the definition of the matrix product and the definition of the terms, prove Eq.~\eqref{eq:sum_p}.

\end{enumerate}


\end{document}