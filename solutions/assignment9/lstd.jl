using LinearAlgebra, Plots
using Distributions: Categorical
using StatsBase: proportions
using ProgressBars: ProgressBar

## ------ Step 1 ------------------------------------

# Blackjack transition probabilities: Cards: 1-11
# state = hand + 1
# hand = 22 means 22 or more
P = zeros(23,23)
r = [17 ≤ state ≤ 22 ? state : 0 for state in 1:23]
# non-restarting transitions
for hand ∈ 0:15, card ∈ 1:11
    nextcount = min(hand + card, 22)
    P[hand + 1, nextcount + 1] += 1. / 11.
end
# restarting transition
for hand ∈ 16:22
    P[hand+1, 1] = 1.0
end
# check transition probabilities
@assert norm(ones(23) - P * ones(23)) ≤ 1e-10

## ------ Step 2 ------------------------------------
# true value function

γ = 0.99
v = (I - γ * P) \ r
plot(0:22, v, legend = nothing, xlabel="Hand", ylabel="Value")

## ------ Step 3 ------------------------------------
# stationary distribution

F = eigen(P')
d = Real.(F.vectors[:,end])
d .= d ./ sum(d) # normalize
@assert all(d .≥ 0) && sum(d) ≈ 1.0
plot(0:22, d; type = ":x", legend = nothing, xlabel="Hand", ylabel="Occupancy")

## ------ Step 4 ------------------------------------
# Model-based LSTD

pos(x) = max(0.,x)
#ϕ(h) = [1; pos(h-5.); pos(h-8.); pos(h-12.); pos(h-16.)]
ϕ(h) = [1; h; h^2; h^3]
Φ = reduce(vcat, (ϕ(h)' for h ∈ 0:22))
D = diagm(d)
# construct the projection matrix
L̃(w) = (Φ' * D * Φ) \ Φ' * D * (r  + γ * P * Φ * w)
w = zeros(size(Φ,2))
for i ∈ 1:1000
    w .= L̃(w)
end
ṽ = Φ * w
plot(0:22, ṽ, legend = nothing, xlabel="Hand", ylabel="Value")

# the following is a simplified iteration which may not converge??
#
#L̃₂(w) = Φ' * D * (r  + γ * P * Φ * w)
#
#w̃₂ = zeros(size(Φ,2))
#for i ∈ 1:1000
#    w̃₂ .= L̃₂(w̃₂)
#end
#ṽ₂ = Φ * w̃₂
## ------ Step 5 ------------------------------------
# Model-based LSTD

w = (Φ' * D * (I - γ * P) * Φ) \ Φ' * D * r

ṽ = Φ * w
plot(0:22, ṽ, legend = nothing, xlabel="Hand", ylabel="Value")
plot!(0:22, v, legend = nothing, xlabel="Hand", ylabel="Value")


## ------ Step 6 ------------------------------------
# Generate data
function generate()
    L = 5000
    Data = (state = fill(-1,L),
            stateprime = fill(-1,L),
            reward = fill(NaN,L) )
    s₀ = 1 # initial state
    s = s₀
    for l ∈ 1:L
        Data.state[l] = s
        Data.reward[l] = r[s]
        dst = Categorical(P[s,:])
        s = rand(dst) # get the random sample
        Data.stateprime[l] = s
    end
    @assert all(extrema(Data.state) .== (1,23))
    Data
end

Data = generate()
println("State difference ", norm(d - proportions(Data.state), 1))


## ------ Step 7 ------------------------------------
# Data-driven LSPI
function estimate(L′)
    K = size(Φ,2)
    F̃ = zeros(K,K)
    H̃ = zeros(K,K)
    h̃ = zeros(K)
    for l ∈ 1:L′
        #inplace multiplication to avoid allocating memory
        mul!(F̃, ϕ(Data.state[l]-1), ϕ(Data.state[l]-1)', 1., 1.)
        mul!(H̃, ϕ(Data.state[l]-1), ϕ(Data.stateprime[l]-1)', 1., 1.)
        h̃ .+= ϕ(Data.state[l]-1) .* Data.reward[l] 
    end
    (F̃ - γ * H̃) \ h̃
end

# not efficient, because it repeats work, but quick to write
err_range = 200:200:5000
errors = [norm(w - estimate(L′)) for L′ ∈ ProgressBar(err_range)]
plot(collect(err_range), errors, xlabel = "Dataset size", ylabel = "Error L2",
     legend = nothing) 


## ------ Step 7 ------------------------------------
# 

plot(0:22, ṽ, legend = nothing, xlabel="Hand", ylabel="Value")
plot!(0:22, v, legend = nothing, xlabel="Hand", ylabel="Value")
plot!(0:22, Φ * estimate(5000))
