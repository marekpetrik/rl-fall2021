# Language: Jupyter Notebook (Python 3.9)
# Author: Student
# Course: Reinforcement Learning (CS 880)
# Assignment 2: Problem 3c
# Creation Date: 9/15/21


### Cost Function ###
# Inputs:
#   1. x, state in {R,S} at stage k
#   2. u, action in {G,P} at stage k
#   3. w, disturbance in {E,F} at stage k
#   4. k, stage in {0,1,...,N-1}
#   5. cf, fuel cost in {Real Numbers}
#   6. cw, walking cost in {Real Numbers}
# Output:
#   1. g, cost in {Real Numbers} at timestep k
# Description: Returns cost g of given state-action pair, x and u, subject to a
#              random disturbance w, at stage k.

def cost(x, u, w, k, cf, cw): # define cost function
    if x == 'R' and u == 'P' and w == 'E': # if parking from road in empty spot
        gf = (k+1)*cf # fuel cost
        gw = (99-k)*cw # walking cost
        g = gf + gw # total cost
    else: # if any other state action pair
        g = 0 # total cost
    return g # return cost to main program


### State Transition Function ###
# Inputs:
#   1. x, state in {R,S} at stage k
#   2. u, action in {G,P} at stage k
#   3. w, disturbance in {E,F} at stage k
# Output:
#   1. y, new state in {R,S} at stage k+1
# Description: Returns new state y at stage k+1 according to the discrete-time
#              dynamic system of the form y = f(x,u,w) where x and u are the state
#              and action pair at stage k, respectively, and w is a random disturbance.

def dynamics(x, u, w): # define transition function
    if x == 'S' or ((u == 'P') and (w == 'E')): # if you are parked or parking and the spot is empty
        y = 'S' # you will be parked
    else: # otherwise
        y = 'R' # you are still on the road
    return y # return new state to main program


### Optimal Policy Function ###
# Inputs:
#   1. fuel cost cf in {Real Numbers}
#   2. walking cost cw in {Real Numbers}
# Output:
#   1. optimal value lookup-table Js in {Real Numbers^(k*x)} at all timesteps k for all states x
# Description: Returns optimal value lookup table Js at all timesteps k for all states x
#              using the dynamic programming algorithm for deterministic finite value
#              problems developed from the principle of optimality.

def policy(cf, cw): # define optimal value function

    ## Initialize Lookup Tables ##
    J = {} # value sequence
    Q = {} # Q-Factor
    P = {} # disturbance probabilities
    Js = {} # optimal value lookup-table
    us = {} # optimal policy

    ## Disturbance Probabilities ##
    P['G'] = {}
    P['P'] = {}
    P['G']['E'] = 1.0 # 100% chance road is empty
    P['G']['F'] = 0.0 # 0% chance road is full
    P['P']['E'] = 0.3 # 30% chance parking spot is empty
    P['P']['F'] = 0.7 # 70% chance parking spot is full

    ## Declare Terminal Optimal Values ##
    Js[N] = {} # initialize terminal optimal values
    Js[N]['R'] = cN # terminal optimal value if on road
    Js[N]['S'] = 0 # terminal optimal value if parked

    ## Compute Optimal Values ##
    for k in range(N-1,-1,-1): # from stage 99 to 0
        Js[k] = {} # initialize optimal value at stage k
        us[k] = {} # initialize optimal policy
        for x in ['R','S']: # for all states
            for u in ['G','P']: # for all actions
                for w in ['E','F']: # for all disturbances
                    Q[w] = P[u][w]*(cost(x, u, w, k, cf, cw) + Js[k+1][dynamics(x, u, w)]) # value for action u at stage k and state x
                J[u] = sum(Q.values())
            Js[k][x] = min(J.values()) # optimal value for stage k and state x
            us[k][x] = min(J, key=J.get) # optimal policy
    return us, Js # return optimal value lookup-table and policy to main program


### Main Program ###

## Trial 1: Gas More Expensive ##
N = 10 # total number of parking spaces
cw = 1 # [$] monetary cost of walking a parking space
cf = 2 # [$] monetary cost of driving a parking space
cN = 1 + N*cf # [$] monetary cost of missing the last parking space

us, Js = policy(cf, cw)
print("Trial 1: Gas More Expensive than Walking")
print("Input Parameters: cf = $", cf,"and cw = $",cw)
print("Optimal Policy:", us)
print("Optimal Value:", Js)
print()

## Trial 2: Walking More Expensive ##
#N = 100 # total number of parking spaces
#cw = 2 # [$] monetary cost of walking a parking space
#cf = 1 # [$] monetary cost of driving a parking space
#cN = 1 + N*cf # [$] monetary cost of missing the last parking space

#us, Js = policy(cf, cw)
#print("Trial 2: Walking More Expensive than Gas")
#print("Input Parameters: cf = $", cf,"and cw = $",cw)
#print("Optimal Policy:", us)
#print("Optimal Value:", Js)
#print()