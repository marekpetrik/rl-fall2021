#%% Define MDP parameters

const states = ['R', 'S']
const actions = ['P', 'G']
const outcomes = ['E', 'F']

""" Defines the parking problem """
struct Parking
    T :: UInt16         # number of spots (0-based: 0 ..  T)
    c_w :: Float64      # cost of walking 
    c_f :: Float64      # cost of fuel
    c_np:: Float64      # cost of not finding a parking spot at all
    p_f :: Float64      # probability the spot is free
end

# Define transitions and probabilities

""" Transitions """
function f(p :: Parking , t :: Int, s :: Char, a :: Char, w :: Char)
    (s == 'S' || (a == 'P' && w == 'E')) ? 'S' : 'R'
end

""" Intermediate rewards """
function r(p ::Parking, t :: Int, s :: Char, a :: Char, w::Char)
    (s == 'R' && a == 'P' && w == 'E') ? -((t+1) * p.c_f + (p.T-t-1) * p.c_w) : 0.0
end

""" Final rewards """
function r_T(p :: Parking, s :: Char) 
    (s == 'R') ? - p.c_np : 0.0
end

""" Probabilities """
function prob(p :: Parking, t :: Int, w :: Char)
    (w == 'F') ? p.p_f : 1.0 - p.p_f
end


#%% Solve the parking problem

"""
    A main function that solves the parking problem.
    Note: I emphasized clarity over performance
"""
function solve_parking(p :: Parking)
    # intialize value function and the policy
    valuefunction = ones(2, p.T + 1) * NaN64 # initialization to make sure all values are updated
    policy = ones(Int, 2, p.T) * -1
    # the q-values are not necessary to compute the solution, but can offer insights
    # indexing (s, a, t)
    q_values = ones(2, 2, p.T) * NaN64

    # update the values for the final step (recall: 1-based indexing)
    valuefunction[:, p.T + 1] = r_T.([p], states)  # the . vectorizes the function
    
    # iterate first over states
    # t is zero based, but arrays are 1-based
    for t in (p.T-1):-1:0
        # update the value and policy of each state
        for (i,s) in enumerate(states)
            # compute values for all actions (this value is q(s,a) for the s in the loop above)            
            for (j,a) in enumerate(actions)
                # compute the value (recall . vectorizes functions and operators)
                # over all possible outcomes / disturbances
                next_states = (f(p, t, s, a, w) for w in outcomes) 
                next_state_indices = (findfirst(isequal(ns), states) for ns in next_states)
                next_values = valuefunction[collect(next_state_indices), t+2]
                
                # compute the expectation as a dot product
                q_values[i,j,t+1] = (r.([p], t, s, a, outcomes) .+ next_values)' * prob.([p], t, outcomes)
            end
            # compute the policy as the maximum value
            policy[i,t+1] = argmax(q_values[i,:,t+1])
            
            # select the maximum value
            valuefunction[i,t+1] = q_values[i,policy[i,t+1],t+1]
        end
    end
    #return the values
    (policy = policy, value = valuefunction)
end

#%% Solve an example
p = Parking(10, 0.1, 0.01, 1.0, 0.3)
solution = solve_parking(p)

println("Policy:")
display(actions[solution.policy])
println("\n\nValue function:")
display(solution.value)