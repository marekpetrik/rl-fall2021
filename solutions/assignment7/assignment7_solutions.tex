\documentclass[letterpaper]{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{times}

\newcommand{\real}{\mathbb{R}}
\newcommand{\tr}{^\top}
\newcommand{\opt}{^\star}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0mm}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers

\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}


\title{Assignment 7}
\author{Reinforcement Learning}
\date{\textbf{Solutions}}


\begin{document}
    \maketitle

    \section*{Problem 1: Interest rate and discounting}

    In this problem we establish an important relationship between discount factors and interest rates.

    Suppose that each policy $\pi$ generates a steady and predictable (deterministic) stream of \emph{annual} incomes $r_0^\pi, r_1^\pi, r_2^\pi, \ldots, r_T^\pi$. You save these incomes in a bank account with an interest rate $\nu \ge 0$. The interest in your account compounds. That is, your account will have $g^\pi_0 = r_0^\pi$ funds in the first year, $g^\pi_1 = r_0^\pi \cdot (1+\nu) + r_1^\pi$ funds in the second year, $g^\pi_2 = (r_0^\pi \cdot (1+\nu) + r_1^\pi)\cdot(1+\nu) + r_2^\pi$, and so on.

    Please answer the following questions now.

    \begin{enumerate}
        \item Give a general formula for the total amounts of funds $g^\pi_t$ at any $t = 0, \ldots, T$.
        \paragraph{Solution:}
        \[
        g^\pi_T = \sum_{t=0}^T (1 + \nu)^{T - t} \cdot r_t^\pi
        \]
        \item Recall that the $\gamma$-discounted return $\rho^\pi$ of a policy for $T$ time steps is defined as:
        \[
        \rho_T^\pi \;=\; \sum_{t=0}^T \gamma^t \cdot r^\pi_t~.
        \]
        Now, give the value for the discount factor $\gamma \in [0,1]$ as a function of $\nu \ge 0$ such that
        \[
        g_T^{\pi_1} > g_T^{\pi_2} \;\Rightarrow\; \rho_T^{\pi_1} > \rho_T^{\pi_2}
        \]
        for any two policies $\pi_1$ and $\pi_2$. In other words, this means that a policy that maximizes the discounted return (value function) also maximizes the total amount of funds in the bank account.
        \paragraph{Solution:}
        \begin{align}
        	 g^\pi_T &= \sum_{t=0}^T (1 + \nu)^{T - t} \cdot r_t^\pi \\
        	 		&=  \sum_{t=0}^T \frac{(1 + \nu)^{T}}{(1 + \nu)^{t}} \cdot r_t^\pi \\
        	 		&= (1 + \nu)^{T} \cdot \sum_{t=0}^T \left(\frac{1}{1 + \nu}\right)^t \cdot r_t^\pi \\
        	 		\gamma &=  \frac{1}{1 + \nu} \\
        	 		\rho_T^\pi &= \frac{1}{(1 + \nu)^{T}} \cdot g^\pi_T
        \end{align}

    \end{enumerate}

    \section*{Problem 2: Value Function in Grid Worlds}

    In this problem, we will evaluate some policy in a grid world. You need to use linear algebra (numpy) to solve this problem. If you have written more than 50 lines of code, you are not implementing the solution to this problem efficiently.



  Give a proof showing that
        \[
        \| v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty \;\le\; \frac{1}{1-\gamma} \| L v_{0,100}^\pi - v_{0,100}^\pi \|_\infty~.
        \]


\paragraph{Solution:}
\begin{align}
	\| v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty &\;=\; \| v_{0,100}^\pi - L v_{0,100}^\pi + L v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty   \\
     &\;\le\; \| v_{0,100}^\pi - L v_{0,100}^\pi \|_\infty + \| L v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty   \\
     &\;=\; \| v_{0,100}^\pi - L v_{0,100}^\pi \|_\infty + \| L v_{0,100}^\pi - Lv_{0,\infty}^\pi \|_\infty   \\
     &\;\leq\; \| v_{0,100}^\pi - L v_{0,100}^\pi \|_\infty + \gamma \cdot \|  v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty   \\
(1 - \gamma) \cdot \| v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty    &\;\leq\; \| v_{0,100}^\pi - L v_{0,100}^\pi \|_\infty  \\
	\| v_{0,100}^\pi - v_{0,\infty}^\pi \|_\infty     &\;\le\; \frac{1}{1-\gamma} \| L v_{0,100}^\pi - v_{0,100}^\pi \|_\infty~.
\end{align}





    \section*{Problem 3: Neumann Series Representation}

    In this section, we will establish an alternative way of expressing the infinite-horizon $\gamma$ discounted value function $v_{0,\infty}^\pi$. As always, we assume that $\gamma \in [0,1)$ and also consider some fixed, given policy $\pi$.

    \begin{enumerate}
        \item Show that the value function can be expressed as the following series
        \[
        v_{0,\infty}^\pi = \sum_{t = 0}^\infty \gamma^t (P^\pi)^t r^\pi~.
        \]
        \paragraph{Solution:}
        \begin{align*}
        	v_{0,0}^\pi &= r^\pi  \\
			v_{0,t+1}^\pi &= L^\pi v_{0,t}^\pi \\
			&= r^\pi + \gamma P^\pi v_{0,t}^\pi \\
			&~\vdots \\
			v_{0,\infty}^\pi &= \sum_{t = 0}^\infty \gamma^t (P^\pi)^t r^\pi~.
        \end{align*}


        \item Look up and give the formula for the sum of the Neumann series (\url{https://en.wikipedia.org/wiki/Neumann_series}). Then use this formula to derive the formula for $v_{0,\infty}^\pi$.
        \paragraph{Solution:}
        Suppose that $T$ is a bounded linear operator on the normed vector space $X$. If the Neumann series converges in the operator norm, then  $I-T$ is invertible and its inverse is the series:
        \[
        (I - T)^{-1} = \sum_{k=0}^{\infty} T^k~.
        \]
        where $I$ is the identity operator in $X$. We have:
        \begin{align*}
				v_{0,\infty}^\pi &= \sum_{t = 0}^\infty \gamma^t (P^\pi)^t r^\pi~ \\
				&= \sum_{t = 0}^\infty (\gamma  P^\pi)^t r^\pi~ \\
				&=  (I - \gamma  P^\pi)^{-1} r^\pi~
        \end{align*}


        \item Show that the solution that you obtained in the part 2 of this problem satisfied the fixed-point solution
        \[ v_{0,\infty}^\pi = r^\pi + \gamma P^\pi v_{0,\infty}^\pi~. \]

        \paragraph{Solution:}
        \begin{align*}
        	v_{0,\infty}^\pi &=  (I - \gamma  P^\pi)^{-1} r^\pi~  \\
        	 			\vspace*{1cm} \\
 			L^\pi v_{0,\infty}^\pi &= r^\pi + \gamma P^\pi  (I - \gamma  P^\pi)^{-1} r^\pi   \\
 			&= \left( (I - \gamma  P^\pi) r^\pi + \gamma P^\pi  r^\pi \right) / (I - \gamma  P^\pi)   \\
			&= \left( r^\pi - \gamma  P^\pi r^\pi + \gamma P^\pi  r^\pi \right) / (I - \gamma  P^\pi)   \\
						&=  r^\pi  (I - \gamma  P^\pi)^{-1}   \\
			&=         	v_{0,\infty}^\pi~.
        \end{align*}


    \end{enumerate}

\end{document}