\documentclass[letterpaper]{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{url}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{times}

\newcommand{\real}{\mathbb{R}}
\newcommand{\tr}{^\top}
\newcommand{\opt}{^\star}

\setlength{\parskip}{3mm plus1mm minus1mm}
\setlength{\parindent}{0mm}

% Some notation that will be useful
\newcommand{\states}{\mathcal{S}}
\newcommand{\actions}{\mathcal{A}}
\newcommand{\Natural}{\mathbb{N}}   % Natural numbers, 1,2,3,4,...
\newcommand{\Real}{\mathbb{R}}      % Real numbers

\newcommand{\Ex}[1]{\mathbb{E}\left[ #1 \right]}
\newcommand{\Exx}[2]{\mathbb{E}_{{#1}}\left[ #2 \right]}
\renewcommand{\Pr}[1]{\mathbb{P}\left[ #1 \right]}
%\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\newcommand{\abs}[1]{\left|#1\right|}

\title{Assignment 6}
\author{Reinforcement Learning}
\date{\textbf{Solution}}


\begin{document}
\maketitle

\section*{Problem 1: Discounted problems}

Consider again a \emph{discounted} stochastic deterministic dynamic program. If $\gamma \in (0,1)$ is the discount factor, then the objective is to maximize:
\[
v_0\opt(s_0) = \max_{\pi\in\Pi} \; \Ex{\sum_{t=0}^{T-1} \gamma^t \cdot r_t(S_t, \pi_t(A_t), W_t) + \gamma^T \cdot r_T(S_T) \mid \ldots}
\]
The dots ``$\ldots$'' refer to the standard assumptions on the states $S_t$ being governed by the state transition dynamics.

Derive finite upper and lower bounds on $v_0\opt(s_0)$ if you know nothing about the parameters of the problem except the discount factor $\gamma$ and that
\[
r_t(s,a,w) \in [-1,1], \qquad r_T(s) \in [-1,1]~
\]
for all states $s$, actions $a$, disturbances $w$, and time steps $t$. The problem may have an arbitrary number of states and actions. You also do not know the horizon length $T$. Your upper and lower bounds on $v_0\opt(s_0)$ should only depend on the discount factor $\gamma$.


\subsection*{Solution:}
The reward $r$ for each time step satisfies:
\[
\abs{r_t(s,a,w)} \leq 1, \qquad\text{for all }\, (s,a,w) \in \mathcal{S} \times \mathcal{A} \times \mathcal{W}
\]
Furthermore, $\abs{r_T(s)} \leq 1$ for all $s \in \mathcal{S}$, and $0 < \gamma < 1$. Therefore

\[
\abs{v_0\opt(s_0)} = \abs{\max_{\pi\in\Pi} \; \Ex{\sum_{t=0}^{T-1} \gamma^t \cdot r_t(S_t, \pi_t(A_t), W_t) + \gamma^T \cdot r_T(S_T) \mid \ldots}} \leq \sum_{t=0}^{T} \gamma^t 
\]
For $T = \infty$, we have $\sum_{t=0}^{T} \gamma^t  = \frac{1}{1 - \gamma}$~.

\section*{Problem 2: Reinforcement Learning}

So far, we have assumed that we are solving a stochastic dynamic program with known parameters. In particular, we assumed the we know the functions $f_t$ and $r_t$. In many reinforcement learning problems, these parameters are unknown, and we must learn from observations. For instance, consider learning to balance the pole in the cart-pole problem (\url{https://gym.openai.com/envs/CartPole-v1/}).

Assume that you have a stochastic dynamic program, but you do not know the function $f_t$. Instead, can access a domain simulator (such as OpenAI gym) that can generate a trajectory (see lecture notes 10) starting from the initial state $s_0$ at time $0$ following any provided policy. You cannot start the simulator from any other state or start it at a time step other than $0$. Assume that the simulation is easy to run.

\subsection*{Solution:}

\begin{enumerate}
    \item Multistep Lookahead: No. Since we can only start from $s_0$, generating trajectories from some states might not be possible. We need a simulator that can be started from all states.
    \item Rollouts: No. For the same reason as multistep lookahead.
    \item Fitted Value Iteration: No. FVI assumes a complete generative model (transition and reward function) is available.
    \item Fitted Q Iteration: Yes. FQI algorithm allows fitting any approximation architecture to the Q-function using a set of four-tuples $(s_t,a_t,r_t,s_{t+1})$. FQI works with random trajectories. It starts with a set of samples and incrementally builds q-function using supervised learning.
    \item Policy Gradient: Yes. Please, see the solution to problem 3.
\end{enumerate}


\section*{Problem 3: Policy Gradient}

Please derive the full formula for the update direction $d^i$ in Algorithm 4.1 in Lecture notes 10 for a softmax policy. In other words, you need to derive the gradient:
\[
\nabla_\theta \log \left(\Pr{\hat{a}^{j_l} \mid \pi_{\theta^i}}\right) ~,
\]
for a softmax policy.



\subsection*{Solution:}
Softmax policy defines as follows.

\[
\pi_\theta(s,a) \;=\; \frac{e^{\phi(s,a)\tr\theta}}{\sum_{a' \in \actions}{e^{\phi(s,a')\tr\theta}}} ~.
\]

We will need the gradient of the log of the softmax policy, with respect to the policy parameters \cite{silver2009monte}

\begin{equation}
\begin{aligned}
\nabla_\theta \log \pi_{\theta}(s,a) &= \nabla_\theta \log e^{\phi(s,a)\tr\theta} - \nabla_\theta \log \left( \sum_b e^{\phi(s,b)\tr\theta} \right) \\
&= \nabla_\theta \left( \phi(s,a)\tr\theta  \right) - \frac{\nabla_\theta \sum_b e^{\phi(s,b)\tr \theta}}{\sum_b e^{\phi(s,b)\tr \theta}} \\
&= \phi(s,a)  - \frac{\sum_b \phi(s,b)  e^{\phi(s,b)\tr \theta}}{\sum_b e^{\phi(s,b)\tr \theta}} \\
&= \phi(s,a)  - \sum_b \pi_{\theta}(s,b)\phi(s,b)
\end{aligned}
\end{equation}
which is the difference between the observed feature vector and the expected feature vector. We denote
this gradient by $\psi(s, a)$.
\begin{equation} \label{eq:gradient_formula}
\begin{aligned}
\nabla_\theta \log \left(\Pr{\hat{a}^j \mid \pi_{\theta^i}}\right) &=
\nabla_\theta \log \left(\prod_{t=0}^{T-1} \pi_{\theta^i}(\hat{s}^j_t,\hat{a}^j_t) \right) \\
&=  \sum_{t=0}^{T-1} \nabla_\theta \log \pi_{\theta^i}(\hat{s}^j_t,\hat{a}^j_t) \\
&=  \sum_{t=0}^{T-1} \psi(\hat{s}^j_t,\hat{a}^j_t)~\\
\end{aligned}
\end{equation}
This leads to a stochastic gradient ascent algorithm, in which each training example $(\hat{s}^j_t,\hat{a}^j_t)$ is used to update
the policy parameters, with step-size $\alpha$,
\[
    \Delta \theta = \alpha \psi(\hat{s}^j_t,\hat{a}^j_t)
\]



\begin{thebibliography}{9}
	\bibitem{silver2009monte}
	Silver, David, and Gerald Tesauro. "Monte-Carlo simulation balancing." Proceedings of the 26th Annual International Conference on Machine Learning. 2009.
\end{thebibliography}


\end{document}