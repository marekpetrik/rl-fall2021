using Random: rand
using Statistics: mean, std
using BenchmarkTools: @btime, @benchmark # for benchmarking
import ThreadsX  # for parallel evaluation

const actions = 0:7
const disturbs = 0:10

""" Transition function """
f_t(x, a, w) = clamp(x + 0.11 * w - 0.13 * a, 0, 1)

""" Rewards """
r_t(x, a, w) = -(x - 0.5)^2 - abs(a - 3)

""" Monte-carlo evaluation of a policy """
function MCe(x_init::Float64, π, t::Int, T::Int; runs::Int=100)
    rets = zeros(Float64, runs)
    for r in 1:runs # Threads.@threads slows this down
        ret_val :: Float64 = 0.
        x = x_init
        for k in t:(T-1)
            a = π(x,k)
            w = rand(disturbs)
            ret_val += r_t(x,a,w) 
            x = f_t(x,a,w)
        end 
        @inbounds rets[r] = ret_val   
    end
    return mean(rets)
end

π_simple(s,t) = Int(round(7 * s))
MCe(0., π_simple, 0, 365; runs = 100)

#MCe(0.,π_simple, 0, 365; runs = 1)

#### Compute variances

""" Computes the mean and standard deviation of the MC estimator """
function compute_mean_std(π; repeats :: Int = 20)
    mean_std(runs) = begin
        estimates = [MCe(0., π, 0, 365; runs = runs)  for i in 1:repeats]
        (runs, mean(estimates), std(estimates))
    end
    # parallelization
    ThreadsX.map(x -> mean_std(x), collect(5:5:200))
end

#@btime compute_mean_var($π_simple)

using Gadfly: plot, Geom
using DataFrames: DataFrame, stack

function plot_variances()
    mean_stds = compute_mean_std(π_simple; repeats = 100);
    plot(DataFrame(runs = getindex.(mean_stds, 1), means = getindex.(mean_stds, 2),
                stds = getindex.(mean_stds, 3) ),
        x = :runs, y = :stds, Geom.line)
end

plot_variances()

### Compute the lookahead policy

""" Computes the myopic policy for a given state """
π_myopic_dtm(x,t) = actions[argmax([r_t(x, a, 0) for a in actions])]

MCe(0.,π_myopic_dtm, 0, 365; runs = 10)


### Compute the rollout policy
""" Computes the rollout policy for the provided policy """
function π_rollout_make(π)
    # compute the q value
    function π_rollout(x, t) 
        q(a) = mean( (r_t(x,a,w) + MCe(f_t(x,a,w), π, t+1, 365; runs = 20) for w in disturbs) )
        actions[argmax([q(a) for a in actions])]
    end
end

MCe(0.,π_rollout_make(π_simple), 0, 365; runs = 1)


### Plot value function

import Plots

xs = collect(range(0,1; length = 30))
true_values = ThreadsX.map(x -> MCe(x,π_simple, 0, 365; runs = 10000), xs)

Plots.scatter(xs, true_values)