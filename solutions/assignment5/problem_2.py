"""
Assignment 5
Problem 2: Fitted Value Iteration
Reinforcement Learning - Fall 2021
Solutions
"""

import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
from joblib import Parallel, delayed

states_true = np.linspace(0, 1, 500)
states_hat = np.linspace(0, 1, 15)
actions = np.arange(0, 8)
disturbances = np.arange(0, 11)
T = 365  # The horizon is one year
state_0 = 0  # initial state


def rbf(state, center, sigma=1):
    return np.exp(-(state - center) ** 2 / (2 * sigma ** 2))


feature_function = {
    "linear": lambda state: np.array([state ** k for k in range(2)]),
    "poly_deg2": lambda state: np.array([state ** k for k in range(3)]),
    "poly_deg3": lambda state: np.array([state ** k for k in range(4)]),
    "rbf": lambda state: np.array([rbf(state, 0.2), rbf(state, 0.5), rbf(state, 0.7)]),
    "constant": lambda state: np.array(list(map(int, abs(np.linspace(0, 1, 5) - state) <= 0.125))),
    "spline": lambda state: np.array([1, state, max(0.0, state - 0.1), max(0.0, state - 0.3), max(0.0, state - 0.7)])
}


def generate_features(states, feature="linear"):
    f = feature_function[feature]
    return np.array(list(map(f, states)))


def transition_function(state, action, disturbance):
    return min(1.0, max(0.0, state + 0.11 * disturbance - 0.13 * action))


def reward_function(state, action, t=0):
    return - (state - 0.5) ** 2 - np.abs(action - 3) if t < T else 0


def expected_return(state, action, w_tilde, feature="poly_deg2", t=0):
    reward_vector = np.ones(len(disturbances)) * reward_function(state, action, t)
    state_prime = np.array([transition_function(state, action, dist) for dist in disturbances])
    phi = generate_features(state_prime, feature)
    return np.mean(reward_vector + phi @ w_tilde)


def bellman_operator(state, w_tilde, feature="poly_deg2"):
    returns = np.array([expected_return(state, action, w_tilde, feature) for action in actions])
    return np.max(returns)


def linear_fitted_value_iteration(states, feature="poly_deg2"):
    v_tilde = [0] * (T + 1)
    w_tilde = [0] * (T + 1)
    v_tilde[-1] = np.zeros(len(states))  # Because r_T = 0 for all states
    phi = generate_features(states, feature)
    w_tilde[-1] = np.linalg.pinv(phi.T @ phi) @ phi.T @ v_tilde[-1]
    for t in range(T - 1, -1, -1):
        v_tilde[t] = np.array([bellman_operator(state, w_tilde[t + 1], feature) for state in states])
        w_tilde[t] = np.linalg.pinv(phi.T @ phi) @ phi.T @ v_tilde[t]
    return np.array(w_tilde)


def plot_value_function(states, w_tilde, feature, value_monte_carlo):
    phi = generate_features(states, feature)
    v_tilde = phi @ w_tilde
    plt.plot(states, v_tilde, label=feature + " fitted function")
    plt.plot(states_hat, value_monte_carlo, color='r', label="Monte Carlo")
    plt.xlabel("State")
    plt.ylabel("Value")
    plt.legend(loc="lower left")
    plt.title(feature + " linear fitted function approximation")
    plt.show()


def greedy_policy(state, w_tilde, t=0, feature="poly_deg2"):
    returns = np.array([expected_return(state, action, w_tilde[t + 1], feature) for action in actions])
    return np.argmax(returns)


def simulated_return(state, w_tilde, current_time_step, feature="poly_deg2"):
    cur_state = state
    sum_reward = 0
    for t in range(current_time_step, T):
        dist = np.random.choice(disturbances)
        action = greedy_policy(cur_state, w_tilde, t, feature)
        sum_reward += reward_function(cur_state, action, t)
        cur_state = transition_function(cur_state, action, dist)
    return sum_reward


""" 
Monte-carlo evaluation of a policy 
"""


def rollout(state, w_tilde, current_time_step, feature="poly_deg2", simulation=30):
    num_cores = multiprocessing.cpu_count()
    results = Parallel(n_jobs=num_cores)(
        delayed(simulated_return)(state, w_tilde, current_time_step, feature) for _ in range(simulation))
    return np.mean(results)


if __name__ == '__main__':
    feature = "poly_deg2"
    simulation = 30
    w_tilde = linear_fitted_value_iteration(states_hat, feature)
    w_tilde_0 = w_tilde[state_0]  # initial state is 0
    values_monte_carlo = [rollout(state, w_tilde, 0, feature, simulation) for state in states_hat]
    plot_value_function(states_true, w_tilde_0, feature, values_monte_carlo)
