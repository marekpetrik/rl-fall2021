"""
Assignment 5
Problem 1: Linear Regression
Reinforcement Learning - Fall 2021
Solutions
"""

import numpy as np
import matplotlib.pyplot as plt

states_true = np.linspace(0, 1, 500)
states_hat = np.linspace(0, 1, 11)


def rbf(state, center, sigma=1):
    return np.exp(-(state - center) ** 2 / (2 * sigma ** 2))


feature_function = {
    "linear": lambda state: np.array([state ** k for k in range(2)]),
    "poly_deg2": lambda state: np.array([state ** k for k in range(3)]),
    "poly_deg3": lambda state: np.array([state ** k for k in range(4)]),
    "rbf": lambda state: np.array([rbf(state, 0.2), rbf(state, 0.5), rbf(state, 0.7)]),
    "constant": lambda state: np.array(list(map(int, abs(np.linspace(0, 1, 5) - state) <= 0.125))),
    "spline": lambda state: np.array([1, state, max(0.0, state - 0.1), max(0.0, state - 0.3), max(0.0, state - 0.7)])
}


def generate_features(states, feature="linear"):
    f = feature_function[feature]
    return np.array([f(state) for state in states])


def true_value_function(state):
    return min(2 * state, -0.1 * state + 0.2, 0.8 - state)


if __name__ == '__main__':
    method = "spline"
    v_true = np.array([true_value_function(state) for state in states_hat])
    phi = generate_features(states_hat, method)
    w_star = np.linalg.pinv(phi.T @ phi) @ phi.T @ v_true
    # approximate value function
    v_tilde = phi @ w_star
    plt.scatter(states_hat, v_tilde, color='r', marker='o', label=method + " features")
    plt.plot(states_true, np.array([true_value_function(state) for state in states_true]), label="True")
    plt.xlabel("State")
    plt.ylabel("Value")
    plt.legend(loc="lower left")
    plt.title(method + " function approximation")
    plt.show()
